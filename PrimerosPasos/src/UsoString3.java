
public class UsoString3 {

	public static void main(String[] args) {
		
		String alumno1, alumno2;
		
		alumno1 = "David";
		alumno2 = "david";
		
		/*El m�todo equals compara 2 cadenas y toma en cuenta min�sculas y 
		may�sculas, devuelve true si son exactamente iguales y false si 
		no*/
		System.out.println(alumno1.equals(alumno2));
		
		/*El m�todo equals compara 2 cadenas y no toma en cuenta 
		min�sculas y may�sculas, devuelve true si son iguales y false 
		si no*/
		System.out.println(alumno1.equalsIgnoreCase(alumno2));
	}

}
