
public class ArrayAleatorio {

	public static void main(String[] args) {
		
		//Declaraci�n del array
		int[] array_aleatorios = new int[150];
		int longitud = array_aleatorios.length;
		int i;
		
		//Llenado de n�meros aleatorios
		for(i=0;i<longitud;i++) {
			
			array_aleatorios[i] = (int)Math.round((Math.random() * 100));
		}
		
		//Formatear el contador
		i = 0;
		
		//Mostrar el array
		for(int j:array_aleatorios) {
			
			System.out.println("�ndice " + (i++) + " : Valor " + j);
		}
	}

}
