
public class ArrayBidimensional {

	public static void main(String[] args) {
		
		int[][] matrizBidimensional = new int[4][5];
		/*Para tener la longitud de la segunda dimension usamos el
		 * primer indice de la primera dimension*/
		int i, j, dimension1 = matrizBidimensional.length,
		dimension2 = matrizBidimensional[0].length;
		
		//Llenamos la matriz de numeros aleatorios
		for(i=0;i<dimension1;i++) {
			for(j=0;j<dimension2;j++) {
				matrizBidimensional[i][j] = (int)Math.round(
					(Math.random() * 100)
				);
				
			}
			
		}
		
		//Mostramos la matriz
		for(int[] k:matrizBidimensional) {
			for(int z:k) {
				System.out.print(z + " ");
			}
			System.out.println();
		}
		
	}

}
