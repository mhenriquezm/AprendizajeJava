import javax.swing.*;

public class NumeroMagico {
	
	public static void main(String[] args) {
		
		int aleatorio = (int)Math.round((Math.random() * 100));
		int dato = -1, intentos = 0;
		
		while(aleatorio != dato) {
			
			dato = Integer.parseInt(JOptionPane
					.showInputDialog("Ingrese un n�mero entre [0-100]")
				);
			
			if(aleatorio < dato) {
				
				System.out.println("Ingrese un n�mero menor");
				
			} 
			else if(aleatorio > dato) {
				
				System.out.println("Ingrese un n�mero mayor");
			}
			
			intentos++;
		}
		
		JOptionPane.showMessageDialog(null, "�Ganaste!"
				+ " N�mero de intentos: " + intentos);
	}
	
}
