import javax.swing.*;

public class FormatoDeNumeros {

	public static void main(String[] args) {
		
		String num1 = JOptionPane.showInputDialog("Ingrese un n�mero");
		double numero = Double.parseDouble(num1);
		
		System.out.print("La ra�z cuadrada de " + numero + " es: ");
		/*Si utilizamos el m�todo printf podemos dar formato a un n�mero
		 el primer par�metro es un String en donde indicamos el formato 
		 y el segundo par�metro es el n�mero o expresion*/
		System.out.printf("%1.2f", Math.sqrt(numero));
		
	}

}
