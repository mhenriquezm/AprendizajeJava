import java.util.*;

public class EvaluarEdad {

	public static void main(String[] args) {
		
		Scanner entrada = new Scanner(System.in);
		
		System.out.print("Ingrese su edad: ");
		int edad = entrada.nextInt();
		
		/*El if evalua una condici�n l�gica y si se cumple ejecuta su 
		 * bloque*/
		if(edad < 18) {
			
			System.out.println("\nEres un adolescente");
			
		} else if(edad < 40) { 
			//En caso contrario ejecuta el bloque del else
			System.out.println("\nEres joven");
			
		} else if(edad < 65) {
			
			System.out.println("\nEres maduro");
			
		} else {
			
			System.out.println("\nCu�date");
			
		}
		
		entrada.close();

	}

}
