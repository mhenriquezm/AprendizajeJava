
public class UsoString {

	public static void main(String[] args) {
		
		//Las cadenas de texto siempre van entre comillas dobles
		String nombre = "Felipe Juan Froil�n de Todos los Santos";
		//El m�todo length() devuelve la longitud de la cadena
		int longitudNombre = nombre.length();
		
		System.out.println("- Mi nombre es: " + nombre
			+ "\n- Mi nombre tiene: " + longitudNombre + " letras"
			+ "\n- La primera letra de " + nombre + " es: " 
			+ nombre.charAt(0)
			+ "\n- La ultima letra de " + nombre + " es: " 
			+ nombre.charAt(longitudNombre-1)
		);
		/*El m�todo charAt(int n) devuelve el caracter de la posici�n
		 * indicada en el par�metro que recibe, el par�metro es de tipo
		 * int*/
		
	}

}
