package clases_internas;

import javax.swing.JOptionPane;

public class Main {

	public static void main(String[] args) {
		
		Temporizador reloj = new Temporizador(3000, true);
		
		reloj.ejecutar();
		
		JOptionPane.showMessageDialog(null, "Aceptar para salir");
		
		System.exit(0);
		
	}

}
