package clases_internas.locales;

import javax.swing.JOptionPane;

public class Main {

	public static void main(String[] args) {
		
		Temporizador reloj = new Temporizador();
		
		reloj.ejecutar(3000, true);
		
		JOptionPane.showMessageDialog(null, "Aceptar para salir");
		
		System.exit(0);
		
	}

}
