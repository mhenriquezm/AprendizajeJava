package clases_internas.locales;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.Toolkit;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.swing.Timer;

public class Temporizador {
	
	//Los par�metros utilizados por la clase interna local son constantes
	public void ejecutar(int frecuencia, final boolean sonido) {
		
		//Si la clase tendr� una unica instancia se puede hacer local
		class DarLaHora implements ActionListener {
			
			public void actionPerformed(ActionEvent e) {
				
				System.out.println("La hora es: " 
					+ new SimpleDateFormat("hh:mm:ss a")
					.format(new Date()));
				
				if(sonido) Toolkit.getDefaultToolkit().beep();
				
			}
			
		}
		
		Timer reloj = new Timer(frecuencia, new DarLaHora());
		
		reloj.start();
	}
	
}