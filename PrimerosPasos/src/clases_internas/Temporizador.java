package clases_internas;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.Toolkit;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.swing.Timer;

public class Temporizador {
	
	//M�todos constructores
	public Temporizador(int frecuencia, boolean sonido) {
		
		this.frecuencia = frecuencia;
		this.sonido = sonido;
		
	}
	
	//M�todos Setter
	public void ejecutar() {
		
		Timer reloj = new Timer(frecuencia, new DarLaHora());
		
		reloj.start();
	}
	
	//Las clases internas pueden ser encapsuladas
	private class DarLaHora implements ActionListener {
		
		public void actionPerformed(ActionEvent e) {
			
			System.out.println("La hora es: " 
				+ new SimpleDateFormat("hh:mm:ss a").format(new Date()));
			
			if(sonido) Toolkit.getDefaultToolkit().beep();
			
		}
		
	}
	
	//Atributos
	private int frecuencia;
	private boolean sonido;
}
