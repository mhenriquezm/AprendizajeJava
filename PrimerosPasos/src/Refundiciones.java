
public class Refundiciones {

	public static void main(String[] args) {
		
		double numero1 = 5.85;
		
		/*Si necesitamos transformar un tipo de dato en otro lo 
		refundimos o lo transformamos con un casting*/
		int resultado = (int) Math.round(numero1);
		
		System.out.println(resultado);

	}

}
