import javax.swing.*;

public class ValidarCorreo {

	public static void main(String[] args) {
		
		//Variables
		String email = JOptionPane.showInputDialog("Ingrese su correo");
		int longCorreo = email.length(), arroba = 0;
		boolean punto = false;
		
		//Procedimiento, verificar toda la cadena
		for(int i=0;i<longCorreo;i++) {
			
			//Evaluar el caracter actual para la arroba
			if(email.charAt(i) == '@') arroba++;
			
			//Evaluar el caracter actual para el punto
			if(email.charAt(i) == '.') punto = true;
			
		}
		
		//Dar el resultado en función a la validación
		if((arroba == 1) && (punto)) {
			System.out.println("Email correcto");
		}
		else {
			System.out.println("Email incorrecto");
		}
		
	}

}
