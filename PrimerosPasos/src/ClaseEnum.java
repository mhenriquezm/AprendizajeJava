import java.util.Scanner;

public class ClaseEnum {
	
	enum Tallas {
		
		//Valores fijos parametrizados
		MINI("S"), MEDIANA("M"), GRANDE("L"), ENORME("XL");
		
		//M�todos constructores (deben estar encapsulados)
		private Tallas(String abreviatura) {
			
			//Asignamos al tipo su abreviatura
			this.abreviatura = abreviatura;
			
		}
		
		//M�todos Getter
		public String devolverAbreviatura() {
			return abreviatura;
		}
		
		//Atributos
		private String abreviatura;
	}
	
	public static void main(String[] args) {
		
		Scanner entrada = new Scanner(System.in);
		
		System.out.print("Ingrese su talla: "
			+ "\n- MINI"
			+ "\n- MEDIANA"
			+ "\n- GRANDE"
			+ "\n- ENORME"
			+ "\n*********************"
			+ "\nOpci�n: ");
		
		String talla = entrada.next().toUpperCase();
		Tallas la_talla = Enum.valueOf(Tallas.class, talla);
		
		System.out.println(
			"\n- Su talla es: " + la_talla
			+ "\n- Abreviatura: " + la_talla.devolverAbreviatura());
		
		entrada.close();
	}

}
