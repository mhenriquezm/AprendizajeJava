
public class UsoArray {
	
	public static void main(String[] args) {
		
		//Declaraci�n de array e iniciaci�n
		int[] array = {15, 38, 27, 92, 71, 95, 85, 65, 25, 14, 78};
		
		//Mostrando los valores de la matriz
		int longArray = array.length;
		
		for(int i=0;i<longArray;i++) {
			
			System.out.println("�ndice #" + i + " -> " + array[i]);
		}
		
	}
	
}
