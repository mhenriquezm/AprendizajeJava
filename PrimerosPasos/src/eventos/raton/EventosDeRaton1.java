package eventos.raton;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;

import javax.swing.JFrame;

public class EventosDeRaton1 {
	
	public static void main(String[] args) {
		
		MarcoRaton1 marco = new MarcoRaton1();
		
		marco.setVisible(true);
		marco.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	
}

class MarcoRaton1 extends JFrame {
	
	//M�todos constructores
	public MarcoRaton1() {
		
		Dimension pantalla = Toolkit.getDefaultToolkit().getScreenSize();
		
		int ancho = pantalla.width;
		int alto = pantalla.height;
		
		setBounds(((ancho - 500) / 2), ((alto - 350) / 2), 500, 350);
		setTitle("Eventos de rat�n");
		
		addMouseMotionListener(new GestionadorDeRaton1());
	}
	
	//Campos de clase
	private static final long serialVersionUID = 1L;
}

class GestionadorDeRaton1 implements MouseMotionListener {

	public void mouseDragged(MouseEvent e) {
		
		System.out.println("Est�s arrastrando el rat�n");
	}

	public void mouseMoved(MouseEvent e) {
		
		System.out.println("Est�s moviendo el rat�n");
	}
}