package eventos.raton;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JFrame;

public class EventosDeRaton {

	public static void main(String[] args) {
		
		MarcoRaton marco = new MarcoRaton();
		
		marco.setVisible(true);
		marco.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

}

class MarcoRaton extends JFrame {
	
	//M�todos constructores
	public MarcoRaton() {
		
		Dimension pantalla = Toolkit.getDefaultToolkit().getScreenSize();
		
		int ancho = pantalla.width;
		int alto = pantalla.height;
		
		setBounds(((ancho - 500) / 2), ((alto - 350) / 2), 500, 350);
		setTitle("Eventos de rat�n");
		
		addMouseListener(new GestionadorDeRaton());
	}
	
	//Campos de clase
	private static final long serialVersionUID = 1L;
}

class GestionadorDeRaton implements MouseListener {
	
	public void mouseClicked(MouseEvent e) {
		
		System.out.println("Has hecho un click");
	}

	public void mouseEntered(MouseEvent e) {
		
		System.out.println("Has entrado al marco");
	}

	public void mouseExited(MouseEvent e) {
		
		System.out.println("Has salido del marco");
	}

	public void mousePressed(MouseEvent e) {
		
		System.out.println("Est�s presionando el bot�n izquierdo");
	}

	public void mouseReleased(MouseEvent e) {
		
		System.out.println("Has dejado de presionar el bot�n izquierdo");
	}
	
}