package eventos.multiples_fuentes;

import java.awt.Dimension;
import java.awt.Toolkit;

import javax.swing.JFrame;

public class MarcoMultiplesFuentes extends JFrame {
	
	//M�todos constructores
	public MarcoMultiplesFuentes() {
		
		Dimension pantalla = Toolkit.getDefaultToolkit().getScreenSize();
		
		int ancho = pantalla.width;
		int alto = pantalla.height;
		
		setBounds(((ancho - 500) / 2), ((alto - 350) / 2), 500, 350);
		setTitle("M�ltiples fuentes de evento");
		
		add(new LaminaMultiplesFuentes());
	}
	
	//Campos de clase
	private static final long serialVersionUID = 1L;
}
