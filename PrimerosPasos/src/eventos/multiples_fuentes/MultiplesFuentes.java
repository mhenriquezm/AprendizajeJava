package eventos.multiples_fuentes;

import javax.swing.JFrame;

public class MultiplesFuentes {

	public static void main(String[] args) {
		
		MarcoMultiplesFuentes marco = new MarcoMultiplesFuentes();
		
		marco.setVisible(true);
		marco.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

}
