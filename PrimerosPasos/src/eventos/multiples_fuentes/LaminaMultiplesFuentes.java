package eventos.multiples_fuentes;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.io.File;

import javax.swing.AbstractAction;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.KeyStroke;

public class LaminaMultiplesFuentes extends JPanel {
	
	//M�todos constructores
	public LaminaMultiplesFuentes() {
		
		String[] titulos = {"Amarillo", "Azul", "Rojo"};
		
		agregarBotones(titulos);
	}
	
	//M�todos Setter
	public void agregarBotones(String[] titulos) {
		
		for(String i: titulos) {
			
			switch(i) {
				case "Amarillo":
					
					accion = instanciarAccion(i, "src"
							+ File.separator
							+ "eventos"
							+ File.separator
							+ "multiples_fuentes"
							+ File.separator
							+ "bola-amarilla.gif", Color.YELLOW);
					
					asignarCombinaciones(JPanel.WHEN_IN_FOCUSED_WINDOW, 
						"ctrl A", "OBJETO1");
					
					add(new JButton(accion));
				break;
				
				case "Azul":
					
					accion = instanciarAccion(i, "src"
							+ File.separator
							+ "eventos"
							+ File.separator
							+ "multiples_fuentes"
							+ File.separator
							+ "bola-azul.gif", Color.BLUE);
					
					asignarCombinaciones(JPanel.WHEN_IN_FOCUSED_WINDOW, 
							"ctrl B", "OBJETO2");
					
					add(new JButton(accion));
				break;
				
				case "Rojo":
					
					accion = instanciarAccion(i, "src"
							+ File.separator
							+ "eventos"
							+ File.separator
							+ "multiples_fuentes"
							+ File.separator
							+ "bola-roja.gif", Color.RED);
					
					asignarCombinaciones(JPanel.WHEN_IN_FOCUSED_WINDOW, 
							"ctrl R", "OBJETO3");
					
					add(new JButton(accion));
				break;
			}
			
		}
		
	}
	
	public GestionadorDeAcciones instanciarAccion(String nombre, 
			String ruta, Color color) {
		
		return new GestionadorDeAcciones(nombre, new ImageIcon(ruta),
				color);
	}
	
	public void asignarCombinaciones(int condicion, String combinacion, 
			String objeto) {
		
		getInputMap(condicion).put(KeyStroke.getKeyStroke(combinacion), 
				objeto);
		
		getActionMap().put(objeto, accion);
	}
	
	//Clases internas
	private class GestionadorDeAcciones extends AbstractAction {
		
		//M�todos constructores
		public GestionadorDeAcciones(String titulo, Icon icono, 
				Color color) {
			
			putValue(AbstractAction.NAME, titulo);
			putValue(AbstractAction.SMALL_ICON, icono);
			putValue(AbstractAction.SHORT_DESCRIPTION, "Coloca la "
					+ "ventana de color " + titulo);
			putValue("ACCION", color);
		}
		
		//M�todos Setter
		public void actionPerformed(ActionEvent e) {
			
			setBackground((Color) getValue("ACCION"));
		}
		
		//Campos de clase
		private static final long serialVersionUID = 1L;
	}
	
	//Campos de clase
	private GestionadorDeAcciones accion;
	private static final long serialVersionUID = 1L;
}
