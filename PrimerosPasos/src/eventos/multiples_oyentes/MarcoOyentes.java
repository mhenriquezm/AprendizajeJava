package eventos.multiples_oyentes;

import javax.swing.JFrame;

public class MarcoOyentes extends JFrame {
	
	//M�todos constructores
	public MarcoOyentes() {
		
		setBounds(900, 100, 300, 200);
		setTitle("M�ltiples oyentes");
		
		add(new LaminaOyentes());
	}
	
	//Campos de clase
	private static final long serialVersionUID = 1L;
}
