package eventos.multiples_oyentes;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;

public class MarcoEmergente extends JFrame {
	
	//M�todos constructores
	public MarcoEmergente(JButton boton) {
		
		ActionListener oyente = new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				
				dispose();
				id = 0;
			}
			
		};
		
		setBounds((80 + (30 * id)), (100 + (30 * id)), 300, 200);
		setTitle("Marco: " + (++id));
		
		boton.addActionListener(oyente);
	}
	
	//Campos de clase
	private static int id = 0;
	private static final long serialVersionUID = 1L;
}
