package eventos.multiples_oyentes;

import javax.swing.JFrame;

public class MultiplesOyentes {

	public static void main(String[] args) {
		
		MarcoOyentes marco = new MarcoOyentes();
		
		marco.setVisible(true);
		marco.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

}
