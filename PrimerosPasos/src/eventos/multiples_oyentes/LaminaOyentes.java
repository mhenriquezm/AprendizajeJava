package eventos.multiples_oyentes;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JPanel;

public class LaminaOyentes extends JPanel {
	
	//M�todos constructores
	public LaminaOyentes() {
		
		JButton nuevo = new JButton("Nuevo");
		final JButton cerrar = new JButton("Cerrar todo");
		
		nuevo.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				
				MarcoEmergente marco = new MarcoEmergente(cerrar);
				
				marco.setVisible(true);
			}
			
		});
		
		add(nuevo);
		add(cerrar);
	}
	
	//Campos de clase
	private static final long serialVersionUID = 1L;
}