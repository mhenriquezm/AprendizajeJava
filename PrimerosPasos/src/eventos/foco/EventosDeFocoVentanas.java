package eventos.foco;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.WindowEvent;
import java.awt.event.WindowFocusListener;

import javax.swing.JFrame;

public class EventosDeFocoVentanas extends JFrame 
implements WindowFocusListener {
	
	//M�todo main
	public static void main(String[] args) {
		
		EventosDeFocoVentanas marco1 = new EventosDeFocoVentanas();
		
		marco1.setVisible(true);
		marco1.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		EventosDeFocoVentanas marco2 = new EventosDeFocoVentanas();
		
		marco2.setVisible(true);
		marco2.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
	}
	
	//M�todos constructores
	public EventosDeFocoVentanas() {
		
		Dimension pantalla = Toolkit.getDefaultToolkit().getScreenSize();
		
		int alto = pantalla.height;
		
		setBounds((150 + (510 * id)), ((alto - 350) / 2), 
				500, 350);
		
		addWindowFocusListener(this);
		
		id++;
	}
	
	//M�todos Setter
	public void windowGainedFocus(WindowEvent e) {
		
		setTitle("Gan� el foco");
	}
	
	public void windowLostFocus(WindowEvent e) {
		
		setTitle("Perd� el foco");
	}
	
	//Campos de clase
	private static int id = 0;
	private static final long serialVersionUID = 1L;
}
