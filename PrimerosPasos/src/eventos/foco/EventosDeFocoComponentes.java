package eventos.foco;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class EventosDeFocoComponentes {

	public static void main(String[] args) {
		
		MarcoFocoComponentes marco = new MarcoFocoComponentes();
		
		marco.setVisible(true);
		marco.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

}

class MarcoFocoComponentes extends JFrame {
	
	//M�todos constructores
	public MarcoFocoComponentes() {
		
		Dimension pantalla = Toolkit.getDefaultToolkit().getScreenSize();
		
		int ancho = pantalla.width;
		int alto = pantalla.height;
		
		setBounds(((ancho - 500) / 2), ((alto - 350) / 2), 500, 350);
		setTitle("Eventos de foco para componentes");
		
		add(new LaminaFocoComponentes());
	}
	
	//Campos de clase
	private static final long serialVersionUID = 1L;
}

class LaminaFocoComponentes extends JPanel {
	
	//M�todos constructores
	public LaminaFocoComponentes() {
		
		//Anulamos el layout por defecto
		setLayout(null);
		
		cuadro1 = new JTextField();
		cuadro2 = new JTextField();
		
		etiqueta1 = new JLabel();
		etiqueta2 = new JLabel();
		
		//Al anular el layout de debe usar setBounds con los componente
		cuadro1.setBounds(50, 50, 100, 20);
		cuadro2.setBounds(50, 80, 100, 20);
		
		etiqueta1.setBounds(160, 50, 100, 20);
		etiqueta2.setBounds(160, 80, 100, 20);
		
		cuadro1.addFocusListener(new GestionadorDeFoco());
		cuadro2.addFocusListener(new GestionadorDeFoco());
		
		add(cuadro1);
		add(cuadro2);
		add(etiqueta1);
		add(etiqueta2);
	}
	
	//Clases internas
	private class GestionadorDeFoco implements FocusListener {

		public void focusGained(FocusEvent e) {
			
			Object fuente = e.getSource();
			
			if(fuente.equals(cuadro1)) {
				
				etiqueta1.setText("Gan� el foco");
			}
			else if(fuente.equals(cuadro2)) {
				
				etiqueta2.setText("Gan� el foco");
			}
			
		}

		public void focusLost(FocusEvent e) {
			
			Object fuente = e.getSource();
			
			if(fuente.equals(cuadro1)) {
				
				etiqueta1.setText("Perd� el foco");
			}
			else if(fuente.equals(cuadro2)) {
				
				etiqueta2.setText("Perd� el foco");
			}
		}
		
	}
	
	//Campos de clase
	private JTextField cuadro1, cuadro2;
	private JLabel etiqueta1, etiqueta2;
	private static final long serialVersionUID = 1L;
}