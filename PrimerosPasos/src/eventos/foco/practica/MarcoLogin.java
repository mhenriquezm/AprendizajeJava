package eventos.foco.practica;

import java.awt.Dimension;
import java.awt.Toolkit;

import javax.swing.JFrame;

public class MarcoLogin extends JFrame {
	
	//M�todos constructores
	public MarcoLogin() {
		
		Dimension pantalla = Toolkit.getDefaultToolkit().getScreenSize();
		
		int ancho = pantalla.width;
		int alto = pantalla.height;
		
		setBounds(((ancho - 500) / 2), ((alto - 350) / 2), 500, 350);
		setTitle("Login MyR");
		
		add(new LaminaLogin());
	}
	
	//Campos de clase
	private static final long serialVersionUID = 1L;
}
