package eventos.foco.practica;

import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

public class LaminaLogin extends JPanel {
	
	//M�todos constructores
	public LaminaLogin() {
		
		setLayout(null);
		
		String[] titulosEtiquetas = {"Usuario:", "Clave:", "", ""};
		
		agregarEtiquetas(titulosEtiquetas);
		agregarCuadros();
	}
	
	//M�todos Setter
	public void agregarEtiquetas(String[] titulos) {
		
		int longitud = titulos.length;
		
		for(int i=0;i<longitud;i++) {
			
			JLabel etiqueta = new JLabel(titulos[i]);
			
			if(i > 1) {
				etiqueta.setBounds(320, (20  + (30 * (i - 1))), 100, 20);
			}
			else {
				etiqueta.setBounds(50, (20  + (30 * (i + 1))), 50, 20);
			}
			
			add(etiqueta);
		}
	}
	
	public void agregarCuadros() {
		
		for(int i=0;i<2;i++) {
			
			if(i == 0) {
				JTextField campo = new JTextField();
				
				campo.setBounds(110, (20 + (30 * (i + 1))), 200, 20);
				
				campo.addFocusListener(new GestionadorDeFoco());
				
				add(campo);
			}
			else {
				JPasswordField campo = new JPasswordField();
				
				campo.setBounds(110, (20 + (30 * (i + 1))), 200, 20);
				
				add(campo);
			}
			
		}
		
	}
	
	//Clases internas
	private class GestionadorDeFoco extends FocusAdapter {

		public void focusLost(FocusEvent e) {
			
			JTextField campo = (JTextField) e.getSource();
			String email = campo.getText();
			JLabel etiqueta = (JLabel) getComponent(2);
			
			int longitud = email.length();
			int arroba = 0;
			boolean punto = false;
			
			for(int i=0;i<longitud;i++) {
				
				if(email.charAt(i) == '@') {
					
					arroba++;
					
					if(arroba > 1) { 
						break;
					}
					
				}
				
				if(email.charAt(i) == '.') {
					
					punto = true;
				}
			}//for
			
			if((arroba == 1) && (punto)) {
				
				etiqueta.setText("Email correcto"); 
			}
			else {
				
				etiqueta.setText("Email incorrecto");
			}
		}
	}
	
	//Campos de clase
	private static final long serialVersionUID = 1L;
}
