package eventos;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class EventosDeAccion {

	public static void main(String[] args) {
		
		MarcoAccion marco = new MarcoAccion();
		
		marco.setVisible(true);
		marco.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
	}

}

class MarcoAccion extends JFrame {
	
	//M�todos constructores
	public MarcoAccion() {
		
		Dimension pantalla = Toolkit.getDefaultToolkit().getScreenSize();
		
		int ancho = pantalla.width;
		int alto = pantalla.height;
		
		setBounds(((ancho - 500) / 2), ((alto - 300) / 2), 500, 300);
		setTitle("Primer manejador de eventos");
		
		add(new LaminaAccion());
	}
	
	//Campos de clase
	private static final long serialVersionUID = 1L;
	
}

class LaminaAccion extends JPanel implements ActionListener {
	
	/*
	 * Objeto evento: click
	 * Objeto fuente: boton
	 * Objeto oyente: LaminaAccion
	*/
	
	//M�todos constructores
	public LaminaAccion() {
		
		String[] colores = {"Amarillo", "Azul", "Rojo"};
		
		agregarBotones(colores);
	}
	
	//M�todos Setter
	public void agregarBotones(String[] colores) {
		
		for(String i:colores) {
			JButton boton = new JButton(i);
			
			agregarOyente(boton, this);
			
			add(boton);
		}
	}
	
	public void agregarOyente(JButton boton, JPanel oyente) {
		
		boton.addActionListener((LaminaAccion) oyente);
		
	}
	
	public void actionPerformed(ActionEvent e) {
		
		//El m�todo getActionCommand devuelve la acci�n asociada al evento
		switch(e.getActionCommand()) {
			case "Amarillo": setBackground(Color.YELLOW); break;
			case "Azul": setBackground(Color.BLUE); break;
			case "Rojo": setBackground(Color.RED); break;
		}
	}
	
	//Campos de clase
	private static final long serialVersionUID = 1L;
	
}