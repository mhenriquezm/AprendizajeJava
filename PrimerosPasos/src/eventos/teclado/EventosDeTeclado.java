package eventos.teclado;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JFrame;

public class EventosDeTeclado {

	public static void main(String[] args) {
		
		MarcoTeclado marco = new MarcoTeclado();
		
		marco.setVisible(true);
		marco.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
	}

}

class MarcoTeclado extends JFrame {
	
	//M�todos constructores
	public MarcoTeclado() {
		
		Dimension pantalla = Toolkit.getDefaultToolkit().getScreenSize();
		
		int ancho = pantalla.width;
		int alto = pantalla.height;
		
		setBounds(((ancho - 500) / 2), ((alto - 350) / 2), 500, 350);
		setTitle("Eventos de teclado");
		
		addKeyListener(new GestionadorDeTeclado());
	}
	
	//Campos de clase
	private static final long serialVersionUID = 1L;
}

class GestionadorDeTeclado implements KeyListener {

	public void keyPressed(KeyEvent e) {
		
		System.out.println("Has pulsado la tecla: " + e.getKeyChar() 
				+ " -> C�digo: " + e.getKeyCode());
	}

	public void keyReleased(KeyEvent e) {}
	
	//keyTyped no retorna el codigo de la tecla
	public void keyTyped(KeyEvent e) {}
	
}