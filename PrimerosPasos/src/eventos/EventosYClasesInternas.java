package eventos;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class EventosYClasesInternas {

	public static void main(String[] args) {
		
		MarcoAccion2 marco = new MarcoAccion2();
		
		marco.setVisible(true);
		marco.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
	}

}

class MarcoAccion2 extends JFrame {
	
	//M�todos constructores
	public MarcoAccion2() {
		
		Dimension pantalla = Toolkit.getDefaultToolkit().getScreenSize();
		
		int ancho = pantalla.width;
		int alto = pantalla.height;
		
		setBounds(((ancho - 500) / 2), ((alto - 300) / 2), 500, 300);
		setTitle("Primer manejador de eventos");
		
		add(new LaminaAccion2());
	}
	
	//Campos de clase
	private static final long serialVersionUID = 1L;
	
}

class LaminaAccion2 extends JPanel {
	
	//M�todos constructores
	public LaminaAccion2() {
		
		String[] colores = {"Amarillo", "Azul", "Rojo"};
		
		agregarBotones(colores);
	}
	
	//M�todos Setter
	public void agregarBotones(String[] colores) {
		
		for(String i:colores) {
			JButton boton = new JButton(i);
			
			switch(i){
				case "Amarillo":
					agregarOyente(boton, new GestionadorColor(
							Color.YELLOW));
				break;
				
				case "Azul":
					agregarOyente(boton, new GestionadorColor(
							Color.BLUE));
				break;
				
				case "Rojo":
					agregarOyente(boton, new GestionadorColor(
							Color.RED));
				break;
			}
			
			add(boton);
		}
	}
	
	public void agregarOyente(JButton boton, GestionadorColor oyente) {
		
		boton.addActionListener(oyente);
		
	}
	
	//Clases internas
	private class GestionadorColor implements ActionListener {
		
		//M�todos constructores
		public GestionadorColor(Color color) {
			
			this.color = color;
		}
		
		//M�todos Setter
		public void actionPerformed(ActionEvent e) {
			
			setBackground(color);
		}
		
		//Campos de clase
		private Color color;
	}
	
	//Campos de clase
	private static final long serialVersionUID = 1L;
	
}