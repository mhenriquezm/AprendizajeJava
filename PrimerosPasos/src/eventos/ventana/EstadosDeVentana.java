package eventos.ventana;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.WindowEvent;
import java.awt.event.WindowStateListener;

import javax.swing.JFrame;

public class EstadosDeVentana {

	public static void main(String[] args) {
		
		MarcoEstadosVentana marco = new MarcoEstadosVentana();
		
		marco.setVisible(true);
		marco.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

}

class MarcoEstadosVentana extends JFrame {
	
	//M�todos constructores
	public MarcoEstadosVentana() {
		
		Dimension pantalla = Toolkit.getDefaultToolkit().getScreenSize();
		
		int ancho = pantalla.width;
		int alto = pantalla.height;
		
		setBounds(((ancho - 500) / 2), ((alto - 350) / 2), 500, 350);
		setTitle("Estados de la ventana");
		
		addWindowStateListener(new GestionadorDeEstados());
	}
	
	//Campos de clase
	private static final long serialVersionUID = 1L;
}

class GestionadorDeEstados implements WindowStateListener {
	
	//M�todos Setter
	public void windowStateChanged(WindowEvent e) {
		
		switch(e.getNewState()) {
			case JFrame.NORMAL:
				System.out.println("La pantalla est� en estado normal");
			break;
			
			case JFrame.MAXIMIZED_BOTH:
				System.out.println("La pantalla est� maximizada");
			break;
			
			case JFrame.ICONIFIED:
				System.out.println("La pantalla est� minimizada");
			break;
			
			case (JFrame.ICONIFIED + JFrame.MAXIMIZED_BOTH):
				System.out.println("La pantalla est� minimizada");
			break;
		}
		
	}
	
}