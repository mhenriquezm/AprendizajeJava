package eventos.ventana;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import javax.swing.JFrame;

public class EventosDeVentana {

	public static void main(String[] args) {
		
		MarcoAccionesV marco = new MarcoAccionesV();
		MarcoAccionesV marco2 = new MarcoAccionesV();
		
		marco.setVisible(true);
		marco2.setVisible(true);
		
		marco.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		marco2.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		
	}

}

//No construimos un JPanel porque las acciones de ventana son del JFrame
class MarcoAccionesV extends JFrame {
	
	//M�todos Constructores
	public MarcoAccionesV() {
		
		idSiguiente = id;
		
		Dimension pantalla = Toolkit.getDefaultToolkit().getScreenSize();
		
		int alto = pantalla.height;
		
		setBounds(((520 * idSiguiente) + 150), ((alto - 350) / 2), 
				500, 350);
		
		addWindowListener(new GestionadorDeAcciones());
		
		id++;
		
		setTitle("Ventana " + id);
	}
	
	//Campos de clase
	private int idSiguiente;
	private static int id = 0;
	private static final long serialVersionUID = 1L;
}

class GestionadorDeAcciones implements WindowListener {

	public void windowActivated(WindowEvent e) {
		
		System.out.println("Ventana activada");
	}

	public void windowClosed(WindowEvent e) {
		
		System.out.println("Ventana cerrada");
	}

	public void windowClosing(WindowEvent e) {
		
		System.out.println("Cerrando la ventana");
	}

	public void windowDeactivated(WindowEvent e) {
		
		System.out.println("Ventana desactivada");
	}

	public void windowDeiconified(WindowEvent e) {
		
		System.out.println("Ventana restaurada");
	}

	public void windowIconified(WindowEvent e) {
		
		System.out.println("Has minimizado la ventana");
	}

	public void windowOpened(WindowEvent e) {
		
		System.out.println("Ventana abierta");
	}
	
}