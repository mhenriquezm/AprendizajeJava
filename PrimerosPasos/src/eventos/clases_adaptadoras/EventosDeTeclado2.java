package eventos.clases_adaptadoras;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import javax.swing.JFrame;

public class EventosDeTeclado2 {

	public static void main(String[] args) {
		
		MarcoTeclado2 marco = new MarcoTeclado2();
		
		marco.setVisible(true);
		marco.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
	}

}

class MarcoTeclado2 extends JFrame {
	
	//M�todos constructores
	public MarcoTeclado2() {
		
		Dimension pantalla = Toolkit.getDefaultToolkit().getScreenSize();
		
		int ancho = pantalla.width;
		int alto = pantalla.height;
		
		setBounds(((ancho - 500) / 2), ((alto - 350) / 2), 500, 350);
		setTitle("Eventos de teclado");
		
		addKeyListener(new GestionadorDeTeclado2());
	}
	
	//Campos de clase
	private static final long serialVersionUID = 1L;
}

class GestionadorDeTeclado2 extends KeyAdapter {

	public void keyPressed(KeyEvent e) {
		
		System.out.println("Has pulsado la tecla: " + e.getKeyChar() 
				+ " -> C�digo: " + e.getKeyCode());
	}
	
}