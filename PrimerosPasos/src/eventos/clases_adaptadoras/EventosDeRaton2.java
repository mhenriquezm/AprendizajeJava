package eventos.clases_adaptadoras;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JFrame;

public class EventosDeRaton2 {

	public static void main(String[] args) {
		
		MarcoRaton2 marco = new MarcoRaton2();
		
		marco.setVisible(true);
		marco.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

}

class MarcoRaton2 extends JFrame {
	
	//M�todos constructores
	public MarcoRaton2() {
		
		Dimension pantalla = Toolkit.getDefaultToolkit().getScreenSize();
		
		int ancho = pantalla.width;
		int alto = pantalla.height;
		
		setBounds(((ancho - 500) / 2), ((alto - 350) / 2), 500, 350);
		setTitle("Eventos de rat�n");
		
		addMouseListener(new GestionadorDeRaton2());
	}
	
	//Campos de clase
	private static final long serialVersionUID = 1L;
}

class GestionadorDeRaton2 extends MouseAdapter {
	
	public void mouseClicked(MouseEvent e) {
		
		//Detectar las coordenadas del puntero
		System.out.println("Has hecho un click en el punto: (" + e.getX()
				+ "," + e.getY() + ")");
	}
	
}