package eventos.clases_adaptadoras;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JFrame;

public class EventosDeRaton5 {

	public static void main(String[] args) {
		
		MarcoRaton5 marco = new MarcoRaton5();
		
		marco.setVisible(true);
		marco.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

}

class MarcoRaton5 extends JFrame {
	
	//M�todos constructores
	public MarcoRaton5() {
		
		Dimension pantalla = Toolkit.getDefaultToolkit().getScreenSize();
		
		int ancho = pantalla.width;
		int alto = pantalla.height;
		
		setBounds(((ancho - 500) / 2), ((alto - 350) / 2), 500, 350);
		setTitle("Eventos de rat�n");
		
		addMouseMotionListener(new GestionadorDeRaton5());
	}
	
	//Campos de clase
	private static final long serialVersionUID = 1L;
}

class GestionadorDeRaton5 extends MouseAdapter {

	public void mouseDragged(MouseEvent e) {
		
		System.out.println("Est�s arrastrando el rat�n");
	}

	public void mouseMoved(MouseEvent e) {
		
		System.out.println("Est�s moviendo el rat�n");
	}
}