package eventos.clases_adaptadoras;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JFrame;

public class EventosDeRaton4 {

	public static void main(String[] args) {
		
		MarcoRaton4 marco = new MarcoRaton4();
		
		marco.setVisible(true);
		marco.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

}

class MarcoRaton4 extends JFrame {
	
	//M�todos constructores
	public MarcoRaton4() {
		
		Dimension pantalla = Toolkit.getDefaultToolkit().getScreenSize();
		
		int ancho = pantalla.width;
		int alto = pantalla.height;
		
		setBounds(((ancho - 500) / 2), ((alto - 350) / 2), 500, 350);
		setTitle("Eventos de rat�n");
		
		addMouseListener(new GestionadorDeRaton4());
	}
	
	//Campos de clase
	private static final long serialVersionUID = 1L;
}

class GestionadorDeRaton4 extends MouseAdapter {
	
	public void mouseClicked(MouseEvent e) {
		
		//Detectar el n�mero de clicks
		switch(e.getClickCount()) {
			case 1: System.out.println("Has hecho un click"); break;
			case 2: System.out.println("Has hecho doble click"); break;
			default: System.out.println("Has hecho m�s de 2 clicks"); break;
		}
	}
	
}