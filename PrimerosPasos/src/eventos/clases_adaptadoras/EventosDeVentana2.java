package eventos.clases_adaptadoras;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JFrame;

public class EventosDeVentana2 {

	public static void main(String[] args) {
		
		MarcoAccionesV2 marco = new MarcoAccionesV2();
		MarcoAccionesV2 marco2 = new MarcoAccionesV2();
		
		marco.setVisible(true);
		marco2.setVisible(true);
		
		marco.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		marco2.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		
	}

}

class MarcoAccionesV2 extends JFrame {
	
	//M�todos Constructores
	public MarcoAccionesV2() {
		
		idSiguiente = id;
		
		Dimension pantalla = Toolkit.getDefaultToolkit().getScreenSize();
		
		int alto = pantalla.height;
		
		setBounds(((520 * idSiguiente) + 150), ((alto - 350) / 2), 
				500, 350);
		
		addWindowListener(new GestionadorDeAcciones2());
		
		id++;
		
		setTitle("Ventana " + id);
	}
	
	//Campos de clase
	private int idSiguiente;
	private static int id = 0;
	private static final long serialVersionUID = 1L;
}

/*Las clases adaptadoras son abstractas e implementan todas aquellas 
interfaces que tienen en su interior m�s de 3 m�todos, en ese sentido
resulta bastante c�modo crear objetos oyentes, ya que solo debemos definir
los m�todos que vamos a utilizar, para ello simplemente heredamos de la 
clase adaptadora y sobreescribimos el m�todo de turno*/

class GestionadorDeAcciones2 extends WindowAdapter {

	public void windowIconified(WindowEvent e) {
		
		System.out.println("Ventana minimizada");
	}
	
}