package eventos.clases_adaptadoras;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JFrame;

public class EventosDeRaton3 {

	public static void main(String[] args) {
		
		MarcoRaton3 marco = new MarcoRaton3();
		
		marco.setVisible(true);
		marco.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

}

class MarcoRaton3 extends JFrame {
	
	//M�todos constructores
	public MarcoRaton3() {
		
		Dimension pantalla = Toolkit.getDefaultToolkit().getScreenSize();
		
		int ancho = pantalla.width;
		int alto = pantalla.height;
		
		setBounds(((ancho - 500) / 2), ((alto - 350) / 2), 500, 350);
		setTitle("Eventos de rat�n");
		
		addMouseListener(new GestionadorDeRaton3());
	}
	
	//Campos de clase
	private static final long serialVersionUID = 1L;
}

class GestionadorDeRaton3 extends MouseAdapter {
	
	public void mouseClicked(MouseEvent e) {
		
		switch(e.getButton()) {
			case MouseEvent.BUTTON1: 
				System.out.println("Has hecho click izquierdo");
			break;
			
			case MouseEvent.BUTTON2: 
				System.out.println("Has hecho click en la rueda del "
						+ "rat�n");
			break;
			
			case MouseEvent.BUTTON3: 
				System.out.println("Has hecho click derecho");
			break;
		}
		
	}
	
}