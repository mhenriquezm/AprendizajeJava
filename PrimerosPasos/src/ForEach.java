import javax.swing.*;

public class ForEach {

	public static void main(String[] args) {
		
		//Declaraci�n de un array
		String[] paises = new String[8];
		
		//Contador de los elementos
		int i;
		
		//Rellenar el array
		for(i=0;i<8;i++) {
			
			paises[i] = JOptionPane.showInputDialog((i+1) 
				+ ". Ingrese un pa�s"
			);
		}
		
		//Formatear el contador
		i = 0;
		
		//Recorrido con el for each variable tipo array : array
		for(String j:paises) {
			
			System.out.println((++i) + ". Pa�s: " + j);
		}
	}

}
