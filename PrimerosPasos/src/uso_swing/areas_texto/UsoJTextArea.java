package uso_swing.areas_texto;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

public class UsoJTextArea {

	public static void main(String[] args) {
		
		MarcoAreaTexto marco = new MarcoAreaTexto();
		marco.setVisible(true);
		marco.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

}

class MarcoAreaTexto extends JFrame {
	
	//M�todos constructores
	public MarcoAreaTexto() {
		
		Dimension pantalla = Toolkit.getDefaultToolkit().getScreenSize();
		
		int ancho = pantalla.width;
		int alto = pantalla.height;
		
		setTitle("Uso de JTextArea");
		setMinimumSize(new Dimension(300, 210));
		setBounds(((ancho - 500) / 2), ((alto - 350) / 2), 500, 350);
		
		add(new LaminaAreaTexto());
	}
	
	//Campos de clase
	private static final long serialVersionUID = 1L;
}

class LaminaAreaTexto extends JPanel {
	
	//M�todos constructores
	public LaminaAreaTexto() {
		
		/*Para crear un �rea de texto convencional se instancia la calse
		 * JTextArea que crea el �rea de texto tradicional, pero no tiene
		 * el t�pico comportamiento de las barras de scroll, para eso
		 * debemos instanciar la clase JScrollPane que es una l�mina
		 * especial donde insertaremos el �rea de texto y la instancia
		 * de JScrollPane se encargar� de establecer las barras de scroll
		 * cuando sean necesarias, finalmente se agrega dicha instancia*/
		
		areaTexto = new JTextArea(10,20);
		JScrollPane laminaBarras = new JScrollPane(areaTexto);
		JButton boton = new JButton("Enviar");
		
		//Establecemos saltos de l�nea autom�ticos
		areaTexto.setLineWrap(true);
		
		boton.addActionListener(new ImprimirTexto());
		
		add(laminaBarras);
		add(boton);
	}
	
	private class ImprimirTexto implements ActionListener {
		
		public void actionPerformed(ActionEvent e) {
			
			System.out.println(areaTexto.getText());
		}
		
	}
	
	//Campos de clase
	private JTextArea areaTexto;
	private static final long serialVersionUID = 1L;
}