package uso_swing.areas_texto;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

public class PracticaSaltosLinea {

	public static void main(String[] args) {
		
		MarcoSaltosLinea marco = new MarcoSaltosLinea();
		marco.setVisible(true);
		marco.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

}

class MarcoSaltosLinea extends JFrame {
	
	//M�todos constructores
	public MarcoSaltosLinea() {
		
		Dimension pantalla = Toolkit.getDefaultToolkit().getScreenSize();
		
		int ancho = pantalla.width;
		int alto = pantalla.height;
		
		setTitle("Uso de JTextArea");
		setMinimumSize(new Dimension(300, 210));
		setBounds(((ancho - 500) / 2), ((alto - 350) / 2), 500, 350);
		
		JTextArea areaTexto = new JTextArea();
		JScrollPane laminaBarras = new JScrollPane(areaTexto);
		
		add(laminaBarras, BorderLayout.CENTER);
		
		agregarLaminaBotones(areaTexto);
	}
	
	private void agregarLaminaBotones(final JTextArea areaTexto) {
		
		JPanel laminaBotones = new JPanel();
		
		JButton botonTexto = new JButton("Agregar texto");
		final JButton botonSaltos = new JButton("Agregar saltos de l�nea");
		
		//Creamos una clase interna local an�nima
		botonTexto.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				
				areaTexto.append("Te amo Raquel ");
			}
			
		});
		
		botonSaltos.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				
				saltos = !areaTexto.getLineWrap();
				
				/*Operador ternario, eval�a la condici�n y retorna un 
				 * resultado*/
				
				botonSaltos.setText(
					saltos 
					? "Quitar saltos de l�nea" 
					: "Agregar saltos de l�nea"
				);
				
				areaTexto.setLineWrap(saltos);
			}
			
			//Campos de clase
			private boolean saltos;
		});
		
		laminaBotones.add(botonTexto);
		laminaBotones.add(botonSaltos);
		
		add(laminaBotones, BorderLayout.SOUTH);
	}
	
	//Campos de clase
	private static final long serialVersionUID = 1L;
}