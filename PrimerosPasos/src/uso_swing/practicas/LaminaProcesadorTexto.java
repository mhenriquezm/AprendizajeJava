package uso_swing.practicas;

import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;

public class LaminaProcesadorTexto extends JPanel {
	
	//M�todos constructores
	public LaminaProcesadorTexto() {
		
		setLayout(new BorderLayout());
		
		areaTexto = new JTextPane();
		
		JScrollPane laminaScroll = new JScrollPane(
			areaTexto, 
			JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, 
			JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED
		);
		
		agregarLaminaMenu();
		
		add(laminaScroll, BorderLayout.CENTER);
	}
	
	//M�todos Setter
	private void agregarLaminaMenu() {
		
		JPanel laminaMenu = new JPanel();
		String[] opcionesMenu = {"Fuente", "Estilo", "Tama�o"};
		
		configurarLamina(laminaMenu, opcionesMenu);
		
		add(laminaMenu, BorderLayout.NORTH);
	}
	
	private void configurarLamina(JPanel laminaMenu, String[] opciones) {
		
		JMenuBar barraMenu = new JMenuBar();
		GestionarEventos oyente = new GestionarEventos();
		
		for(String i: opciones) {
			
			JMenu menu = new JMenu(i);
			
			switch(i) {
				case "Fuente":
					agregarItems(menu, new String[] {
						"Arial",
						"Courier",
						"Verdana"
					}, false, oyente);
				break;
				
				case "Estilo":
					agregarItems(menu, new String[] {
						"Negrita",
						"Cursiva"
					}, false, oyente);
				break;
				
				case "Tama�o":
					agregarItems(menu, new String[] {
						"12",
						"16",
						"20",
						"24"
					}, false, oyente);
				break;
			}
			
			barraMenu.add(menu);
		}
		
		laminaMenu.add(barraMenu);
	}
	
	private void agregarItems(JMenu menu, String[] items, 
			boolean separador, GestionarEventos oyente) {
		
		for(String i: items) {
			
			JMenuItem item = new JMenuItem(i);
			
			item.addActionListener(oyente);
			
			menu.add(item);
		}
		
		if(separador) menu.addSeparator();
	}
	
	//Clases internas
	private class GestionarEventos implements ActionListener {
		
		public void actionPerformed(ActionEvent e) {
			
			fuente = areaTexto.getFont();
			comando = e.getActionCommand();
			
			switch(comando) {
				case "Arial":
				case "Courier":
				case "Verdana":
					
					tipo = comando;
					estilo = fuente.getStyle();
					tamanio = fuente.getSize();
				break;
				
				case "Negrita":
				case "Cursiva":
					
					tipo = fuente.getName();
					estilo = devolverEstilo(comando);
					tamanio = fuente.getSize();
				break;
				
				case "12":
				case "16":
				case "20":
				case "24":
					
					tipo = fuente.getName();
					estilo = fuente.getStyle();
					tamanio = Integer.parseInt(comando);
				break;
			}
			
			areaTexto.setFont(new Font(tipo, estilo, tamanio));
		}
		
		public int devolverEstilo(String comando) {
			
			int estilo = Font.PLAIN;
			int anterior = fuente.getStyle();
			
			switch(comando) {
				case "Negrita":
					
					if(anterior == Font.BOLD) {
						return estilo;
					}
					else if(anterior < (Font.BOLD + Font.ITALIC)) {
						
						estilo = Font.BOLD + anterior;
					}
					else {
						estilo = anterior - Font.BOLD;
					}
				break;
				
				case "Cursiva": 
					
					if(anterior == Font.ITALIC) {
						
						return estilo;
					}
					else if(anterior < (Font.BOLD + Font.ITALIC)) {
						
						estilo = Font.ITALIC + anterior;
					}
					else {
						estilo = anterior - Font.ITALIC;
					}
				break;
			}
			
			return estilo;
		}
		
		//Campos de clase
		private String comando, tipo;
		private int estilo, tamanio;
	}
	
	//Campos de clase
	private Font fuente;
	private JTextPane areaTexto;
	private static final long serialVersionUID = 1L;
}
