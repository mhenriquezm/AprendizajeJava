package uso_swing.control_deslizante;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Toolkit;

import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

public class UsoChangeListener {

	public static void main(String[] args) {
		
		MarcoChangeListener marco = new MarcoChangeListener();
		marco.setVisible(true);
		marco.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

}

class MarcoChangeListener extends JFrame {
	
	//M�todos constructores
	public MarcoChangeListener() {
		
		Dimension pantalla = Toolkit.getDefaultToolkit().getScreenSize();
		
		int ancho = pantalla.width;
		int alto = pantalla.height;
		
		setTitle("Control deslizante");
		setBounds(((ancho - 550) / 2), ((alto - 350) / 2), 550, 350);
		
		add(new LaminaChangeListener());
	}
	
	//Campos de clase
	private static final long serialVersionUID = 1L;
}

class LaminaChangeListener extends JPanel {
	
	//M�todos constructores
	public LaminaChangeListener() {
		
		setLayout(new BorderLayout());
		
		JLabel rotulo = new JLabel("Te amo Raquel", JLabel.CENTER);
		
		agregarLaminaControl(rotulo);
		add(rotulo, BorderLayout.CENTER);
	}
	
	//M�todos Setter
	private void agregarLaminaControl(final JLabel rotulo) {
		
		JPanel laminaControl = new JPanel();
		final JSlider control = new JSlider(8, 48, 12);
		ChangeListener oyente = new ChangeListener() {
			
			public void stateChanged(ChangeEvent e) {
				
				valor = control.getValue();
				rotulo.setFont(new Font(Font.DIALOG, Font.BOLD, valor));
				
			}
			
			private int valor;
		};
		
		
		
		configurarControl(control, oyente);
		agregarComponente(laminaControl, control);
		
		add(laminaControl, BorderLayout.NORTH);
	}
	
	private void configurarControl(JSlider control, ChangeListener oyente)
	{
		control.setMinorTickSpacing(2);
		control.setMajorTickSpacing(8);
		
		control.setPaintTicks(true);
		control.setPaintLabels(true);
		control.setFont(new Font(Font.SERIF, Font.ITALIC, 12));
		
		control.addChangeListener(oyente);
	}
	
	public void agregarComponente(JPanel lamina, JComponent c) {
		
		lamina.add(c);
	}
	
	//Campos de clase
	private static final long serialVersionUID = 1L;
}