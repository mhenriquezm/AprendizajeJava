package uso_swing.control_deslizante;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.Toolkit;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JSlider;

public class UsoJSlider {

	public static void main(String[] args) {
		
		MarcoDeslizante marco = new MarcoDeslizante();
		marco.setVisible(true);
		marco.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

}

class MarcoDeslizante extends JFrame {
	
	//M�todos constructores
	public MarcoDeslizante() {
		
		Dimension pantalla = Toolkit.getDefaultToolkit().getScreenSize();
		
		int ancho = pantalla.width;
		int alto = pantalla.height;
		
		setTitle("Control deslizante");
		setBounds(((ancho - 550) / 2), ((alto - 350) / 2), 550, 350);
		
		add(new LaminaDeslizante());
	}
	
	//Campos de clase
	private static final long serialVersionUID = 1L;
}

class LaminaDeslizante extends JPanel {
	
	//M�todos constructores
	public LaminaDeslizante() {
		
		JSlider control = new JSlider(0, 100, 25);
		
		//Primero se establecen las marcas y se imprimen
		control.setMajorTickSpacing(25);
		control.setMinorTickSpacing(5);
		control.setPaintTicks(true);
		
		control.setFont(new Font(Font.SERIF, Font.ITALIC, 12));
		
		//Despu�s se establecen los intervalos y se imprimen
		control.setPaintLabels(true);
		
		//Lleva el control a la siguiente marca
		control.setSnapToTicks(true);
		
		add(control);
	}
	
	//Campos de clase
	private static final long serialVersionUID = 1L;
}