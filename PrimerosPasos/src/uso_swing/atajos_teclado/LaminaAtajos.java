package uso_swing.atajos_teclado;

import java.awt.BorderLayout;
import java.awt.event.KeyEvent;

import javax.swing.ButtonGroup;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JRadioButtonMenuItem;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;
import javax.swing.KeyStroke;
import javax.swing.text.StyledEditorKit;

public class LaminaAtajos extends JPanel {
	
	//M�todos constructores
	public LaminaAtajos() {
		
		setLayout(new BorderLayout());
		
		areaTexto = new JTextPane();
		grupo = new ButtonGroup();
		emergente = new JPopupMenu();
		
		JScrollPane laminaScroll = new JScrollPane(
			areaTexto, 
			JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, 
			JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED
		);
		
		agregarLaminaMenu();
		agregarItems(emergente, new String[] {"Negrita", "Cursiva"}, 2);
		
		areaTexto.setComponentPopupMenu(emergente);
		
		add(laminaScroll, BorderLayout.CENTER);
	}
	
	//M�todos Setter
	private void agregarLaminaMenu() {
		
		JPanel laminaMenu = new JPanel();
		String[] opcionesMenu = {"Fuente", "Estilo", "Tama�o"};
		
		configurarLamina(laminaMenu, opcionesMenu);
		
		add(laminaMenu, BorderLayout.NORTH);
	}
	
	private void configurarLamina(JPanel laminaMenu, String[] opciones) {
		
		JMenuBar barraMenu = new JMenuBar();
		
		for(String i: opciones) {
			
			JMenu menu = new JMenu(i);
			
			switch(i) {
				case "Fuente":
					agregarItems(menu, new String[] {
						"Arial",
						"Courier",
						"Verdana"
					}, false);
				break;
				
				case "Estilo":
					agregarItems(menu, new String[] {
						"Negrita", "Cursiva"
					}, 2);
				break;
				
				case "Tama�o":
					agregarItems(menu, new String[] {
						"12",
						"16",
						"20",
						"24"
					}, false);
				break;
			}
			
			barraMenu.add(menu);
		}
		
		laminaMenu.add(barraMenu);
	}
	
	private void agregarItems(JMenu menu, String[] items, 
			boolean separador) {
		
		for(String i: items) {
			
			JMenuItem item = new JMenuItem(i);
			
			switch(menu.getText()) {
				case "Fuente":
					item.addActionListener(
						new StyledEditorKit
						.FontFamilyAction("CambiarF", i));
				break;
				
				case "Estilo":
					switch(i) {
						case "Negrita": 
							item.addActionListener(
									new StyledEditorKit.BoldAction());
						break;
						
						case "Cursiva": 
							item.addActionListener(
									new StyledEditorKit.ItalicAction());
						break;
					}
				break;
			
				case "Tama�o":
					item = new JRadioButtonMenuItem(i);
					
					grupo.add(item);
					
					item.addActionListener(
						new StyledEditorKit
						.FontSizeAction("CambiarT", Integer.parseInt(i)));
				break;
			}
			
			menu.add(item);
		}
		
		if(separador) menu.addSeparator();
	}
	
	private void agregarItems(JMenu menu, String[] items, int tipo) {
		
		for(String i: items) {
			
			JMenuItem item = new JMenuItem();
			
			switch(tipo) {
				case 1:
					item = new JCheckBoxMenuItem(i);
				break;
				
				case 2:
					item = new JMenuItem(i);
				break;
			}
			
			switch(menu.getText()) {
				case "Estilo":
					switch(i) {
						case "Negrita": 
							//Este m�todo establece un atajo especificado
							item.setAccelerator(KeyStroke.getKeyStroke(
									KeyEvent.VK_N, KeyEvent.CTRL_DOWN_MASK
								)
							);
							
							item.addActionListener(
									new StyledEditorKit.BoldAction());
						break;
						
						case "Cursiva": 
							item.setAccelerator(KeyStroke.getKeyStroke(
									KeyEvent.VK_K, KeyEvent.CTRL_DOWN_MASK
								)
							);
							
							item.addActionListener(
									new StyledEditorKit.ItalicAction());
						break;
					}
				break;
			}
			
			menu.add(item);
		}
	}
	
	private void agregarItems(JPopupMenu menu, String[] items, int tipo) {
		
		for(String i: items) {
			
			JMenuItem item = new JMenuItem();
			
			switch(tipo) {
				case 1:
					item = new JCheckBoxMenuItem(i);
				break;
				
				case 2:
					item = new JMenuItem(i);
				break;
			}
			
			switch(i) {
				case "Negrita": 
					item.addActionListener(
							new StyledEditorKit.BoldAction());
				break;
				
				case "Cursiva": 
					item.addActionListener(
							new StyledEditorKit.ItalicAction());
				break;
			}
			
			menu.add(item);
		}
	}
	
	//Campos de clase
	private JTextPane areaTexto;
	private ButtonGroup grupo;
	private JPopupMenu emergente;
	private static final long serialVersionUID = 1L;
}