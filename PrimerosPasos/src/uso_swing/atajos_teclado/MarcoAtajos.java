package uso_swing.atajos_teclado;

import java.awt.Dimension;
import java.awt.Toolkit;

import javax.swing.JFrame;

public class MarcoAtajos extends JFrame {
	
	//M�todos constructores
	public MarcoAtajos() {
		
		Dimension pantalla = Toolkit.getDefaultToolkit().getScreenSize();
		
		int ancho = pantalla.width;
		int alto = pantalla.height;
		
		setTitle("Atajos de teclado");
		setBounds(((ancho - 550) / 2), ((alto - 350) / 2), 550, 350);
		
		add(new LaminaAtajos());
	}
	
	//Campos de clase
	private static final long serialVersionUID = 1L;
}
