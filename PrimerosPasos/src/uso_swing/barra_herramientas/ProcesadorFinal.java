package uso_swing.barra_herramientas;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.KeyEvent;
import java.io.File;

import javax.swing.ButtonGroup;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JRadioButtonMenuItem;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;
import javax.swing.JToolBar;
import javax.swing.KeyStroke;
import javax.swing.text.StyledEditorKit;

public class ProcesadorFinal {

	public static void main(String[] args) {
		
		MarcoProcesadorFinal marco = new MarcoProcesadorFinal();
		marco.setVisible(true);
		marco.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

}

class MarcoProcesadorFinal extends JFrame {
	
	//M�todos constructores
	public MarcoProcesadorFinal() {
		
		Dimension pantalla = Toolkit.getDefaultToolkit().getScreenSize();
		
		int ancho = pantalla.width;
		int alto = pantalla.height;
		
		setTitle("Procesador de textos MyR V5");
		setBounds(((ancho - 550) / 2), ((alto - 450) / 2), 550, 450);
		
		add(new LaminaProcesadorFinal());
	}
	
	//Campos de clase
	private static final long serialVersionUID = 1L;
}

class LaminaProcesadorFinal extends JPanel {
	
	//M�todos constructores
	public LaminaProcesadorFinal() {
		
		setLayout(new BorderLayout());
		
		areaTexto = new JTextPane();
		grupo = new ButtonGroup();
		emergente = new JPopupMenu();
		
		JScrollPane laminaScroll = new JScrollPane(
			areaTexto, 
			JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, 
			JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED
		);
		
		agregarLaminaMenu();
		agregarItems(emergente, new String[] {"Negrita", "Cursiva"}, 2);
		
		areaTexto.setComponentPopupMenu(emergente);
		
		agregarBarraHerramientas();
		
		add(laminaScroll, BorderLayout.CENTER);
	}
	
	//M�todos Setter
	private void agregarLaminaMenu() {
		
		JPanel laminaMenu = new JPanel();
		String[] opcionesMenu = {"Fuente", "Estilo", "Tama�o"};
		
		configurarLamina(laminaMenu, opcionesMenu);
		
		add(laminaMenu, BorderLayout.NORTH);
	}
	
	private void configurarLamina(JPanel laminaMenu, String[] opciones) {
		
		JMenuBar barraMenu = new JMenuBar();
		
		for(String i: opciones) {
			
			JMenu menu = new JMenu(i);
			
			switch(i) {
				case "Fuente":
					agregarItems(menu, new String[] {
						"Arial",
						"Courier",
						"Verdana"
					}, false);
				break;
				
				case "Estilo":
					agregarItems(menu, new String[] {
						"Negrita", "Cursiva"
					}, 2);
				break;
				
				case "Tama�o":
					agregarItems(menu, new String[] {
						"12",
						"16",
						"20",
						"24"
					}, false);
				break;
			}
			
			barraMenu.add(menu);
		}
		
		laminaMenu.add(barraMenu);
	}
	
	private void agregarItems(JMenu menu, String[] items, 
			boolean separador) {
		
		for(String i: items) {
			
			JMenuItem item = new JMenuItem(i);
			
			switch(menu.getText()) {
				case "Fuente":
					item.addActionListener(
						new StyledEditorKit
						.FontFamilyAction("CambiarF", i));
				break;
				
				case "Estilo":
					switch(i) {
						case "Negrita": 
							item.addActionListener(
									new StyledEditorKit.BoldAction());
						break;
						
						case "Cursiva": 
							item.addActionListener(
									new StyledEditorKit.ItalicAction());
						break;
					}
				break;
			
				case "Tama�o":
					item = new JRadioButtonMenuItem(i);
					
					grupo.add(item);
					
					item.addActionListener(
						new StyledEditorKit
						.FontSizeAction("CambiarT", Integer.parseInt(i)));
				break;
			}
			
			menu.add(item);
		}
		
		if(separador) menu.addSeparator();
	}
	
	private void agregarItems(JMenu menu, String[] items, int tipo) {
		
		for(String i: items) {
			
			JMenuItem item = new JMenuItem();
			
			switch(tipo) {
				case 1:
					item = new JCheckBoxMenuItem(i);
				break;
				
				case 2:
					item = new JMenuItem(i);
				break;
			}
			
			switch(menu.getText()) {
				case "Estilo":
					switch(i) {
						case "Negrita": 
							//Este m�todo establece un atajo especificado
							item.setAccelerator(KeyStroke.getKeyStroke(
									KeyEvent.VK_N, KeyEvent.CTRL_DOWN_MASK
								)
							);
							
							item.addActionListener(
									new StyledEditorKit.BoldAction());
						break;
						
						case "Cursiva": 
							item.setAccelerator(KeyStroke.getKeyStroke(
									KeyEvent.VK_K, KeyEvent.CTRL_DOWN_MASK
								)
							);
							
							item.addActionListener(
									new StyledEditorKit.ItalicAction());
						break;
					}
				break;
			}
			
			menu.add(item);
		}
	}
	
	private void agregarItems(JPopupMenu menu, String[] items, int tipo) {
		
		for(String i: items) {
			
			JMenuItem item = new JMenuItem();
			
			switch(tipo) {
				case 1:
					item = new JCheckBoxMenuItem(i);
				break;
				
				case 2:
					item = new JMenuItem(i);
				break;
			}
			
			switch(i) {
				case "Negrita": 
					item.addActionListener(
							new StyledEditorKit.BoldAction());
				break;
				
				case "Cursiva": 
					item.addActionListener(
							new StyledEditorKit.ItalicAction());
				break;
			}
			
			menu.add(item);
		}
	}
	
	private void agregarBarraHerramientas() {
		
		JToolBar barraHerramientas = new JToolBar(JToolBar.VERTICAL);
		
		Object[] items = {
			"Negrita", new ImageIcon(
				"src"
				+ File.separator + "uso_swing"
				+ File.separator + "barra_herramientas"
				+ File.separator + "negrita.gif"
			),
			"Cursiva", new ImageIcon(
				"src"
				+ File.separator + "uso_swing"
				+ File.separator + "barra_herramientas"
				+ File.separator + "cursiva.gif"
			),
			"Subrayado", new ImageIcon(
				"src"
				+ File.separator + "uso_swing"
				+ File.separator + "barra_herramientas"
				+ File.separator + "subrayado.gif"
			),
			"Amarillo", new ImageIcon(
				"src"
				+ File.separator + "uso_swing"
				+ File.separator + "barra_herramientas"
				+ File.separator + "bola-amarilla.jpg"
			),
			"Azul", new ImageIcon(
				"src"
				+ File.separator + "uso_swing"
				+ File.separator + "barra_herramientas"
				+ File.separator + "bola-azul.jpg"
			),
			"Rojo", new ImageIcon(
				"src"
				+ File.separator + "uso_swing"
				+ File.separator + "barra_herramientas"
				+ File.separator + "bola-roja.jpg"
			),
			"Izquierdo", new ImageIcon(
				"src"
				+ File.separator + "uso_swing"
				+ File.separator + "barra_herramientas"
				+ File.separator + "izquierdo.png"
			),
			"Centrado", new ImageIcon(
				"src"
				+ File.separator + "uso_swing"
				+ File.separator + "barra_herramientas"
				+ File.separator + "centrado.png"
			),
			"Derecho", new ImageIcon(
				"src"
				+ File.separator + "uso_swing"
				+ File.separator + "barra_herramientas"
				+ File.separator + "derecho.png"
			),
			"Justificado", new ImageIcon(
				"src"
				+ File.separator + "uso_swing"
				+ File.separator + "barra_herramientas"
				+ File.separator + "justificado.png"
			)
		};
		
		configurarBarraHerramientas(barraHerramientas, items);
		
		add(barraHerramientas, BorderLayout.WEST);
	}
	
	private void configurarBarraHerramientas(JToolBar barraHerramientas, 
		Object[] items) {
		
		int longitud = items.length;
		
		for(int i=0;i<longitud;i+=2) {
			
			JButton boton = new JButton((Icon) items[i + 1]);
			
			switch((String) items[i]) {
				case "Negrita":
					boton.addActionListener(new StyledEditorKit
							.BoldAction());
				break;
				
				case "Cursiva":
					boton.addActionListener(new StyledEditorKit
							.ItalicAction());
				break;
				
				case "Subrayado":
					boton.addActionListener(new StyledEditorKit
							.UnderlineAction());
				break;
				
				case "Amarillo":
					boton.addActionListener(new StyledEditorKit
							.ForegroundAction("AMARILLO", Color.YELLOW));
					
					barraHerramientas.addSeparator();
				break;
				
				case "Azul":
					boton.addActionListener(new StyledEditorKit
							.ForegroundAction("AZUL", Color.BLUE));
				break;
				
				case "Rojo":
					boton.addActionListener(new StyledEditorKit
							.ForegroundAction("ROJO", Color.RED));
				break;
				
				case "Izquierdo":
					boton.addActionListener(new StyledEditorKit
							.AlignmentAction("IZQUIERDO", 0));
					
					barraHerramientas.addSeparator();
				break;
				
				case "Centrado":
					boton.addActionListener(new StyledEditorKit
							.AlignmentAction("CENTRADO", 1));
				break;
				
				case "Derecho":
					boton.addActionListener(new StyledEditorKit
							.AlignmentAction("DERECHO", 2));
				break;
				
				case "Justificado":
					boton.addActionListener(new StyledEditorKit
							.AlignmentAction("JUSTIFICADO", 3));
				break;
			}
			
			barraHerramientas.add(boton);
		}
	}
	
	//Campos de clase
	private JTextPane areaTexto;
	private ButtonGroup grupo;
	private JPopupMenu emergente;
	private static final long serialVersionUID = 1L;
}