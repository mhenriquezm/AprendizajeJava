package uso_swing.barra_herramientas;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.io.File;

import javax.swing.AbstractAction;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JPanel;
import javax.swing.JToolBar;
import javax.swing.KeyStroke;

public class UsoJToolBar {

	public static void main(String[] args) {
		
		MarcoHerramientas marco = new MarcoHerramientas();
		marco.setVisible(true);
		marco.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

}

class MarcoHerramientas extends JFrame {
	
	//M�todos constructores
	public MarcoHerramientas() {
		
		Dimension pantalla = Toolkit.getDefaultToolkit().getScreenSize();
		
		int ancho = pantalla.width;
		int alto = pantalla.height;
		
		setTitle("Uso de barras de herramientas");
		setBounds(((ancho - 550) / 2), ((alto - 350) / 2), 550, 350);
		
		add(new LaminaBarraHerramientas(this));
	}
	
	//Campos de clase
	private static final long serialVersionUID = 1L;
}

class LaminaBarraHerramientas extends JPanel {
	
	//M�todos constructores
	public LaminaBarraHerramientas(JFrame marco) {
		
		setLayout(new BorderLayout());
		
		Object[] items = {
			"Amarillo", new ImageIcon(
				"src"
				+ File.separator + "uso_swing"
				+ File.separator + "barra_herramientas"
				+ File.separator + "bola-amarilla.jpg"
			), Color.YELLOW, "ctrl A",
			"Azul", new ImageIcon(
				"src"
				+ File.separator + "uso_swing"
				+ File.separator + "barra_herramientas"
				+ File.separator + "bola-azul.jpg"
			), Color.BLUE, "ctrl B",
			"Rojo", new ImageIcon(
				"src"
				+ File.separator + "uso_swing"
				+ File.separator + "barra_herramientas"
				+ File.separator + "bola-roja.jpg"
			), Color.RED, "ctrl R"
		};
		
		this.marco = marco;
		
		agregarBarraMenu(items);
		agregarBarraHerramientas(items);
	}
	
	//M�todos Setter
	private void agregarBarraMenu(Object[] items) {
		
		JMenuBar barraMenu = new JMenuBar();
		JMenu menu = new JMenu("Colores");
		
		int longitud = items.length;
		
		for(int i=0;i<longitud;i+=4) {
			
			AbstractAction accion = devolverAccion(
				(String) items[i], 
				(Icon) items[i + 1], 
				(Color) items[i + 2]
			);
			
			asignarCombinaciones(
				JPanel.WHEN_IN_FOCUSED_WINDOW,
				(String) items[i + 3],
				("Objecto" + i), 
				accion
			);
			
			//Asignamos cada acci�n al men�
			menu.add(accion);
		}
		
		//Asignamos el men� a la barra
		barraMenu.add(menu);
		
		//Establecemos el men� en el marco
		marco.setJMenuBar(barraMenu);
	}
	
	private void agregarBarraHerramientas(Object[] items) {
		
		JToolBar barraHerramientas = new JToolBar(JToolBar.VERTICAL);
		
		int longitud = items.length;
		
		for(int i=0;i<longitud;i+=4) {
			
			AbstractAction accion = devolverAccion(
				(String) items[i], 
				(Icon) items[i + 1], 
				(Color) items[i + 2]
			);
			
			asignarCombinaciones(
				JPanel.WHEN_IN_FOCUSED_WINDOW,
				(String) items[i + 3],
				("Objecto" + i), 
				accion
			);
			
			//Asignamos cada acci�n al men�
			barraHerramientas.add(accion);
		}
		
		barraHerramientas.addSeparator();
		
		//Bot�n de salida
		barraHerramientas.add(new AbstractAction("Salir", 
			new ImageIcon(
				"src"
				+ File.separator + "uso_swing"
				+ File.separator + "barra_herramientas"
				+ File.separator + "salir.gif"
			)) {
			
			public void actionPerformed(ActionEvent e) {
				
				System.exit(0);
			}
			
			//Campos de clase
			private static final long serialVersionUID = 1L;
		});
		
		add(barraHerramientas, BorderLayout.EAST);
	}
	
	public void asignarCombinaciones(int condicion, String combinacion, 
			String objeto, AbstractAction accion) {
		
		getInputMap(condicion).put(KeyStroke.getKeyStroke(combinacion), 
				objeto);
		
		getActionMap().put(objeto, accion);
	}
	
	//M�todos Getter
	public AbstractAction devolverAccion(String nombre, Icon icono, 
			Color color) {
		
		class GestionarAcciones extends AbstractAction {
			
			//M�todos constructores
			public GestionarAcciones(String nombre, Icon icono, 
					Color color) {
				
				putValue(AbstractAction.NAME, nombre);
				putValue(AbstractAction.SMALL_ICON, icono);
				putValue(AbstractAction.SHORT_DESCRIPTION, 
						"Coloca la ventana de color " + nombre);
				putValue("ACCION", color);
				
			}
			
			//M�todos Setter
			public void actionPerformed(ActionEvent e) {
				
				setBackground((Color) getValue("ACCION"));
			}
			
			//Campos de clase
			private static final long serialVersionUID = 1L;
		}
		
		return new GestionarAcciones(nombre, icono, color);
	}
	
	//Campos de clase
	private JFrame marco;
	private static final long serialVersionUID = 1L;
}