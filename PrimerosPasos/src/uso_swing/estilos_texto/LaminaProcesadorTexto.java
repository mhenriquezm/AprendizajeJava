package uso_swing.estilos_texto;

import java.awt.BorderLayout;
import java.io.File;

import javax.swing.ImageIcon;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;
import javax.swing.text.StyledEditorKit;

public class LaminaProcesadorTexto extends JPanel {
	
	//M�todos constructores
	public LaminaProcesadorTexto() {
		
		setLayout(new BorderLayout());
		
		areaTexto = new JTextPane();
		
		JScrollPane laminaScroll = new JScrollPane(
			areaTexto, 
			JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, 
			JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED
		);
		
		agregarLaminaMenu();
		
		add(laminaScroll, BorderLayout.CENTER);
	}
	
	//M�todos Setter
	private void agregarLaminaMenu() {
		
		JPanel laminaMenu = new JPanel();
		String[] opcionesMenu = {"Fuente", "Estilo", "Tama�o"};
		
		configurarLamina(laminaMenu, opcionesMenu);
		
		add(laminaMenu, BorderLayout.NORTH);
	}
	
	private void configurarLamina(JPanel laminaMenu, String[] opciones) {
		
		JMenuBar barraMenu = new JMenuBar();
		
		for(String i: opciones) {
			
			JMenu menu = new JMenu(i);
			
			switch(i) {
				case "Fuente":
					agregarItems(menu, new String[] {
						"Arial",
						"Courier",
						"Verdana"
					}, false);
				break;
				
				case "Estilo":
					agregarItems(menu, new Object[] {
						"Negrita",
						new ImageIcon(
							"src"
							+ File.separator + "uso_swing"
							+ File.separator + "estilos_texto"
							+ File.separator + "negrita.gif"
						),
						"Cursiva",
						new ImageIcon(
							"src"
							+ File.separator + "uso_swing"
							+ File.separator + "estilos_texto"
							+ File.separator + "cursiva.gif"
						)
					});
				break;
				
				case "Tama�o":
					agregarItems(menu, new String[] {
						"12",
						"16",
						"20",
						"24"
					}, false);
				break;
			}
			
			barraMenu.add(menu);
		}
		
		laminaMenu.add(barraMenu);
	}
	
	private void agregarItems(JMenu menu, String[] items, 
			boolean separador) {
		
		for(String i: items) {
			
			JMenuItem item = new JMenuItem(i);
			
			switch(menu.getText()) {
				case "Fuente":
					item.addActionListener(
						new StyledEditorKit
						.FontFamilyAction("CambiarF", i));
				break;
				
				case "Estilo":
					switch(i) {
						case "Negrita": 
							item.addActionListener(
									new StyledEditorKit.BoldAction());
						break;
						
						case "Cursiva": 
							item.addActionListener(
									new StyledEditorKit.ItalicAction());
						break;
					}
				break;
			
				case "Tama�o":
					item.addActionListener(
						new StyledEditorKit
						.FontSizeAction("CambiarT", Integer.parseInt(i)));
				break;
			}
			
			menu.add(item);
		}
		
		if(separador) menu.addSeparator();
	}
	
	private void agregarItems(JMenu menu, Object[] items) {
		
		int longitud = items.length;
		
		for(int i=0;i<longitud;i+=2) {
			
			String rotulo = String.valueOf(items[i]);
			ImageIcon icono = (ImageIcon) items[i + 1];
			JMenuItem item = new JMenuItem(rotulo, icono);
			
			switch(menu.getText()) {
				case "Estilo":
					switch(rotulo) {
						case "Negrita": 
							item.addActionListener(
									new StyledEditorKit.BoldAction());
						break;
						
						case "Cursiva": 
							item.addActionListener(
									new StyledEditorKit.ItalicAction());
						break;
					}
				break;
			}
			
			menu.add(item);
		}
	}
	
	//Campos de clase
	private JTextPane areaTexto;
	private static final long serialVersionUID = 1L;
}
