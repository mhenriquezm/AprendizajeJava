package uso_swing.check_box;

import java.awt.Dimension;
import java.awt.Toolkit;

import javax.swing.JFrame;

public class MarcoCheckBox extends JFrame {
	
	//M�todos constructores
	public MarcoCheckBox() {
		
		Dimension pantalla = Toolkit.getDefaultToolkit().getScreenSize();
		
		int ancho = pantalla.width;
		int alto = pantalla.height;
		
		setTitle("Uso de JCheckBox");
		setBounds(((ancho - 500) / 2), ((alto - 350) / 2), 500, 350);
		
		add(new LaminaCheckBox());
	}
	
	//Campos de clase
	private static final long serialVersionUID = 1L;
}