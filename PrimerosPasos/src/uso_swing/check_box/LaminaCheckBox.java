package uso_swing.check_box;

import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class LaminaCheckBox extends JPanel {
	
	//M�todos constructores
	public LaminaCheckBox() {
		
		setLayout(new BorderLayout());
		
		Font fuente = new Font(Font.SERIF, Font.PLAIN, 24);
		JLabel etiqueta = new JLabel("Te amo Raquel", JLabel.CENTER);
		
		etiqueta.setFont(fuente);
		
		add(etiqueta, BorderLayout.CENTER);
		
		agregarLaminaBotones(etiqueta);
	}
	
	//M�todos Setter
	private void agregarLaminaBotones(final JLabel etiqueta) {
		
		JPanel laminaBotones = new JPanel();
		final JCheckBox botonNegrita = new JCheckBox("Negrita");
		final JCheckBox botonCursiva = new JCheckBox("Cursiva");
		
		//Clase an�nima
		ActionListener oyente = new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				
				estilo = Font.PLAIN;
				
				//Comprobar si el bot�n est� seleccionado
				if(botonNegrita.isSelected())
					estilo += Font.BOLD;
				
				//Comprobar si el bot�n est� seleccionado
				if(botonCursiva.isSelected())
					estilo += Font.ITALIC;
				
				etiqueta.setFont(new Font(Font.SERIF, estilo, 24));
			}
			
			//Campos de clase
			private int estilo;
		};
		
		botonNegrita.addActionListener(oyente);
		botonCursiva.addActionListener(oyente);
		
		laminaBotones.add(botonNegrita);
		laminaBotones.add(botonCursiva);
		
		add(laminaBotones, BorderLayout.SOUTH);
	}
	
	//Campos de clase
	private static final long serialVersionUID = 1L;
}
