package uso_swing.eventos_documento;

import java.awt.Dimension;
import java.awt.Toolkit;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.Document;

public class UsoDocumentEvent {

	public static void main(String[] args) {
		
		MarcoDocumento marco = new MarcoDocumento();
		marco.setVisible(true);
		marco.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

}

class MarcoDocumento extends JFrame {
	
	//M�todos constructores
	public MarcoDocumento() {
		
		Dimension pantalla = Toolkit.getDefaultToolkit().getScreenSize();
		
		int ancho = pantalla.width;
		int alto = pantalla.height;
		
		setTitle("Uso de Document");
		setBounds(((ancho - 500) / 2), ((alto - 350) / 2), 500, 350);
		
		add(new LaminaDocumento());
	}
	
	//Campos de clase
	private static final long serialVersionUID = 1L;
}

class LaminaDocumento extends JPanel {
	
	//M�todos constructores
	public LaminaDocumento() {
		
		JTextField campo1 = new JTextField(20);
		
		//Objeto que representa el documento
		Document documento = campo1.getDocument();
		
		//Colocamos el modelo del documento a la escucha de cambios
		documento.addDocumentListener(new GestionarCambios());
		
		add(campo1);
	}
	
	//Clases internas
	private class GestionarCambios implements DocumentListener {
		
		public void changedUpdate(DocumentEvent e) {
			
		}
		
		public void insertUpdate(DocumentEvent e) {
			System.out.println("Has agregado texto");
		}
		
		public void removeUpdate(DocumentEvent e) {
			System.out.println("Has borrado texto");
		}
		
	}
	
	//Campos de clase
	private static final long serialVersionUID = 1L;
}