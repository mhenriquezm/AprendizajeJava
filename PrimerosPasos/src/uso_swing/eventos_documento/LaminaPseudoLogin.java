package uso_swing.eventos_documento;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

public class LaminaPseudoLogin extends JPanel {
	
	//M�todos constructores
	public LaminaPseudoLogin() {
		
		setLayout(new BorderLayout());
		
		clave = new JPasswordField();
		
		agregarCabecera();
		
		add(new JButton("Enviar"), BorderLayout.SOUTH);
	}
	
	//M�todos Setter
	private void agregarCabecera() {
		
		//Clase interna local
		class ValidarClave implements DocumentListener {
			
			public void changedUpdate(DocumentEvent e) {}
			
			public void insertUpdate(DocumentEvent e) {
				
				validar();
			}
			
			public void removeUpdate(DocumentEvent e) {
				
				validar();
			}
			
			public void validar() {
				
				longTexto = clave.getPassword().length;
				
				if((longTexto >= 8) && (longTexto <= 12)) {
					
					clave.setBackground(Color.WHITE);
				}
				else {
					
					clave.setBackground(Color.RED);
				}
			}
			
			//Campos de clase
			private int longTexto;
		}
		
		JPanel cabecera = new JPanel(new GridLayout(2,2));
		JTextField usuario = new JTextField();
		
		cabecera.add(devolverEtiqueta("Usuario:"));
		cabecera.add(usuario);
		cabecera.add(devolverEtiqueta("Contrase�a:"));
		cabecera.add(clave);
		
		//Capturar el documento y colocarlo a la escucha
		clave.getDocument().addDocumentListener(new ValidarClave());
		
		add(cabecera, BorderLayout.NORTH);
	}
	
	public JLabel devolverEtiqueta(String texto) {
		return new JLabel(texto);
	}
	
	//Campos de clase
	private JPasswordField clave;
	private static final long serialVersionUID = 1L;
}