package uso_swing.eventos_documento;

import java.awt.Dimension;
import java.awt.Toolkit;

import javax.swing.JFrame;

public class MarcoPseudoLogin extends JFrame {
	
	//M�todos constructores
	public MarcoPseudoLogin() {
		
		Dimension pantalla = Toolkit.getDefaultToolkit().getScreenSize();
		
		int ancho = pantalla.width;
		int alto = pantalla.height;
		
		setTitle("Uso de Document");
		setMinimumSize(new Dimension(300, 210));
		setBounds(((ancho - 400) / 2), ((alto - 280) / 2), 400, 280);
		
		add(new LaminaPseudoLogin());
	}
	
	//Campos de clase
	private static final long serialVersionUID = 1L;
}
