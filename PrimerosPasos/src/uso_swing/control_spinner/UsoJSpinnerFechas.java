package uso_swing.control_spinner;

import java.awt.Dimension;
import java.awt.Toolkit;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.SpinnerDateModel;

public class UsoJSpinnerFechas {

	public static void main(String[] args) {
		
		MarcoSpinnerFechas marco = new MarcoSpinnerFechas();
		marco.setVisible(true);
		marco.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

}

class MarcoSpinnerFechas extends JFrame {
	
	//M�todos constructores
	public MarcoSpinnerFechas() {
		
		Dimension pantalla = Toolkit.getDefaultToolkit().getScreenSize();
		
		int ancho = pantalla.width;
		int alto = pantalla.height;
		
		setTitle("Control spinner");
		setBounds(((ancho - 550) / 2), ((alto - 350) / 2), 550, 350);
		
		add(new LaminaSpinnerFechas());
	}
	
	//Campos de clase
	private static final long serialVersionUID = 1L;
}

class LaminaSpinnerFechas extends JPanel {
	
	//M�todos constructores
	public LaminaSpinnerFechas() {
		
		JSpinner control = new JSpinner(new SpinnerDateModel());
		
		//Se establece el tama�o por defecto del componente
		control.setPreferredSize(new Dimension(130, 20));
		
		add(control);
	}
	
	//Campos de clase
	private static final long serialVersionUID = 1L;
}