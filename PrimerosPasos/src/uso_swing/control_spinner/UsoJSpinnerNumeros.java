package uso_swing.control_spinner;

import java.awt.Dimension;
import java.awt.Toolkit;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;

public class UsoJSpinnerNumeros {

	public static void main(String[] args) {
		
		MarcoSpinnerNumeros marco = new MarcoSpinnerNumeros();
		marco.setVisible(true);
		marco.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

}

class MarcoSpinnerNumeros extends JFrame {
	
	//M�todos constructores
	public MarcoSpinnerNumeros() {
		
		Dimension pantalla = Toolkit.getDefaultToolkit().getScreenSize();
		
		int ancho = pantalla.width;
		int alto = pantalla.height;
		
		setTitle("Control spinner");
		setBounds(((ancho - 550) / 2), ((alto - 350) / 2), 550, 350);
		
		add(new LaminaSpinnerNumeros());
	}
	
	//Campos de clase
	private static final long serialVersionUID = 1L;
}

class LaminaSpinnerNumeros extends JPanel {
	
	//M�todos constructores
	public LaminaSpinnerNumeros() {
		
		//Controlamos los valores del spinner en n�meros
		JSpinner control = new JSpinner(new SpinnerNumberModel(
			5, 0, 100, 1
		));
		
		control.setPreferredSize(new Dimension(50, 20));
		
		add(control);
	}
	
	//Campos de clase
	private static final long serialVersionUID = 1L;
}