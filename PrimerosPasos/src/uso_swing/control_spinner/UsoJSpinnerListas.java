package uso_swing.control_spinner;

import java.awt.Dimension;
import java.awt.GraphicsEnvironment;
import java.awt.Toolkit;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.SpinnerListModel;

public class UsoJSpinnerListas {

	public static void main(String[] args) {
		
		MarcoSpinnerListas marco = new MarcoSpinnerListas();
		marco.setVisible(true);
		marco.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

}

class MarcoSpinnerListas extends JFrame {
	
	//M�todos constructores
	public MarcoSpinnerListas() {
		
		Dimension pantalla = Toolkit.getDefaultToolkit().getScreenSize();
		
		int ancho = pantalla.width;
		int alto = pantalla.height;
		
		setTitle("Control spinner");
		setBounds(((ancho - 550) / 2), ((alto - 350) / 2), 550, 350);
		
		add(new LaminaSpinnerListas());
	}
	
	//Campos de clase
	private static final long serialVersionUID = 1L;
}

class LaminaSpinnerListas extends JPanel {
	
	//M�todos constructores
	public LaminaSpinnerListas() {
		
		String[] lista = GraphicsEnvironment
				.getLocalGraphicsEnvironment()
				.getAvailableFontFamilyNames();
		
		JSpinner control = new JSpinner(new SpinnerListModel(lista));
		
		control.setPreferredSize(new Dimension(220, 20));
		
		add(control);
	}
	
	//Campos de clase
	private static final long serialVersionUID = 1L;
}