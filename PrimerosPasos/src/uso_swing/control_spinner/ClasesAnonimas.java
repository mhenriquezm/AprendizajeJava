package uso_swing.control_spinner;

import java.awt.Dimension;
import java.awt.Toolkit;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;

public class ClasesAnonimas {

	public static void main(String[] args) {
		
		MarcoClaseAnonima marco = new MarcoClaseAnonima();
		marco.setVisible(true);
		marco.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

}

class MarcoClaseAnonima extends JFrame {
	
	//M�todos constructores
	public MarcoClaseAnonima() {
		
		Dimension pantalla = Toolkit.getDefaultToolkit().getScreenSize();
		
		int ancho = pantalla.width;
		int alto = pantalla.height;
		
		setTitle("Control spinner");
		setBounds(((ancho - 550) / 2), ((alto - 350) / 2), 550, 350);
		
		add(new LaminaClaseAnonima());
	}
	
	//Campos de clase
	private static final long serialVersionUID = 1L;
}

class LaminaClaseAnonima extends JPanel {
	
	//M�todos constructores
	public LaminaClaseAnonima() {
		
		/*Creamos una clase an�nima que sobreescribe SpinnerNumberModel
		 * de esa forma cambiamos el comportamiento por defecto*/
		JSpinner control = new JSpinner(new SpinnerNumberModel(5,0,100,1) {
			
			public Object getNextValue() {
				return super.getPreviousValue();
			}
			
			public Object getPreviousValue() {
				return super.getNextValue();
			}
			
			//Campos de clase
			private static final long serialVersionUID = 1L;
		});
		
		control.setPreferredSize(new Dimension(50, 20));
		
		add(control);
	}
	
	//Campos de clase
	private static final long serialVersionUID = 1L;
}