package uso_swing.control_spinner;

import java.awt.Dimension;
import java.awt.Toolkit;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JSpinner;

public class UsoJSpinner {

	public static void main(String[] args) {
		
		MarcoSpinner marco = new MarcoSpinner();
		marco.setVisible(true);
		marco.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

}

class MarcoSpinner extends JFrame {
	
	//M�todos constructores
	public MarcoSpinner() {
		
		Dimension pantalla = Toolkit.getDefaultToolkit().getScreenSize();
		
		int ancho = pantalla.width;
		int alto = pantalla.height;
		
		setTitle("Control spinner");
		setBounds(((ancho - 550) / 2), ((alto - 350) / 2), 550, 350);
		
		add(new LaminaSpinner());
	}
	
	//Campos de clase
	private static final long serialVersionUID = 1L;
}

class LaminaSpinner extends JPanel {
	
	//M�todos constructores
	public LaminaSpinner() {
		
		//Controlamos los valores del spinner en n�meros
		JSpinner control = new JSpinner();
		
		control.setPreferredSize(new Dimension(50, 20));
		
		add(control);
	}
	
	//Campos de clase
	private static final long serialVersionUID = 1L;
}