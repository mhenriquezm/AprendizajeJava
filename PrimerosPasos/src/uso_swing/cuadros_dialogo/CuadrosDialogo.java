package uso_swing.cuadros_dialogo;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

public class CuadrosDialogo {

	public static void main(String[] args) {
		
		MarcoCuadrosDialogo marco = new MarcoCuadrosDialogo();
		marco.setVisible(true);
		marco.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

}

class MarcoCuadrosDialogo extends JFrame {
	
	//M�todos constructores
	public MarcoCuadrosDialogo() {
		
		Dimension pantalla = Toolkit.getDefaultToolkit().getScreenSize();
		
		int ancho = pantalla.width;
		int alto = pantalla.height;
		
		setTitle("Cuadros de dialogo");
		setBounds(((ancho - 550) / 2), ((alto - 350) / 2), 550, 350);
		
		add(new LaminaCuadrosDialogo());
	}
	
	//Campos de clase
	private static final long serialVersionUID = 1L;
}

class LaminaCuadrosDialogo extends JPanel {
	
	//M�todos constructores
	public LaminaCuadrosDialogo() {
		
		agregarBotones(new String[] {
			"Bot�n 1", "Bot�n 2", "Bot�n 3", "Bot�n 4"
		});
	}
	
	//M�todos Setter
	private void agregarBotones(String[] rotulos) {
		
		final Toolkit herramientas = Toolkit.getDefaultToolkit();
		
		for(String i: rotulos) {
			
			JButton boton = new JButton(i);
			
			boton.addActionListener(new ActionListener() {
				
				public void actionPerformed(ActionEvent e) {
					
					comando = e.getActionCommand();
					
					switch(comando) {
						case "Bot�n 1":
							herramientas.beep();
							
							JOptionPane.showMessageDialog(
								LaminaCuadrosDialogo.this,
								"Has pulsado el Bot�n 1",
								"Informaci�n",
								JOptionPane.INFORMATION_MESSAGE
							);
						break;
							
						case "Bot�n 2": 
							JOptionPane.showInputDialog(
								LaminaCuadrosDialogo.this,
								"�Qui�n es la luna de mi vida?",
								"Pregunta",
								JOptionPane.QUESTION_MESSAGE
							);
						break;
						
						case "Bot�n 3":
							JOptionPane.showConfirmDialog(
								LaminaCuadrosDialogo.this,
								"�Amas a Raquel?",
								"Pregunta",
								JOptionPane.YES_NO_CANCEL_OPTION
							);
						break;
						
						case "Bot�n 4":
							JOptionPane.showOptionDialog(
								LaminaCuadrosDialogo.this,
								"Mensaje de prueba",
								"Advertencia",
								JOptionPane.OK_CANCEL_OPTION,
								JOptionPane.WARNING_MESSAGE,
								null,
								null, 
								null
							);
						break;
					}//switch
				}
				
				//Campos de clase
				private String comando;
			});
			
			add(boton);
		}//for
	}
	
	//Campos de clase
	private static final long serialVersionUID = 1L;
}