package uso_swing.radio_button;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ButtonGroup;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;

public class PracticaTamanioLetra {

	public static void main(String[] args) {
		
		MarcoTamanioLetra marco = new MarcoTamanioLetra();
		marco.setVisible(true);
		marco.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

}

class MarcoTamanioLetra extends JFrame {
	
	//M�todos constructores
	public MarcoTamanioLetra() {
		
		Dimension pantalla = Toolkit.getDefaultToolkit().getScreenSize();
		
		int ancho = pantalla.width;
		int alto = pantalla.height;
		
		setTitle("Uso de JRadioButton");
		setBounds(((ancho - 500) / 2), ((alto - 350) / 2), 500, 350);
		
		add(new LaminaTamanioLetra());
	}
	
	//Campos de clase
	private static final long serialVersionUID = 1L;
}

class LaminaTamanioLetra extends JPanel {
	
	//M�todos constructores
	public LaminaTamanioLetra() {
		
		setLayout(new BorderLayout());
		
		//Fuente por defecto
		Font fuente = new Font(Font.DIALOG, Font.BOLD, 16);
		etiqueta = new JLabel("Te amo Raquel", JLabel.CENTER);
		
		etiqueta.setFont(fuente);
		
		add(etiqueta, BorderLayout.CENTER);
		agregarLaminaBotones();
	}
	
	//M�todos Setter
	private void agregarLaminaBotones() {
		
		JPanel laminaBotones = new JPanel();
		String[] tamanios = {
			"Peque�o", "Mediano", "Grande", "Extra grande"
		};
		
		agregarBotones(laminaBotones, tamanios);
		
		add(laminaBotones, BorderLayout.SOUTH);
	}
	
	private void agregarBotones(JPanel lamina, String[] nombres) {
		
		ButtonGroup grupoTamanios = new ButtonGroup();
		ActionListener oyente = new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				
				switch(e.getActionCommand()) {
					case "Peque�o": 
						etiqueta.setFont(
							new Font(Font.DIALOG, Font.BOLD, 12)
						); 
					break;
					
					case "Mediano": 
						etiqueta.setFont(
							new Font(Font.DIALOG, Font.BOLD, 16)
						); 
					break;
					
					case "Grande": 
						etiqueta.setFont(
							new Font(Font.DIALOG, Font.BOLD, 20)
						); 
					break;
					
					case "Extra grande": 
						etiqueta.setFont(
							new Font(Font.DIALOG, Font.BOLD, 24)
						); 
					break;
				}
			}
		};
		
		for(String i: nombres) {
			
			JRadioButton boton = new JRadioButton(i,
				i.equals("Mediano") ? true : false
			);
			
			boton.addActionListener(oyente);
			
			lamina.add(boton);
			grupoTamanios.add(boton);
		}
	}
	
	//Campos de clase
	private JLabel etiqueta;
	private static final long serialVersionUID = 1L;
}