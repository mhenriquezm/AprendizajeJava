package uso_swing.radio_button;

import java.awt.Dimension;
import java.awt.Toolkit;

import javax.swing.ButtonGroup;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JRadioButton;

public class UsoJRadioButton {

	public static void main(String[] args) {
		
		MarcoRadioButton marco = new MarcoRadioButton();
		marco.setVisible(true);
		marco.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

}

class MarcoRadioButton extends JFrame {
	
	//M�todos constructores
	public MarcoRadioButton() {
		
		Dimension pantalla = Toolkit.getDefaultToolkit().getScreenSize();
		
		int ancho = pantalla.width;
		int alto = pantalla.height;
		
		setTitle("Uso de JRadioButton");
		setBounds(((ancho - 500) / 2), ((alto - 350) / 2), 500, 350);
		
		add(new LaminaRadioButton());
	}
	
	//Campos de clase
	private static final long serialVersionUID = 1L;
}

class LaminaRadioButton extends JPanel {
	
	//M�todos constructores
	public LaminaRadioButton() {
		
		//-------------------------- COLORES --------------------------
		
		//ButtonGroup permite agrupar botones y darles exclusividad
		ButtonGroup grupo1 = new ButtonGroup();
		String[] colores = {"Amarillo", "Azul", "Rojo"};
		
		for(String i: colores) {
			
			//Asignamos nombre y si est� o no seleccionado
			JRadioButton boton = new JRadioButton(i, 
				i.equals("Azul") ? true : false
			);
			
			grupo1.add(boton);//Agregamos cada bot�n al grupo
			
			add(boton);//Agregamos cada bot�n a la l�mina
		}
		
		
		//-------------------------- GENERO --------------------------
		ButtonGroup grupo2 = new ButtonGroup();
		String[] genero = {"Masculino", "Femenino"};
		
		for(String i: genero) {
			
			JRadioButton boton = new JRadioButton(i, 
				i.equals("Masculino") ? true : false
			);
			
			grupo2.add(boton);
			
			add(boton);
		}
	}
	
	//Campos de clase
	private static final long serialVersionUID = 1L;
}