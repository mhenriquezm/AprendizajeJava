package uso_swing.combo_box;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class UsoDeDesplegables {

	public static void main(String[] args) {
		
		MarcoComboBox marco = new MarcoComboBox();
		marco.setVisible(true);
		marco.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

}

class MarcoComboBox extends JFrame {
	
	//M�todos constructores
	public MarcoComboBox() {
		
		Dimension pantalla = Toolkit.getDefaultToolkit().getScreenSize();
		
		int ancho = pantalla.width;
		int alto = pantalla.height;
		
		setTitle("Uso de JComboBox");
		setBounds(((ancho - 500) / 2), ((alto - 350) / 2), 500, 350);
		
		add(new LaminaComboBox());
	}
	
	//Campos de clase
	private static final long serialVersionUID = 1L;
}

class LaminaComboBox extends JPanel {
	
	//M�todos constructores
	public LaminaComboBox() {
		
		setLayout(new BorderLayout());
		
		etiqueta = new JLabel("Te amo Raquel", JLabel.CENTER);
		
		etiqueta.setFont(new Font(Font.SERIF, Font.BOLD, 20));
		
		String[] fuentes = {
			"Verdana", Font.SERIF, Font.SANS_SERIF, Font.MONOSPACED
		};
		
		add(etiqueta, BorderLayout.CENTER);
		agregarLaminaCombo(fuentes);
	}
	
	//M�todos Setter
	private void agregarLaminaCombo(String[] fuentes) {
		
		JPanel laminaCabecera = new JPanel();
		
		//Crear el desplegable parametrizado con String
		final JComboBox<String> desplegable = new JComboBox<String>();
		ActionListener oyente = new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				
				//Capturar el item seleccionado
				fuente = (String) desplegable.getSelectedItem();
				
				etiqueta.setFont(new Font(fuente, Font.BOLD, 20));
			}
			
			private String fuente;
		};
		
		//Agregar los items al desplegable
		for(String i: fuentes) {
			
			desplegable.addItem(i);
			
			//Seleccionar la letra por defecto
			if(i.equals(Font.SERIF))
				desplegable.setSelectedItem(i);
		}
		
		//Colocar las escuchas
		desplegable.addActionListener(oyente);
		desplegable.setEditable(true);//Activar la edici�n
		
		laminaCabecera.add(desplegable);
		
		add(laminaCabecera, BorderLayout.NORTH);
	}
		
	//Campos de clase
	private JLabel etiqueta;
	private static final long serialVersionUID = 1L;
}