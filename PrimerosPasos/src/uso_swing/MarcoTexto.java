package uso_swing;

import java.awt.Dimension;
import java.awt.Toolkit;

import javax.swing.JFrame;

public class MarcoTexto extends JFrame {
	
	//M�todos constructores
	public MarcoTexto() {
		
		Dimension pantalla = Toolkit.getDefaultToolkit().getScreenSize();
		
		int ancho = pantalla.width;
		int alto = pantalla.height;
		
		setTitle("Uso de JTextFiel");
		setBounds(((ancho - 500) / 2), ((alto - 350) / 2), 500, 350);
		
		add(new LaminaTexto());
	}
	
	//Campos de clase
	private static final long serialVersionUID = 1L;
}