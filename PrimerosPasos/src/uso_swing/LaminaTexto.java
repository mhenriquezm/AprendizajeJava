package uso_swing;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class LaminaTexto extends JPanel {
	
	//M�todos constructores
	public LaminaTexto() {
		
		setLayout(new BorderLayout());
		
		Font fuente = new Font("Arial", Font.BOLD, 20);
		
		resultado = new JLabel("", JLabel.CENTER);
		resultado.setFont(fuente);
		
		agregarCabecera();
		
		add(resultado, BorderLayout.CENTER);
	}
	
	//M�todos Setter
	private void agregarCabecera() {
		
		JPanel cabecera = new JPanel();
		JLabel etiqueta = new JLabel("Email:");
		JButton enviar = new JButton("Enviar");
		
		campo1 = new JTextField(20);
		
		enviar.addActionListener(new CapturarTexto());
		
		cabecera.add(etiqueta);
		cabecera.add(campo1);
		cabecera.add(enviar);
		
		add(cabecera, BorderLayout.NORTH);
	}
	
	//Clases internas
	private class CapturarTexto implements ActionListener {
		
		public void actionPerformed(ActionEvent e) {
			
			//El m�todo trim elimina los espacios sobrantes del texto
			String email = campo1.getText().trim();
			int arroba = 0, longitudCorreo = email.length();
			
			for(int i=0;i<longitudCorreo;i++) {
				
				if(email.charAt(i) == '@')
					arroba++;
				
				
			}//for
			
			if(arroba == 1) {
				resultado.setForeground(Color.BLUE);
				resultado.setText("Email correcto");
			}
			else {
				resultado.setForeground(Color.RED);
				resultado.setText("Email incorrecto");
			}
		}
		
	}
	
	//Campos de clase
	private JLabel resultado;
	private JTextField campo1;
	private static final long serialVersionUID = 1L;
}