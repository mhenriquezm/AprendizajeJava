package uso_swing.menus.emergentes;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Toolkit;

import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;

public class UsoJPopupMenu {

	public static void main(String[] args) {
		
		MarcoPopup marco = new MarcoPopup();
		marco.setVisible(true);
		marco.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

}

class MarcoPopup extends JFrame {
	
	//M�todos constructores
	public MarcoPopup() {
		
		Dimension pantalla = Toolkit.getDefaultToolkit().getScreenSize();
		
		int ancho = pantalla.width;
		int alto = pantalla.height;
		
		setTitle("Men�s emergentes");
		setBounds(((ancho - 550) / 2), ((alto - 350) / 2), 550, 350);
		
		add(new LaminaPopup());
	}
	
	//Campos de clase
	private static final long serialVersionUID = 1L;
}

class LaminaPopup extends JPanel {
	
	//M�todos constructores
	public LaminaPopup() {
		
		setLayout(new BorderLayout());
		
		areaTexto = new JTextPane();
		JScrollPane laminaScroll = new JScrollPane(areaTexto, 
			JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, 
			JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED
		);
		
		agregarBarraMenu();
		
		//Construimos un men� emergente (contenedor)
		JPopupMenu emergente = new JPopupMenu();
		String[] items = {"Opci�n 1", "Opci�n 2", "Opci�n 3"};
		
		agregarItems(emergente, items);
		
		//Para agregar un men� emergente usamos el m�todo siguiente:
		areaTexto.setComponentPopupMenu(emergente);
		
		add(laminaScroll, BorderLayout.CENTER);
	}
	
	//M�todos Setter
	private void agregarItems(JPopupMenu emergente, String[] items) {
		
		for(String i: items) {
			emergente.add(new JMenuItem(i));
		}
	}
	
	private void agregarBarraMenu() {
		
		JPanel laminaSuperior = new JPanel();
		JMenuBar barraMenu = new JMenuBar();
		String[] items = {"Fuente", "Estilo", "Tama�o"};
		
		configurarMenu(barraMenu, items);
		
		laminaSuperior.add(barraMenu);
		
		add(laminaSuperior, BorderLayout.NORTH);
	}
	
	private void configurarMenu(JMenuBar barraMenu, String[] items) {
		
		for(String i: items) {
			barraMenu.add(new JMenu(i));
		}
	}
	
	//Campos de clase
	private JTextPane areaTexto;
	private static final long serialVersionUID = 1L;
}