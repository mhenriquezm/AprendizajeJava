package uso_swing.menus.imagenes;

import java.awt.Dimension;
import java.awt.Toolkit;

import javax.swing.JFrame;

public class MarcoMenuConImagen extends JFrame {
	
	//M�todos constructores
	public MarcoMenuConImagen() {
		
		Dimension pantalla = Toolkit.getDefaultToolkit().getScreenSize();
		
		int ancho = pantalla.width;
		int alto = pantalla.height;
		
		setTitle("Uso de men�s con im�genes");
		setBounds(((ancho - 550) / 2), ((alto - 350) / 2), 550, 350);
		
		add(new LaminaMenuConImagen());
	}
	
	//Campos de clase
	private static final long serialVersionUID = 1L;
}