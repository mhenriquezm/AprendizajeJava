package uso_swing.menus.imagenes;

import java.awt.BorderLayout;
import java.io.File;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;

/*
Los formatos soportados para im�gens son:
 * gif
 * jpg
 * png
Es recomendable que para iconos de men� se use gif ya que pesa menos*/

public class LaminaMenuConImagen extends JPanel {
	
	//M�todos constructores
	public LaminaMenuConImagen() {
		
		setLayout(new BorderLayout());
		
		JPanel laminaSuperior = new JPanel();
		String[] opciones = {"Archivo", "Edici�n", "Herramientas"};
		
		areaTexto = new JTextPane();
		
		JScrollPane laminaScroll = new JScrollPane(areaTexto,
			JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
			JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED
		);
		
		configurarBarra(laminaSuperior, opciones);
		
		add(laminaSuperior, BorderLayout.NORTH);
		add(laminaScroll, BorderLayout.CENTER);
	}
	
	//M�todos Setter
	private void configurarBarra(JPanel laminaSuperior, String[] opciones)
	{
		
		JMenuBar barraMenu = new JMenuBar();
		
		for(String i: opciones) {
			
			JMenu menu = new JMenu(i);
			
			switch(i) {
				case "Archivo":
					agregarItems(menu, new String[] {
						"Guardar", "Guardar Como"
					}, true);
					
					agregarItems(menu, new String[] {"Salir"});
				break;
				
				case "Edici�n":
					agregarItems(menu, new Object[] {
						"Copiar", new ImageIcon(
							"src" 
							+ File.separator + "uso_swing"
							+ File.separator + "menus" 
							+ File.separator + "imagenes" 
							+ File.separator + "copiar.gif"
						), 
						"Cortar", new ImageIcon(
							"src" 
							+ File.separator + "uso_swing"
							+ File.separator + "menus" 
							+ File.separator + "imagenes" 
							+ File.separator + "cortar.gif"
						), 
						"Pegar", new ImageIcon(
							"src" 
							+ File.separator + "uso_swing"
							+ File.separator + "menus" 
							+ File.separator + "imagenes" 
							+ File.separator + "pegar.gif"
						),
					}, true);
					
					agregarSubMenu(menu, new JMenu("Opciones"), 
						new String[] {
						
						"Opci�n 1", "Opci�n 2"
					});
				break;
				
				case "Herramientas":
					agregarItems(menu, new String[] {
						"Generales"
					});
				break;
			}
			
			barraMenu.add(menu);
		}
		
		laminaSuperior.add(barraMenu);
	}
	
	private void agregarSubMenu(JMenu menu, JMenu subMenu, 
		String[] items) {
		
		agregarItems(subMenu, items);
		
		menu.add(subMenu);
	}
	
	private void agregarItems(JMenu menu, String[] items) {
		
		for(String i: items) {
			
			menu.add(new JMenuItem(i));
		}
	}
	
	private void agregarItems(JMenu menu, String[] items, 
			boolean separador) {
		
		for(String i: items) {
			
			menu.add(new JMenuItem(i));
		}
		
		if(separador) menu.addSeparator();
	}
	
	private void agregarItems(JMenu menu, Object[] items, 
			boolean separador) {
		
		int longitud = items.length;
		
		for(int i=0;i<longitud;i+=2) {
			
			//Correspondemos cada submen� con su icono
			JMenuItem subMenu = new JMenuItem(
				(String)items[i], (Icon)items[i + 1]
			);
			
			//subMenu.setHorizontalTextPosition(JMenuItem.LEFT);
			//Este m�todo sirve para cambiar la posici�n del texto
			
			menu.add(subMenu);
		}
		
		if(separador) menu.addSeparator();
	}
	
	//Campos de clase
	private JTextPane areaTexto;
	private static final long serialVersionUID = 1L;
}