package uso_swing.menus;

import java.awt.Dimension;
import java.awt.Toolkit;

import javax.swing.JFrame;

public class MarcoMenu extends JFrame {
	
	//M�todos constructores
	public MarcoMenu() {
		
		Dimension pantalla = Toolkit.getDefaultToolkit().getScreenSize();
		
		int ancho = pantalla.width;
		int alto = pantalla.height;
		
		setTitle("Uso de men�s");
		setBounds(((ancho - 550) / 2), ((alto - 350) / 2), 550, 350);
		
		add(new LaminaMenu());
	}
	
	//Campos de clase
	private static final long serialVersionUID = 1L;
}