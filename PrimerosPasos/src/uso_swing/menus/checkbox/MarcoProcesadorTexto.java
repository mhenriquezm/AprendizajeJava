package uso_swing.menus.checkbox;

import java.awt.Dimension;
import java.awt.Toolkit;

import javax.swing.JFrame;

public class MarcoProcesadorTexto extends JFrame {
	
	//M�todos constructores
	public MarcoProcesadorTexto() {
		
		Dimension pantalla = Toolkit.getDefaultToolkit().getScreenSize();
		
		int ancho = pantalla.width;
		int alto = pantalla.height;
		
		setTitle("Procesador de textos MyR");
		setBounds(((ancho - 550) / 2), ((alto - 350) / 2), 550, 350);
		
		add(new LaminaProcesadorTexto());
	}
	
	//Campos de clase
	private static final long serialVersionUID = 1L;
}