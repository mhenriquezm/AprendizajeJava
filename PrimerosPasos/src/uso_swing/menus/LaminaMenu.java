package uso_swing.menus;

import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;

public class LaminaMenu extends JPanel {
	
	//M�todos constructores
	public LaminaMenu() {
		
		JMenuBar barraMenu = new JMenuBar();
		String[] opciones = {"Archivo", "Edici�n", "Herramientas"};
		
		configurarBarra(barraMenu, opciones);
		
		add(barraMenu);
	}
	
	//M�todos Setter
	private void configurarBarra(JMenuBar barraMenu, String[] opciones) {
		
		for(String i: opciones) {
			
			JMenu menu = new JMenu(i);
			
			switch(i) {
				case "Archivo":
					agregarItems(menu, new String[] {
						"Guardar", "Guardar Como"
					}, true);
					
					agregarItems(menu, new String[] {"Salir"});
				break;
				
				case "Edici�n":
					agregarItems(menu, new String[] {
						"Copiar", "Cortar", "Pegar"
					}, true);
					
					agregarSubMenu(menu, new JMenu("Opciones"), 
						new String[] {
						
						"Opci�n 1", "Opci�n 2"
					});
				break;
				
				case "Herramientas":
					agregarItems(menu, new String[] {
						"Generales"
					});
				break;
			}
			
			barraMenu.add(menu);
		}
	}
	
	private void agregarSubMenu(JMenu menu, JMenu subMenu, 
		String[] items) {
		
		agregarItems(subMenu, items);
		
		menu.add(subMenu);
	}
	
	private void agregarItems(JMenu menu, String[] items) {
		
		for(String i: items) {
			
			menu.add(new JMenuItem(i));
		}
	}
	
	private void agregarItems(JMenu menu, String[] items, 
			boolean separador) {
		
		for(String i: items) {
			
			menu.add(new JMenuItem(i));
		}
		
		if(separador) menu.addSeparator();
	}
	
	//Campos de clase
	private static final long serialVersionUID = 1L;
}