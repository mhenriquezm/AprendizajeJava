import java.util.Scanner;

public class Factorial {

	public static void main(String[] args) {
		
		Scanner entrada = new Scanner(System.in);
		
		System.out.print("Ingrese un n�mero: ");
		int numero = entrada.nextInt();
		long factorial = 1L;
		
		System.out.print("\n" + numero + "!: ");
		
		for(int i=numero;i>0;i--) {
			factorial *= i;
			
			if(i == 1) {
				System.out.println(i + " = " + factorial);
			}
			else {
				System.out.print(i + " * ");
			}
		}
		
		entrada.close();
	}

}
