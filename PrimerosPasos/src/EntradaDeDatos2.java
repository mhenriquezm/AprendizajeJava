import javax.swing.*;

public class EntradaDeDatos2 {

	public static void main(String[] args) {
		
		/*El m�todo es static, usamos la clase por delante y devuelve 
		un String*/
		String nombre = JOptionPane.showInputDialog("Ingrese su nombre");
		
		/*Como estamos recibiendo un String no lo podemos refundir a 
		 entero por eso usamos la clase envoltorio Integer y su m�todo
		 parseInt para transformar ese String en un tipo primitivo, 
		 en este caso entero*/
		int edad = Integer.parseInt(JOptionPane.
				showInputDialog("Ingrese su edad")
			);
		
		System.out.println("Bienvenido/a, sus datos son:"
				+ "\n- Nombre: " + nombre
				+ "\n- Edad: " + edad
			);
	}

}
