import javax.swing.*;

public class PesoIdeal {

	public static void main(String[] args) {
		
		String genero = "";
		
		//Validar el g�nero
		do {
			
			genero = JOptionPane.showInputDialog("Ingrese su g�nero "
					+ "[M/F]").toUpperCase();//Pasamos a may�sculas
			
		}while((!genero.equals("M")) && (!genero.equals("F")));
		
		//Variables para operar
		float altura = Float.parseFloat(JOptionPane
				.showInputDialog("Ingrese su altura en metros"));
		int alturaCM = (int)(altura * 100), pesoIdeal = 0;
		
		//Evaluamos el peso en funci�n al g�nero
		switch(genero.charAt(0)) {
			case 'M': pesoIdeal = alturaCM - 110; break;
			case 'F': pesoIdeal = alturaCM - 120; break;
		}
		
		//Mostramos el resultado
		JOptionPane.showMessageDialog(null, 
				"Su peso ideal es: " + pesoIdeal + "Kg.");
		
	}

}
