package layouts;

import java.awt.Dimension;
import java.awt.Toolkit;

import javax.swing.JFrame;

public class MarcoFlowLayout extends JFrame {
	
	//M�todos constructores
	public MarcoFlowLayout() {
		
		Dimension pantalla = Toolkit.getDefaultToolkit().getScreenSize();
		
		int ancho = pantalla.width;
		int alto = pantalla.height;
		
		setTitle("Uso de FlowLayout");
		setBounds(((ancho - 500) / 2), ((alto - 350) / 2), 500, 350);
		
		add(new LaminaFlowLayout());
	}
	
	//Campos de clase
	private static final long serialVersionUID = 1L;
}
