package layouts.border_layout;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Toolkit;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class DisposicionBorder {

	public static void main(String[] args) {
		
		MarcoBorderLayout marco = new MarcoBorderLayout();
		marco.setVisible(true);
		marco.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

}

class MarcoBorderLayout extends JFrame {
	
	//M�todos constructores
	public MarcoBorderLayout() {
		
		Dimension pantalla = Toolkit.getDefaultToolkit().getScreenSize();
		
		int ancho = pantalla.width;
		int alto = pantalla.height;
		
		setTitle("Uso de BorderLayout");
		setBounds(((ancho - 500) / 2), ((alto - 350) / 2), 500, 350);
		
		add(new LaminaBorderLayout());
	}
	
	//Campos de clase
	private static final long serialVersionUID = 1L;
}

class LaminaBorderLayout extends JPanel {
	
	//M�todos constructores
	public LaminaBorderLayout() {
		
		setLayout(new BorderLayout(10, 10));
		
		Object[][] botones = {
			{"Norte", "Sur", "Este", "Oeste", "Centro"},
			{
				BorderLayout.NORTH, BorderLayout.SOUTH, 
				BorderLayout.EAST, BorderLayout.WEST, 
				BorderLayout.CENTER
			}
		};
		
		agregarBotones(botones);
	}
	
	//M�todos Setter
	public void agregarBotones(Object[][] botones) {
		
		int filas = botones[0].length;
		
		for(int i=0;i<filas;i++) {
			
			add(new JButton((String) botones[0][i]), botones[1][i]);
		}
	}
	
	//Campos de clase
	private static final long serialVersionUID = 1L;
}