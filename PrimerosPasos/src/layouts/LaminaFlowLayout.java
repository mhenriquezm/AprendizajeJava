package layouts;

import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JPanel;

public class LaminaFlowLayout extends JPanel {
	
	//Métodos constructores
	public LaminaFlowLayout() {
		
		//Creamos una instancia de FlowLayout para usar esa disposición
		setLayout(new FlowLayout(FlowLayout.CENTER, 75, 100));
		
		String[] titulos = {"Amarillo", "Azul", "Rojo"};
		
		agregarBotones(titulos);
	}
	
	//Métodos Setter
	public void agregarBotones(String[] titulos) {
		
		for(String i: titulos) {
			
			add(new JButton(i));
		}
		
	}
	
	//Campos de clase
	private static final long serialVersionUID = 1L;
}
