package layouts.multiples_layouts;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Toolkit;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;


public class Principal {

	public static void main(String[] args) {
		
		MarcoMultiplesLayout marco = new MarcoMultiplesLayout();
		marco.setVisible(true);
		marco.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
	}

}

class MarcoMultiplesLayout extends JFrame {
	
	//M�todos constructores
	public MarcoMultiplesLayout() {
		
		Dimension pantalla = Toolkit.getDefaultToolkit().getScreenSize();
		
		int ancho = pantalla.width;
		int alto = pantalla.height;
		
		setTitle("Uso de m�ltiples layouts");
		setBounds(((ancho - 500) / 2), ((alto - 350) / 2), 500, 350);
		
		//Los JFrame tienen por defecto BorderLayout
		add(new LaminaFlowLayout(), BorderLayout.NORTH);
		add(new LaminaBorderLayout(), BorderLayout.SOUTH);
	}
	
	//Campos de clase
	private static final long serialVersionUID = 1L;
}

class LaminaFlowLayout extends JPanel {
	
	//M�todos constructores
	public LaminaFlowLayout() {
		
		setLayout(new FlowLayout(FlowLayout.LEFT));
		
		agregarBotones();
	}
	
	//M�todos setter
	public void agregarBotones() {
		
		for(int i=0;i<2;i++) {
			
			add(new JButton("Bot�n " + (i + 1)));
		}
	}
	
	//Campos de clase
	private static final long serialVersionUID = 1L;
}

class LaminaBorderLayout extends JPanel {
	
	//M�todos constructores
	public LaminaBorderLayout() {
		
		setLayout(new BorderLayout());
		
		Object[][] botones = {
			{"Bot�n 3", "Bot�n 4", "Bot�n 5"},
			{BorderLayout.WEST, BorderLayout.SOUTH, BorderLayout.EAST}
		};
		
		agregarBotones(botones);
	}
	
	//M�todos setter
	public void agregarBotones(Object[][] botones) {
		
		int columnas = botones[0].length;
		
		for(int i=0;i<columnas;i++) {
			
			add(new JButton((String) botones[0][i]), botones[1][i]);
		}
	}
	
	//Campos de clase
	private static final long serialVersionUID = 1L;
}