package layouts.avanzados.libre;

import java.awt.Dimension;
import java.awt.Toolkit;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class EjemploFormulario {

	public static void main(String[] args) {
		
		MarcoEjemplo marco = new MarcoEjemplo();
		marco.setVisible(true);
		marco.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

}

class MarcoEjemplo extends JFrame {
	
	//M�todos Constructores
	public MarcoEjemplo() {
		
		Dimension pantalla = Toolkit.getDefaultToolkit().getScreenSize();
		
		int ancho = pantalla.width;
		int alto = pantalla.height;
		
		setTitle("Disposiciones en libres");
		setBounds(((ancho - 550) / 2), ((alto - 350) / 2), 550, 350);
		
		add(new LaminaEjemplo());
	}
	
	//Campos de clase
	private static final long serialVersionUID = 1L;
}

class LaminaEjemplo extends JPanel {
	
	//M�todos Constructores
	public LaminaEjemplo() {
		
		setLayout(null);
		
		JLabel nombre = new JLabel("Nombre:");
		JLabel apellido = new JLabel("Apellido:");
		
		JTextField c_nombre = new JTextField();
		JTextField c_apellido = new JTextField();
		
		nombre.setBounds(50, 50, 50, 20);
		c_nombre.setBounds(110, 50, 150, 20);
		
		apellido.setBounds(50, 80, 50, 20);
		c_apellido.setBounds(110, 80, 150, 20);
		
		add(nombre);
		add(apellido);
		add(c_nombre);
		add(c_apellido);
	}
	
	//Campos de clase
	private static final long serialVersionUID = 1L; 
}