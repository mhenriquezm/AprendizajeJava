package layouts.avanzados.libre;

import java.awt.Dimension;
import java.awt.Toolkit;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class EjemploLayoutPropio {

	public static void main(String[] args) {
		
		MarcoLayoutPropio marco = new MarcoLayoutPropio();
		marco.setVisible(true);
		marco.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

}

class MarcoLayoutPropio extends JFrame {
	
	//M�todos Constructores
	public MarcoLayoutPropio() {
		
		Dimension pantalla = Toolkit.getDefaultToolkit().getScreenSize();
		
		int ancho = pantalla.width;
		int alto = pantalla.height;
		
		setTitle("Disposiciones en libres");
		setBounds(((ancho - 550) / 2), ((alto - 350) / 2), 550, 350);
		
		add(new LaminaLayoutPropio());
	}
	
	//Campos de clase
	private static final long serialVersionUID = 1L;
}

class LaminaLayoutPropio extends JPanel {
	
	//M�todos Constructores
	public LaminaLayoutPropio() {
		//Usamos el layout propio
		setLayout(new LayoutPersonal());
		
		JLabel nombre = new JLabel("Nombre:");
		JLabel apellido = new JLabel("Apellido:");
		JLabel edad = new JLabel("Edad:");
		
		JTextField c_nombre = new JTextField();
		JTextField c_apellido = new JTextField();
		JTextField c_edad = new JTextField();
		
		add(nombre);
		add(c_nombre);
		add(apellido);
		add(c_apellido);
		add(edad);
		add(c_edad);
	}
	
	//Campos de clase
	private static final long serialVersionUID = 1L; 
}