package layouts.avanzados.libre;

import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.LayoutManager;

//Ubica los componentes por parejas y en columnas
public class LayoutPersonal implements LayoutManager {
	
	//M�todos constructores
	public LayoutPersonal() {
		x = y = 20;//Asignaci�n simplificada
	}
	
	//M�todos implementados
	public void addLayoutComponent(String nombre, Component padre) {}
	
	public void layoutContainer(Container padre) {
		
		//Cantidad de componentes dentro del contenedor
		int n_elementos = padre.getComponentCount();
		
		//Recorremos todos los elementos
		for(int i = 0; i < n_elementos; i++) {
			
			//Componente actual
			Component c = padre.getComponent(i);
			
			//Ubicar el componente
			if((i % 2) == 0) {
				c.setBounds(x, y, 50, 20);
				x += 80;
			}
			else {
				c.setBounds(x, y, 150, 20);
				y += 30;
				x = 20;
			}
		}
	}
	
	public Dimension minimumLayoutSize(Container padre) {
		return null;
	}
	
	public Dimension preferredLayoutSize(Container padre) {
		return null;
	}
	
	public void removeLayoutComponent(Component padre) {}
	
	//Campos de clase
	private int x, y;
}
