package layouts.avanzados.libre;

import java.awt.Dimension;
import java.awt.Toolkit;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class DisposicionLibre {

	public static void main(String[] args) {
		
		MarcoLibre marco = new MarcoLibre();
		marco.setVisible(true);
		marco.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

}

class MarcoLibre extends JFrame {
	
	//M�todos Constructores
	public MarcoLibre() {
		
		Dimension pantalla = Toolkit.getDefaultToolkit().getScreenSize();
		
		int ancho = pantalla.width;
		int alto = pantalla.height;
		
		setTitle("Disposiciones en libres");
		setBounds(((ancho - 550) / 2), ((alto - 350) / 2), 550, 350);
		
		add(new LaminaLibre());
	}
	
	//Campos de clase
	private static final long serialVersionUID = 1L;
}

class LaminaLibre extends JPanel {
	
	//M�todos Constructores
	public LaminaLibre() {
		//Para trabajar con disposiciones libres anulamos el layout
		setLayout(null);
		
		JButton boton1 = new JButton("Bot�n 1");
		JButton boton2 = new JButton("Bot�n 2");
		
		/*Si usamos disposiciones libres debemos usar setBounds para
		indicar las dimensiones y ubicaci�n de los componentes*/
		boton1.setBounds(50, 50, 80, 25);
		boton2.setBounds(150, 50, 80, 25);
		
		add(boton1);
		add(boton2);
		
		//setLocation no funciona con layouts libres
	}
	
	//Campos de clase
	private static final long serialVersionUID = 1L; 
}