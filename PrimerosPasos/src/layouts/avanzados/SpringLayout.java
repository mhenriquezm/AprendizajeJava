package layouts.avanzados;

import java.awt.Dimension;
import java.awt.Toolkit;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.Spring;

public class SpringLayout {

	public static void main(String[] args) {
		
		MarcoSpring marco = new MarcoSpring();
		marco.setVisible(true);
		marco.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

}

class MarcoSpring extends JFrame {
	
	//M�todos Constructores
	public MarcoSpring() {
		
		Dimension pantalla = Toolkit.getDefaultToolkit().getScreenSize();
		
		int ancho = pantalla.width;
		int alto = pantalla.height;
		
		setTitle("Disposiciones en muelle");
		setBounds(((ancho - 800) / 2), ((alto - 350) / 2), 800, 350);
		
		add(new LaminaSpring());
	}
	
	//Campos de clase
	private static final long serialVersionUID = 1L;
}

class LaminaSpring extends JPanel {
	
	//M�todos Constructores
	public LaminaSpring() {
		
		//Como hay conflicos de nombres sw invoca la clase desde su paquete
		javax.swing.SpringLayout layout = new javax.swing.SpringLayout();
		
		setLayout(layout);
		
		agregarBotones(new String[] {"Bot�n 1", "Bot�n 2", "Bot�n 3"}, 
				layout);
	}
	
	//M�todos Setter
	private void agregarBotones(String[] rotulos, javax.swing.SpringLayout 
			layout) {
		
		Spring muelle = Spring.constant(0, 10, 100);
		Spring muelleFijo = Spring.constant(80);
		
		int cant_botones = rotulos.length;
		
		for(int i = 0; i <= cant_botones; i++) {
			
			if(i < cant_botones) {
				add(new JButton(rotulos[i]));
				
				if(i == 0) {
					layout.putConstraint(
						javax.swing.SpringLayout.WEST, getComponent(i), 
						muelle, 
						javax.swing.SpringLayout.WEST, this
					);
				}
				else {
					layout.putConstraint(
						javax.swing.SpringLayout.WEST, getComponent(i), 
						muelleFijo, 
						javax.swing.SpringLayout.EAST, getComponent(i - 1)
					);
				}
			}
			else {
				layout.putConstraint(
					javax.swing.SpringLayout.EAST, this, 
					muelle, 
					javax.swing.SpringLayout.EAST, getComponent(i - 1)
				);
			}
		}
	}
	
	//Campos de clase
	private static final long serialVersionUID = 1L; 
}