package layouts.avanzados;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Toolkit;

import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

public class BoxLayout {

	public static void main(String[] args) {
		
		MarcoBox marco = new MarcoBox();
		marco.setVisible(true);
		marco.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

}

class MarcoBox extends JFrame {
	
	//M�todos Constructores
	public MarcoBox() {
		
		Dimension pantalla = Toolkit.getDefaultToolkit().getScreenSize();
		
		int ancho = pantalla.width;
		int alto = pantalla.height;
		
		setTitle("Disposiciones en caja");
		setBounds(((ancho - 200) / 2), ((alto - 200) / 2), 200, 200);
		
		JLabel rotulo1 = new JLabel("Usuario");
		JTextField texto1 = new JTextField(10);
		texto1.setMaximumSize(texto1.getPreferredSize());
		
		Box cajaH1 = Box.createHorizontalBox();
		
		cajaH1.add(rotulo1);
		cajaH1.add(Box.createHorizontalStrut(10));
		cajaH1.add(texto1);
		
		JLabel rotulo2 = new JLabel("Contrase�a");
		JPasswordField texto2 = new JPasswordField(10);
		texto2.setMaximumSize(texto2.getPreferredSize());
		
		Box cajaH2 = Box.createHorizontalBox();
		
		cajaH2.add(rotulo2);
		cajaH2.add(Box.createHorizontalStrut(10));
		cajaH2.add(texto2);
		
		JButton boton1 = new JButton("Enviar");
		JButton boton2 = new JButton("Cancelar");
		
		Box cajaH3 = Box.createHorizontalBox();
		
		cajaH3.add(boton1);
		cajaH3.add(Box.createGlue());
		cajaH3.add(boton2);
		
		Box cajaV1 = Box.createVerticalBox();
		
		cajaV1.add(cajaH1);
		cajaV1.add(cajaH2);
		cajaV1.add(cajaH3);
		
		setMinimumSize(new Dimension(200, 200));
		
		add(cajaV1, BorderLayout.CENTER);
	}
	
	//Campos de clase
	private static final long serialVersionUID = 1L;
}