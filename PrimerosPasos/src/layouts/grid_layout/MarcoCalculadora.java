package layouts.grid_layout;

import java.awt.Dimension;
import java.awt.Toolkit;

import javax.swing.JFrame;

public class MarcoCalculadora extends JFrame {
	
	//M�todos constructores
	public MarcoCalculadora() {
		
		Dimension pantalla = Toolkit.getDefaultToolkit().getScreenSize();
		
		int ancho = pantalla.width;
		int alto = pantalla.height;
		
		setTitle("Calculadora MyR");
		setBounds(((ancho - 300) / 2), ((alto - 280) / 2), 300, 280);
		
		setResizable(false);
		
		add(new LaminaCalculadora());
	}
	
	//Campos de clase
	private static final long serialVersionUID = 1L;
}
