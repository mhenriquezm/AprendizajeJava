package layouts.grid_layout;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;

import javax.swing.JTextField;
import javax.swing.JPanel;

public class LaminaCalculadora extends JPanel {
	
	public LaminaCalculadora() {
		
		setLayout(new BorderLayout());
		
		display = new JTextField("0");
		
		display.setHorizontalAlignment(JTextField.RIGHT);
		display.setFont(new Font(Font.SANS_SERIF, Font.BOLD, 20));
		display.setBackground(new Color(237, 237, 237));
		display.setEnabled(false);
		
		add(display, BorderLayout.NORTH);
		add(new LaminaBotones(display), BorderLayout.CENTER);
	}
	
	//Campos de clase
	private JTextField display;
	private static final long serialVersionUID = 1L;
}
