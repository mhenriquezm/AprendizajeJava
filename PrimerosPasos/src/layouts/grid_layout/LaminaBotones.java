package layouts.grid_layout;

import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class LaminaBotones extends JPanel {
	
	//M�todos constructores
	public LaminaBotones(JTextField display) {
		
		setLayout(new GridLayout(4, 4));
		
		textoDisplay = "";
		
		this.display=display;
		
		agregarBotones();
	}
	
	private void agregarBotones() {
		
		int i=7, j=10, k, cont=0;
		char[] operaciones = {'/', '*', '-'};
		char[] ultimaFila = {'.', '0', '=', '+'};
		
		Font fuente = new Font(Font.SANS_SERIF, Font.BOLD, 18);
		ActionListener oyenteEscritura = new EscribirNumero();
		ActionListener oyenteOperaciones = new GestionarOperaciones();
		
		while(i>0){			
			for(k=i;k<j;k++){
				
				configurarBoton(("" + k), fuente, oyenteEscritura);
			}
			
			configurarBoton(("" + operaciones[cont]), fuente, 
					oyenteOperaciones);
			
			i-=3;
			j-=3;
			cont++;
			
			if(i < 0) {
				for(char z: ultimaFila) {
					
					switch(z) {
						case '.':
						case '0': 
							configurarBoton(("" + z), fuente, 
									oyenteEscritura);
						break;
						default:
							configurarBoton(("" + z), fuente, 
									oyenteOperaciones);
					}
				}
			}
			
		}
		
	}
	
	private void configurarBoton(String rotulo, Font fuente, 
			ActionListener oyente) {
		
		JButton boton = new JButton(rotulo);
		
		boton.setFont(fuente);
		
		boton.addActionListener(oyente);
		
		add(boton);
		
	}
	
	//Clases internas
	private class EscribirNumero implements ActionListener {
		
		public void actionPerformed(ActionEvent e) {
			
			texto = e.getActionCommand();
			
			display.setText(textoDisplay + texto);
			
			textoDisplay += texto;
		}
		
		//Campos de clase
		private String texto;
	}
	
	private class GestionarOperaciones implements ActionListener {
		
		public GestionarOperaciones() {
			ultimaOperacion = "";
		}
		
		public void actionPerformed(ActionEvent e) {
			
			calcular(Double.parseDouble(display.getText()));
			
			ultimaOperacion = e.getActionCommand();
			
			textoDisplay = "";
		}
		
		//M�todos Setter
		public void calcular(double valorDisplay) {
			
			switch(ultimaOperacion) {
				case "+":
					resultado += valorDisplay;
				break;
				
				case "-":
					resultado -= valorDisplay;
				break;
				
				case "*":
					resultado *= valorDisplay;
				break;
				
				case "/":
					resultado /= valorDisplay;
				break;
				
				default:
					resultado = valorDisplay;
			}
			
			display.setText(String.valueOf(resultado));
		}
		
		//Campos de clase
		private double resultado;
		private String ultimaOperacion;
	}
	
	//Campos de clase
	private String textoDisplay;
	private JTextField display;
	private static final long serialVersionUID = 1L;
}