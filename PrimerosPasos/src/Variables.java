
public class Variables {

	public static void main(String[] args) {
		//Tipo de variable, nombre de la variable finaliza en ';'
		byte edad = 26;
		
		//Imprimimos el valor de la variable
		System.out.println(edad);
		
		edad = 75;//Cambiamos el valor de la variable
		
		System.out.println(edad);
	}

}
