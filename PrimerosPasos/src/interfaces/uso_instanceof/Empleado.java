package interfaces.uso_instanceof;

import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;

//Implementamos la interfaz comparable y la parametrizamos 
public class Empleado implements Comparable<Object> {
	
	//M�todos Constructores
	public Empleado(String nombre, double sueldo, int dia, int mes,
		int anio) {
		
		this.nombre = nombre;
		this.sueldo = sueldo;
		
		GregorianCalendar calendario = new GregorianCalendar(anio, 
			(mes - 1), dia);
		
		altaContrato = calendario.getTime();
		
		IdSiguiente++;
		Id = IdSiguiente;
		
	}
	
	public Empleado(String nombre) {
		this(nombre, 40000, 1, 1, 2000);
	}
	
	//M�todos Setter
	public void aumentarSueldo(double porcentaje) {
		sueldo *= (1 + (porcentaje / 100));
	}
	
	//M�todos Getter
	public int devolverId() {
		return Id;
	}
	
	public String devolverNombre() {
		return nombre;
	}
	
	public String devolverFechaDeAlta() {
		
		if(altaContrato == null) {
			return "dd/mm/aaaa";
		}
		
		return new SimpleDateFormat("dd/MM/yyyy").format(altaContrato);
	}
	
	//Sobrecarga de m�todos
	public String devolverSueldo(NumberFormat formato) {
		return formato.format(sueldo);
	}
	
	public double devolverSueldo() {
		return sueldo;
	}
	
	//M�todos implementados
	public int compareTo(Object T) {
		
		//Refundimos el par�metro para tener un empleado
		Empleado empleado = (Empleado) T;
		
		//Comparaciones para el resultado del quick sort
		if(this.Id < empleado.Id) {
			return -1;
		}
		else if(this.Id > empleado.Id) {
			return 1;
		}
		
		return 0;
	}
	
	//Atributos
	private String nombre;
	private double sueldo;
	private Date altaContrato;
	private int Id;
	private static int IdSiguiente = 0;
}
