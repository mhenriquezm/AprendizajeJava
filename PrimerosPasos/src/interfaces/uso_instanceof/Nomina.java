package interfaces.uso_instanceof;

public class Nomina {
	
	public static void main(String[] args) {
		
		//Usamos el principio de sustitución
		Empleado gerente = new Jefe("Maira Moreno", 2000, 11, 02, 2020);
		Comparable<Object> ejemplo = new Empleado("Jose Henriquez", 
				2000, 20, 04, 2020);
		
		//Comprobamos si el objeto gerente es una instancia de Empleado
		if(gerente instanceof Empleado) {
			
			System.out.println("Es un Jefe");
		}
		
		if(ejemplo instanceof Comparable) {
			
			System.out.println("Implementa la interfaz Comparable");
		}
		
	}
	
	
}