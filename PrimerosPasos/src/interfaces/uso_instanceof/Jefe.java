package interfaces.uso_instanceof;

import java.text.NumberFormat;

public class Jefe extends Empleado {
	
	//M�todos constructores
	public Jefe(String nombre, double sueldo, int dia, int mes, 
			int anio) {
		
		super(nombre, sueldo, dia, mes, anio);
	}
	
	public Jefe(String nombre) {
		super(nombre);
	}
	
	//M�todos Setter
	public void establecerIncentivo(double incentivo) {
		this.incentivo = incentivo;
	}
	
	//M�todos sobre escritos
	public double devolverSueldo() {
		return super.devolverSueldo() + incentivo;
	}

	public String devolverSueldo(NumberFormat formato) {
		return formato.format(super.devolverSueldo() + incentivo);
	}
	
	//Atributos
	private double incentivo;
	
}
