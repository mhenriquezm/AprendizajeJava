package interfaces.interfaces_propias;

//Una interfaz se crea igual que una clase cambiando class por interface
public interface Trabajadores {
	
	double establecerBono(double gratificacion);
	
	//Las interfaces tienen constantes de clase
	double BASE = 1500;
}
