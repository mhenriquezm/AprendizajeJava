package interfaces.interfaces_propias;

//Podemos crear una jerarqu�a de herencia en las interfaces
public interface Gerencia extends Trabajadores {
	
	//Los m�todos de una interfaz son p�blicos y abstractos
	String tomarDecision(String cargo, String decision);
	
}
