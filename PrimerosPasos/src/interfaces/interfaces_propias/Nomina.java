package interfaces.interfaces_propias;

import java.text.NumberFormat;
import java.util.Arrays;
import java.util.Locale;

public class Nomina {
	
	public static void main(String[] args) {
		
		Jefe manuel = new Jefe("Manuel Henriquez", 600000, 14, 5, 2019);
		
		manuel.establecerIncentivo(500000);
		
		Empleado[] nomina = {
			new Empleado("Sergio Cova"),
			new Empleado("Ender Paredes", 350000,	1, 1, 2018),
			new Empleado("Fernanda Miranda"),
			new Empleado("Hector Bastidas"),
			manuel,
			new Jefe("Raquel Herrera", 850000, 10, 10, 2019)
		};
		
		int longNomina = nomina.length;
		Jefe jefe_RRHH = (Jefe) nomina[longNomina-1];
		jefe_RRHH.establecerIncentivo(250000);
		
		System.out.println(manuel.tomarDecision("El Ing. ", 
			"aumentar un 5% del sueldo a todos los empleados.\n"));
		
		NumberFormat formato = NumberFormat
			.getCurrencyInstance(new Locale("es", "VE"));
		
		System.out.println("El Ing. " + manuel.devolverNombre() 
				+ " tiene un bono de: " 
				+ formato.format(manuel.establecerBono(500)));
		
		System.out.println("El Br. " + nomina[1].devolverNombre() 
				+ " tiene un bono de: " 
				+ formato.format(nomina[1].establecerBono(500)) 
				+ "\n");
		
		for(int i=0;i<longNomina;i++) {
			
			nomina[i].aumentarSueldo(5);
			
		}
		
		Arrays.sort(nomina);
		
		for(Empleado i:nomina) {
			
			System.out.println("Datos del empleado:"
				+ "\n- Id: " + i.devolverId()
				+ "\n- Nombre: " + i.devolverNombre()
				+ "\n- Sueldo: " + i.devolverSueldo(formato)
				+ "\n- Alta de contrado: " + i.devolverFechaDeAlta() 
				+ "\n");
		}
		
	}
	
	
}