package interfaces.interfaces_propias;

import java.text.NumberFormat;

public class Jefe extends Empleado implements Gerencia {
	
	//M�todos constructores
	public Jefe(String nombre, double sueldo, int dia, int mes, 
			int anio) {
		
		super(nombre, sueldo, dia, mes, anio);
	}
	
	public Jefe(String nombre) {
		super(nombre);
	}
	
	//M�todos Setter
	public void establecerIncentivo(double incentivo) {
		this.incentivo = incentivo;
	}
	
	//M�todos sobre escritos
	public double devolverSueldo() {
		return super.devolverSueldo() + incentivo;
	}

	public String devolverSueldo(NumberFormat formato) {
		return formato.format(super.devolverSueldo() + incentivo);
	}
	
	//M�todos implementados
	public String tomarDecision(String cargo, String decision) {
		return  cargo 
			+ devolverNombre() 
			+ " ha tomado la decisi�n de " 
			+ decision;
	}
	
	public double establecerBono(double gratificacion) {
		
		double prima = 2000;
		//Las constantes de la interfaz se llaman como campos static
		return Trabajadores.BASE + prima + gratificacion;
	}
	
	//Atributos
	private double incentivo;
	
}
