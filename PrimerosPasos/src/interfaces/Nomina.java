package interfaces;

import java.text.NumberFormat;
import java.util.Arrays;
import java.util.Locale;

public class Nomina {
	
	public static void main(String[] args) {
		
		Jefe manuel = new Jefe("Manuel Henriquez", 600000, 14, 5, 2019);
		
		manuel.establecerIncentivo(500000);
		
		Empleado[] nomina = {
			new Empleado("Sergio Cova"),
			new Empleado("Ender Paredes", 350000,	1, 1, 2018),
			new Empleado("Fernanda Miranda"),
			new Empleado("Hector Bastidas"),
			manuel,
			new Jefe("Raquel Herrera", 850000, 10, 10, 2019)
		};
		
		int longNomina = nomina.length;
		Jefe jefe_RRHH = (Jefe) nomina[longNomina-1];
		jefe_RRHH.establecerIncentivo(250000);
		
		for(int i=0;i<longNomina;i++) {
			
			nomina[i].aumentarSueldo(5);
			
		}
		
		Arrays.sort(nomina);
		
		for(Empleado i:nomina) {
			
			System.out.println("Datos del empleado:"
				+ "\n- Id: " + i.devolverId()
				+ "\n- Nombre: " + i.devolverNombre()
				+ "\n- Sueldo: " + i.devolverSueldo(NumberFormat
						.getCurrencyInstance(
						new Locale("es", "VE")))
				+ "\n- Alta de contrado: " + i.devolverFechaDeAlta() 
				+ "\n");
		}
		
	}
	
	
}