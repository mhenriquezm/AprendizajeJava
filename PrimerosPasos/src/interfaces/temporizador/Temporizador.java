package interfaces.temporizador;

import java.awt.event.ActionListener;

import javax.swing.JOptionPane;
import javax.swing.Timer;

public class Temporizador {

	public static void main(String[] args) {
		
		ActionListener oyente = new DarHora();
		
		//Frecuencia de llamadas y objeto oyente de las acciones
		Timer temporizador = new Timer(5000, oyente);
		
		temporizador.start();
		
		//Detenemos el flujo de ejecución para ver las acciones
		JOptionPane.showMessageDialog(null, "Aceptar para salir");
		
		//Detenemos el programa
		System.exit(0);
		
	}

}