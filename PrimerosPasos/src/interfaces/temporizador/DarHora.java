package interfaces.temporizador;

import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DarHora implements ActionListener {
	
	public void actionPerformed(ActionEvent e) {
		
		//Ejecutamos el beep del sistema operativo
		Toolkit.getDefaultToolkit().beep();
		
		System.out.println("La hora es: " 
		+ new SimpleDateFormat("hh:mm:ss a").format(new Date()));
		
	}
	
}