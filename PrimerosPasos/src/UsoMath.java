
public class UsoMath {

	public static void main(String[] args) {
		
		//El m�todo sqrt de la clase Math devuelve un double
		double raiz = Math.sqrt(9);
		
		System.out.println("- La ra�z de 9 es: " + raiz);
		
		//Para usar una potencia necesitamos base y exponente
		double base = 5, exponente = 3, resultado = 0;
		
		/*El m�todo pow recibe 2 par�metros base y exponente, retorna 
		  el resultado de la potencia como un valor double */
		resultado = Math.pow(base, exponente);
		
		System.out.println("- La potencia: " + base + " ^ " + exponente 
			+ " = " + resultado
		);
	}

}
