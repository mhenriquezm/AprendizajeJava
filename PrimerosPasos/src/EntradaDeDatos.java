import java.util.*;//Importamos todo el paquete java.util y sus clases

public class EntradaDeDatos {

	public static void main(String[] args) {
		
		/*Como la clase Scanner no pertenece al paquete java.lang 
		debemos importarlo, creamos un objeto, para esto usamos el 
		operador new y llamamos a un constructor de la clase Scanner, 
		usamos el que recibe un InputStream, para ello utilizamos el 
		campo estatico de la clase System y asi conseguimos una 
		conexi�n con la consola para entrada*/
		Scanner entrada = new Scanner(System.in);
		
		System.out.print("Ingrese su nombre: ");
		
		/*Como String pertenece al paquete java.lang podemos usarla 
		 libremente debido a que es el paquete por defecto*/ 
		String nombre = entrada.nextLine();
		
		System.out.print("Ingrese su edad: ");
		int edad = entrada.nextInt();
		
		System.out.println("\nBienvenido/a, sus datos son:"
			+ "\n- Nombre: " + nombre
			+ "\n- Edad: " + edad
		);
		
		entrada.close();
		
	}

}
