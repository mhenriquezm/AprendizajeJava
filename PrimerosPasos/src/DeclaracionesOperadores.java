
public class DeclaracionesOperadores {

	public static void main(String[] args) {
		
		float a = 5F;//Declarada e iniciada
		float b;//Solo declarada
		
		/*Al ser un lenguaje fuertemente tipado las operacione deben de 
		  hacerse con el mismo tipo de datos*/
		b = 7F;
		
		//Dividimos las 2 variables anteriores y guardamos el resultado
		float c = b / a;
		
		//c += 6;//Incrementamos en 6 el valor de c
		
		System.out.println(c);//Imprimimos el resultado
		
	}

}
