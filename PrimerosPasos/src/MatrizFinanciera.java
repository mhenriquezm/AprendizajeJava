
public class MatrizFinanciera {

	public static void main(String[] args) {
		
		float[][] matriz = new float[5][6];
		int i, j, dim1 = matriz.length, dim2 = matriz[0].length;
		float interes = 0.10f;
		
		//Procedimiento para el calculo
		for(i=0;i<dim1;i++) {
			for(j=0;j<dim2;j++) {
				
				if(i == 0) matriz[i][j] = 10000f;
				
				else {
					matriz[i][j] = (matriz[i-1][j] * (1 + interes));
					interes += 0.01;
				}
			}
			interes = 0.10f;
		}
		
		//Muestra de resultado
		for(float[] k:matriz) {
			for(float x:k) {
				System.out.printf("%1.2f", x);
				System.out.print(" ");
			}
			System.out.println();
		}
	}

}
