import javax.swing.JOptionPane;


//Los tipos enumerados permiten almacenar valores exclusivos

public class TiposEnumerados {
	
	//Los objetos Enum no pueden ser declarados dentro del main
	enum Talla {
		MINI,
		MEDIANA,
		GRANDE,
		ENORME
	}//Creamos un objeto enum con sus valores asignados
	
	public static void main(String[] args) {
		
		Talla S = Talla.MINI;
		Talla M = Talla.MEDIANA;
		Talla L = Talla.GRANDE;
		Talla XL = Talla.ENORME;
		
		String pedido = JOptionPane.showInputDialog("Ingrese su talla: "
			+ "[S-M-L-XL]:");
		
		if(pedido.matches("([smlSML]|((xl)|(XL)))?")) {
			
			switch(pedido.toUpperCase()) {
				case "S": System.out.println(S); break;
				case "M": System.out.println(M); break;
				case "L": System.out.println(L); break;
				case "XL": System.out.println(XL); break;
			}
			
		}
		else {
			System.out.println("Talla no existente");
		}
	}

}
