
public class Constantes {

	public static void main(String[] args) {
		
		/*Las constantes se usan para tener un valor fijo que no puede
		 * se alterado a lo largo de la ejecución del programa, dicha 
		 * constante por convencion se declara en mayuscula y se le da
		 * valor inmediatamente*/
		
		
		// *************** Ejercicio *************** 
		
		/*Desarrolle una aplicación de consola que permita convertir
		  una medida en centimetros en una medida en pulgadas*/
		
		//Al usar la palabra reservada final declaramos una constante
		final double A_PULGADAS = 2.54;
		double centimetros = 6;
		double resultado = centimetros / A_PULGADAS;
		
		//Concatenamos un texto con las variables usando +
		System.out.println("En " + centimetros 
			+ "cm hay " 
			+ resultado + " pulgadas"
		);
		
	}

}
