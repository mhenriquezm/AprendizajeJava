import java.util.*;
import javax.swing.*;

public class Areas {

	public static void main(String[] args) {
		
		/* Crear un programa que calcule el �rea de:
		 * 1. Cuadrado
		 * 2. Rect�ngulo
		 * 3. Tri�ngulo
		 * 4. C�rculo*/
		
		Scanner entrada = new Scanner(System.in);
		
		//La instrucci�n \n genera un salto de l�nea
		System.out.println(
			"**********************************************"
			+ "\n********** MENU PARA CALCULAR AREAS **********"
			+ "\n**********************************************"
			+ "\n1. �rea de un cuadrado"
			+ "\n2. �rea de un rect�ngulo"
			+ "\n3. �rea de un tri�ngulo"
			+ "\n4. �rea de un c�rculo"
			+ "\n5. Salir"
			+ "\n**********************************************"
		);
		
		System.out.print("Ingrese una opci�n: ");
		char opcion = entrada.next().charAt(0);//Validamos la entrada
		
		System.out.println();
		//Codicional m�ltiple, permite evaluar varias condiciones fijas
		switch(opcion) {
			case '1':
				//Solicitamos la arista
				double arista = Double.parseDouble(JOptionPane
					.showInputDialog("Ingrese uno de los lados")
				);
				
				System.out.print("El �rea del cuadrado es: ");
				System.out.printf("%1.2f", Math.pow(arista, 2));
			break;
			case '2':
				double base = 0, altura = 0;
				
				base = Double.parseDouble(JOptionPane.showInputDialog(
					"Ingrese la base:"
				));
				
				altura = Double.parseDouble(JOptionPane.showInputDialog(
					"Ingrese la altura:"
				));
				
				System.out.print("El �rea del rect�ngulo es: ");
				System.out.printf("%1.2f", (base * altura));
			break;
			case '3':
				base = Double.parseDouble(JOptionPane.showInputDialog(
					"Ingrese la base:"
				));
				
				altura = Double.parseDouble(JOptionPane.showInputDialog(
					"Ingrese la altura:"
				));
				
				System.out.print("El �rea del tri�ngulo es: ");
				System.out.printf("%1.2f", ((base * altura) / 2));
			break;
			case '4':
				double radio = Double.parseDouble(JOptionPane
					.showInputDialog("Ingrese el radio")
				);
				
				System.out.print("El �rea del c�rculo es: ");
				System.out.printf("%1.2f", 
					(Math.PI * Math.pow(radio, 2))
				);
			break;
			case '5': 
				System.out.println("Adi�s..."); 
			break;
			default: 
				System.out.println("Opci�n no contemplada");
		}
		
		//Cerramos el flujo de datos con la consola
		entrada.close();
	}

}
