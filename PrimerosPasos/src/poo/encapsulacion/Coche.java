package poo.encapsulacion;

public class Coche {
	
	public static void main(String[] args) {
		
		Plataforma audi = new Plataforma();
		
		//audi.ruedas = 3; Campos no visibles
		
		//System.out.println(audi.ruedas); Campos no visibles
		System.out.println(audi);
		
	}
	
}
