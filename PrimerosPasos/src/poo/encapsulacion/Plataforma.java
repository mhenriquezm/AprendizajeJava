package poo.encapsulacion;

public class Plataforma {
	
	public Plataforma() {
		
		ruedas = 4;
		largo = 2000F;
		ancho = 300F;
		peso = 500F;
	}
	
	public String devolverCaracteristicas() {
		return "Características del vehículo:"
				+ "\n- Ruedas: " + ruedas
				+ "\n- Largo: " + largo + " cm."
				+ "\n- Ancho: " + ancho + " cm."
				+ "\n- Peso: " + peso + " kg.";
	}
	
	/*Al colocar el modificador de acceso private garantizamos la 
	 * integridad de la información al prohivir la manipulación de los
	 * atributos de la clase a cualquier entidad externa*/
	private int ruedas;
	private float largo, ancho, peso;
	
}
