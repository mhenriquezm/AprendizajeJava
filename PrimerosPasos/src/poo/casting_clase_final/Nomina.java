package poo.casting_clase_final;

import java.text.NumberFormat;
import java.util.Locale;

public class Nomina {
	
	public static void main(String[] args) {
		
		Empleado[] nomina = {
			new Empleado("Sergio Cova"),
			new Empleado("Ender Paredes", 350000,	1, 1, 2018),
			new Empleado("Fernanda Miranda"),
			new Empleado("Hector Bastidas"),
			new Jefe("Manuel Henriquez", 600000, 14, 5, 2019),
			new Jefe("Raquel Herrera", 850000, 10, 10, 2019)
		};
		
		int longNomina = nomina.length;
		Jefe jefe;//Creamos un objeto Jefe para refundir
		
		for(int i=0;i<longNomina;i++) {
			
			nomina[i].aumentarSueldo(5);
			
			//Usamos instanceof para verificar a que clase pertenece
			if(nomina[i] instanceof Jefe) {
				
				/*En caso de ser Jefe refundimos el objeto Empleado
				 porque un Jefe es un Empleado*/
				jefe = (Jefe) nomina[i];
				
				switch(nomina[i].devolverId()) {
					case 5: jefe.establecerIncentivo(1200000); break;
					case 6: jefe.establecerIncentivo(250000); break;
				}
				
			}
			
		}
		
		for(Empleado i:nomina) {
			
			System.out.println("Datos del empleado:"
				+ "\n- Id: " + i.devolverId()
				+ "\n- Nombre: " + i.devolverNombre()
				+ "\n- Sueldo: " + i.devolverSueldo(NumberFormat
						.getCurrencyInstance(
						new Locale("es", "VE")))
				+ "\n- Alta de contrado: " + i.devolverFechaDeAlta() 
				+ "\n");
		}
		
	}
	
	
}