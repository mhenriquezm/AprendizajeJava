package poo.casting_clase_final;

import java.text.NumberFormat;

//Al crear una clase final detenemos la cadena de herencia
public final class Jefe extends Empleado {
	
	//M�todos constructores
	public Jefe(String nombre, double sueldo, int dia, int mes, 
			int anio) {
		
		super(nombre, sueldo, dia, mes, anio);
	}
	
	public Jefe(String nombre) {
		super(nombre);
	}
	
	//M�todos Setter
	public void establecerIncentivo(double incentivo) {
		this.incentivo = incentivo;
	}
	
	//M�todos sobre escritos
	public double devolverSueldo() {
		return super.devolverSueldo() + incentivo;
	}
	
	//Un m�todo final impide a una subclase sobre escribirlo
	public final String devolverSueldo(NumberFormat formato) {
		return formato.format(super.devolverSueldo() + incentivo);
	}
	
	//Atributos
	private double incentivo;
	
}
