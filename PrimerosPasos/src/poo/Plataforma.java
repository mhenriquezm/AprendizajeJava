//Si estamos en un paquete usamos la directiva package seguido del nombre

package poo;

//Si necesitamos importar un paquete se coloca a continuaci�n de package

public class Plataforma {//Clase public es la clase principal del fichero
	
	//M�todo constructor, es el encargado de iniciar el objeto
	public Plataforma() {
		
		//El constructor es publico y tiene el mismo nombre de la clase
		ruedas = 4;
		largo = 2000F;
		ancho = 300F;
		peso = 500F;
	}
	
	//Atributos de clase
	int ruedas;
	float largo, ancho, peso;
	
}
