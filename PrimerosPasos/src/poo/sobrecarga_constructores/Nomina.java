package poo.sobrecarga_constructores;

import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;

public class Nomina {
	
	public static void main(String[] args) {
		
		Empleado[] nomina = {
			new Empleado("Manuel Henriquez", 85000, 14, 5, 2019),
			new Empleado("Raquel Herrera", 95000, 1, 12, 2019),
			new Empleado("Paola Henriquez", 105000,	1, 1, 2018),
			new Empleado("Alvaro de la Rosa")
		};
		
		//El objeto formato se encargar� del formato moneda
		NumberFormat formato = NumberFormat.getCurrencyInstance(
			new Locale("es", "ES"));
		
		int longNomina = nomina.length;
		
		for(int i=0;i<longNomina;i++) {
			
			nomina[i].aumentarSueldo(5);
		}
		
		for(Empleado i:nomina) {
			
			System.out.println("Datos del empleado:"
				+ "\n- Nombre: " + i.devolverNombre()
				+ "\n- Sueldo: " + formato.format(i.devolverSueldo())
				+ "\n- Alta de contrado: " + i.devolverFechaDeAlta() 
				+ "\n");
		}
		
	}
	
	
}

class Empleado {
	
	//M�todos Constructores
	public Empleado(String nombre, double sueldo, int dia, int mes,
		int anio) {
		
		this.nombre = nombre;
		this.sueldo = sueldo;
		
		//Construimos la fecha
		GregorianCalendar calendario = new GregorianCalendar(anio, 
			(mes - 1), dia);//Los meses van de 0-11
		
		//Almacenamos la fecha con el m�todo getTime()
		altaContrato = calendario.getTime();
		
		
	}
	
	//Sobrecarga de constructores, debemos pasar distintos par�metros
	public Empleado(String nombre) {
		
		//Podemos usar el this para llamar al otro constructor
		this(nombre, 30000, 1, 1, 2000);
	}
	
	//M�todos Setter
	public void aumentarSueldo(double porcentaje) {
		
		//Procedimiento para aumentar el sueldo el porcentaje dado
		sueldo *= (1 + (porcentaje / 100));
		
	}
	
	//M�todos Getter
	public String devolverNombre() {
		return nombre;
	}
	
	public double devolverSueldo() {
		return sueldo;
	}
	
	public String devolverFechaDeAlta() {
		
		//Validamos la fecha
		if(altaContrato == null) {
			return "dd/mm/aaaa";
		}
		
		//Damos formato a la fecha con la clase SimpleDateFormat
		return new SimpleDateFormat("dd/MM/yyyy").format(altaContrato);
	}
	
	//Atributos
	private String nombre;
	private double sueldo;
	private Date altaContrato;
}