package poo.abstraccion;

public class UsoPersona {

	public static void main(String[] args) {
		
		//Polimorfismo
		Persona[] personas = {
			new Alumno("Raquel Herrera", "Derecho"),
			new Empleado("Manuel Henriquez", 2000000, 14, 5, 2019)
		};
		
		for(Persona i:personas) {
			System.out.println(i.devolverDescripcion() + "\n");
		}
		
	}

}
