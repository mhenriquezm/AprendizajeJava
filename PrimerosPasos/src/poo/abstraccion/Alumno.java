package poo.abstraccion;

public class Alumno extends Persona {
	
	//M�todos constructores
	public Alumno(String nombre, String carrera) {
		
		super(nombre);
		this.carrera = carrera;
		
	}
	
	//M�todos abstractos sobreescritos
	public String devolverDescripcion() {
		return "Descripci�n del alumno:"
			+ "\n- Nombre: " + super.devolverNombre()
			+ "\n- Carrera: " + this.carrera;
	}
	
	//Atributos
	private String carrera;
}
