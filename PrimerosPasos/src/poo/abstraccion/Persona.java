package poo.abstraccion;

//Al existir un m�todo abstracto la clase debe ser abstracta
public abstract class Persona {
	
	//M�todos constructores
	public Persona(String nombre) {
		this.nombre = nombre;
	}
	
	//M�todos Getter
	public String devolverNombre() {
		return nombre;
	}
	
	//Los m�todos abstractos s�lo se declaran, no tienen un cuerpo
	public abstract String devolverDescripcion();
	
	//Atributos
	private String nombre;
}
