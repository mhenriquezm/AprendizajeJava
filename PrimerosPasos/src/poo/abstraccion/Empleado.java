package poo.abstraccion;

import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;

public class Empleado extends Persona {
	
	//M�todos Constructores
	public Empleado(String nombre, double sueldo, int dia, int mes,
		int anio) {
		
		/*Las clases abstractas no se pueden instanciar, pero al heredar
		de ellas podemos llamar a su m�todo constructor a trav�s del
		operador super*/
		super(nombre);
		this.sueldo = sueldo;
		
		GregorianCalendar calendario = new GregorianCalendar(anio, 
			(mes - 1), dia);
		
		altaContrato = calendario.getTime();
		
		IdSiguiente++;
		Id = IdSiguiente;
		
	}
	
	//M�todos Setter
	public void aumentarSueldo(double porcentaje) {
		sueldo *= (1 + (porcentaje / 100));
	}
	
	//M�todos Getter
	public int devolverId() {
		return Id;
	}
	
	public String devolverFechaDeAlta() {
		
		if(altaContrato == null) {
			return "dd/mm/aaaa";
		}
		
		return new SimpleDateFormat("dd/MM/yyyy").format(altaContrato);
	}
	
	//Sobrecarga de m�todos
	public String devolverSueldo(NumberFormat formato) {
		return formato.format(sueldo);
	}
	
	public double devolverSueldo() {
		return sueldo;
	}
	
	//M�todo abstracto sobreescrito (Patr�n de dise�o)
	public String devolverDescripcion() {
		return "Descripci�n del empleado:"
			+ "\n- Id: " + this.Id
			+ "\n- Nombre: " + super.devolverNombre()
			+ "\n- Sueldo: " + this.devolverSueldo(NumberFormat
					.getCurrencyInstance(new Locale("es", "VE")))
			+ "\n- Alta de contrato: " + this.devolverFechaDeAlta();
	}
	
	//Atributos
	private double sueldo;
	private Date altaContrato;
	private int Id;
	private static int IdSiguiente = 0;
}