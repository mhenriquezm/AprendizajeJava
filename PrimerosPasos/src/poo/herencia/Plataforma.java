package poo.herencia;

import java.text.DecimalFormat;

public class Plataforma {
	
	//M�todos Constructores
	public Plataforma() {
		
		ruedas = 4;
		largo = 2000F;
		ancho = 300F;
		pesoPlataforma = 500F;
	}
	
	//M�todos Setter
	public void establecerColor(String color) {
		
		this.color = color;
	}
	
	public void establecerAsientos(String asientosCuero) {
		
		if(asientosCuero.equalsIgnoreCase("si")) {
			
			this.asientosCuero = true;
		}
		else {
			
			this.asientosCuero = false;
		}
		
	}
	
	public void establecerClimatizador(String climatizador) {
		
		if(climatizador.equalsIgnoreCase("si")) {
			
			this.climatizador = true;
		}
		else {
			
			this.climatizador = false;
		}
		
	}
	
	//M�todos Getter
	public String devolverDatosGenerales() {

		return "Caracter�sticas generales del veh�culo: "
			+ "\n- Ruedas: "	+ ruedas + "."
			+ "\n- Largo: "	+ (largo/1000) + " m."
			+ "\n- Ancho: "	+ ancho + " cm."
			+ "\n- Peso: "	
			+ new DecimalFormat("#,##0.00").format(pesoPlataforma)
			+ " kg.";
	}
	
	public String devolverColor() {
		
		return "- Color: " + color;
	}
	
	public String devolverAsientos() {
		
		if(asientosCuero) {
			return "- Tipo de asientos: Cuero.";
		}
		
		return "- Tipo de asientos: Serie.";
	}
	
	public String devolverClimatizador() {
		
		if(climatizador) {
			
			return "- Tipo de ventilaci�n: Climatizador.";
		}
		
		return "- Tipo de ventilaci�n: Aire acondicionado.";
	}
	
	public String devolverPrecio() {
		
		float precioFinal = 10000F;
		
		if(asientosCuero) precioFinal += 2000;
		
		if(climatizador) precioFinal += 1500;
		
		return new DecimalFormat("#,##0.00").format(precioFinal);
	}
	
	public String devolverPesoTotal() {

		float pesoCarroceria = 500F;
		
		pesoTotal = pesoPlataforma + pesoCarroceria;
		
		if(asientosCuero) pesoTotal += 50;
		
		if(climatizador) pesoTotal += 20;

		return "- Peso total: " 
		+ new DecimalFormat("#,##0.00").format(pesoTotal) + " kg.";
	}
	
	//Atributos comunes
	private int ruedas;
	private float largo, ancho, pesoPlataforma;
	
	//Atributos variables
	private String color;
	private float pesoTotal;
	private boolean asientosCuero, climatizador;
	
}
