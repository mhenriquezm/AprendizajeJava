package poo.herencia;

public class Consecionario {
	
	public static void main(String[] args) {
		
		Plataforma audi = new Plataforma();
		Furgoneta vmw = new Furgoneta(7, 580F);
		
		audi.establecerColor("Negro");
		
		//La Herencia permite usar propiedades y m�todos de la clase padre
		vmw.establecerColor("Blanco");
		vmw.establecerAsientos("Si");
		vmw.establecerClimatizador("Si");
		
		System.out.println(
			
			audi.devolverDatosGenerales()
			+ "\n" + audi.devolverColor()
			+ "\n\n" + vmw.devolverDatosGenerales()
			+ "\n" + vmw.devolverDatosFurgoneta()
		);
	}
	
}
