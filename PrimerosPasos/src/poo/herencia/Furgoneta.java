package poo.herencia;

import java.text.NumberFormat;

//Usamos la palabra reservada extends para heredar de otra clase
public class Furgoneta extends Plataforma {
	
	//M�todos constructores
	public Furgoneta(int plazasExtra, float capacidadCarga) {
		
		//Llamamos al constructor de la clase padre
		super();
		
		//Creamos el estado inicial de la furgoneta
		this.plazasExtra = plazasExtra;
		this.capacidadCarga = capacidadCarga;
		formato = NumberFormat.getNumberInstance();
	}
	
	//M�todos Getter
	public String devolverDatosFurgoneta() {
		
		return 
			"- Capacidad de carga: " + formato.format(capacidadCarga) 
			+ " Kg."
			+ "\n- Plazas extra: " + formato.format(plazasExtra)
			+ " personas.";
	}
	
	//Atributos
	private int plazasExtra;
	private float capacidadCarga;
	private NumberFormat formato;
	
}
