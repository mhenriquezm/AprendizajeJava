package poo.herencia.diseniar_herencia;

import java.text.NumberFormat;
import java.util.Locale;

public class Nomina {
	
	public static void main(String[] args) {
		
		//Creamos una instancia de Jefe
		Jefe analistaDesarrollo = new Jefe("Manuel Henriquez", 2000000, 
				14, 5, 2019);
		
		//Establecemos un incentivo
		analistaDesarrollo.establecerIncentivo(50000);
		
		//Imprimimos los datos del jefe
		System.out.println("Datos del jefe:"
				+ "\n- Id: " + analistaDesarrollo.devolverId()
				+ "\n- Nombre: " + analistaDesarrollo.devolverNombre()
				+ "\n- Sueldo: " + analistaDesarrollo.devolverSueldo(
						NumberFormat.getCurrencyInstance(
						new Locale("es", "VE")))
				+ "\n- Alta de contrado: " + analistaDesarrollo
				.devolverFechaDeAlta()
				+ "\n");
		
		Empleado[] nomina = {
			new Empleado("Raquel Herrera", 420000, 10, 10, 2019),
			new Empleado("Ender Paredes", 350000,	1, 1, 2018),
			new Empleado("Fernanda Miranda"),
			new Empleado("Hector Bastidas")
		};
		
		int longNomina = nomina.length;
		
		for(int i=0;i<longNomina;i++) {
			
			nomina[i].aumentarSueldo(5);
		}
		
		for(Empleado i:nomina) {
			
			System.out.println("Datos del empleado:"
				+ "\n- Id: " + i.devolverId()
				+ "\n- Nombre: " + i.devolverNombre()
				+ "\n- Sueldo: " + i.devolverSueldo(NumberFormat
						.getCurrencyInstance(
						new Locale("es", "VE")))
				+ "\n- Alta de contrado: " + i.devolverFechaDeAlta() 
				+ "\n");
		}
		
	}
	
	
}