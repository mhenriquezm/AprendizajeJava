package poo.herencia.diseniar_herencia;

import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;

public class Empleado {
	
	//M�todos Constructores
	public Empleado(String nombre, double sueldo, int dia, int mes,
		int anio) {
		
		this.nombre = nombre;
		this.sueldo = sueldo;
		
		GregorianCalendar calendario = new GregorianCalendar(anio, 
			(mes - 1), dia);
		
		altaContrato = calendario.getTime();
		
		IdSiguiente++;
		Id = IdSiguiente;
		
	}
	
	public Empleado(String nombre) {
		this(nombre, 40000, 1, 1, 2000);
	}
	
	//M�todos Setter
	public void aumentarSueldo(double porcentaje) {
		sueldo *= (1 + (porcentaje / 100));
	}
	
	//M�todos Getter
	public int devolverId() {
		return Id;
	}
	
	public String devolverNombre() {
		return nombre;
	}
	
	public String devolverFechaDeAlta() {
		
		if(altaContrato == null) {
			return "dd/mm/aaaa";
		}
		
		return new SimpleDateFormat("dd/MM/yyyy").format(altaContrato);
	}
	
	//Sobrecarga de m�todos
	public String devolverSueldo(NumberFormat formato) {
		return formato.format(sueldo);
	}
	
	public double devolverSueldo() {
		return sueldo;
	}
	
	//Atributos
	private String nombre;
	private double sueldo;
	private Date altaContrato;
	private int Id;
	private static int IdSiguiente = 0;
}
