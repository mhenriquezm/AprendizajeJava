package poo.construccion_objetos;

import java.text.DecimalFormat;

public class Plataforma {
	
	public Plataforma() {
		
		ruedas = 4;
		largo = 2000F;
		ancho = 300F;
		pesoPlataforma = 500F;
	}
	
	//M�todos Setter o definidores, son void y no retornan nada
	
	//Recibimos un par�metro String con el color del veh�culo
	public void establecerColor(String color) {
		
		//El operador this hace referencia a la clase
		this.color = color;
	}
	
	public void establecerAsientos(String asientosCuero) {
		
		//Validando el par�metro comprobamos el tipo de asientos
		if(asientosCuero.equalsIgnoreCase("si")) {
			
			this.asientosCuero = true;
		}
		else {
			
			this.asientosCuero = false;
		}
		
	}
	
	public void establecerClimatizador(String climatizador) {
		
		if(climatizador.equalsIgnoreCase("si")) {
			
			this.climatizador = true;
		}
		else {
			
			this.climatizador = false;
		}
		
	}
	
	//M�todos Getter o m�todos captadores
	
	public String devolverDatosGenerales() {
		//El m�todo Getter debe obligatoriamente retornar un valor
		return "Caracter�sticas generales del veh�culo: "
			+ "\n- Ruedas: "	+ ruedas + "."
			+ "\n- Largo: "	+ (largo/1000) + " m."
			+ "\n- Ancho: "	+ ancho + " cm."
			+ "\n- Peso: "	
			+ new DecimalFormat("#,##0.0#").format(pesoPlataforma)
			+ " kg.";
	}
	
	//Este m�todo devuelve un String
	public String devolverColor() {
		
		return "- Color: " + color;
	}
	
	public String devolverAsientos() {
		
		if(asientosCuero) {
			//El m�todo termina al ejecutar el return
			return "- Tipo de asientos: Cuero.";
		}
		
		return "- Tipo de asientos: Serie.";
	}
	
	public String devolverClimatizador() {
		
		if(climatizador) {
			
			return "- Tipo de ventilaci�n: Climatizador.";
		}
		
		return "- Tipo de ventilaci�n: Aire acondicionado.";
	}
	
	public String devolverPrecio() {
		
		float precioFinal = 10000F;
		
		if(asientosCuero) precioFinal += 2000;
		
		if(climatizador) precioFinal += 1500;
		
		return new DecimalFormat("#,##0.0#").format(precioFinal);
	}
	
	//M�todo Setter y Getter (desaconsejado)
	public String devolverPesoTotal() {
		
		//Establecemos el valor del peso total
		float pesoCarroceria = 500F;
		
		pesoTotal = pesoPlataforma + pesoCarroceria;
		
		if(asientosCuero) pesoTotal += 50;
		
		if(climatizador) pesoTotal += 20;
		
		//Retornamos el valor del peso total
		return "- Peso total: " 
		+ new DecimalFormat("#,##0.0#").format(pesoTotal) + " kg.";
	}
	
	//Atributos comunes
	private int ruedas;
	private float largo, ancho, pesoPlataforma;
	
	//Atributos variables
	private String color;
	private float pesoTotal;
	private boolean asientosCuero, climatizador;
	
}
