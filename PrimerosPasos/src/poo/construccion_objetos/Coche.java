package poo.construccion_objetos;

import javax.swing.JOptionPane;

public class Coche {
	
	public static void main(String[] args) {
		
		Plataforma audi = new Plataforma();
		
		audi.establecerColor(JOptionPane.showInputDialog(
				"�De qu� color desea su veh�culo?"
			));
		audi.establecerAsientos(JOptionPane.showInputDialog(
				"�Desea tener asientos de cuero (Si/No)?"
			));
		audi.establecerClimatizador(JOptionPane.showInputDialog(
				"�Desea tener climatizador (Si/No)?"
			));
		
		System.out.println(audi.devolverDatosGenerales());
		System.out.println(
			"\nCaracter�sticas particulares:"
			+ "\n" + audi.devolverColor()
			+ "\n" + audi.devolverAsientos()
			+ "\n" + audi.devolverClimatizador()
			+ "\n" + audi.devolverPesoTotal()
			+ "\n- Precio: " + audi.devolverPrecio() + " �."
		);
		
	}
	
}
