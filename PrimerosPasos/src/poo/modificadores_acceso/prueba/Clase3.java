package poo.modificadores_acceso.prueba;

//Importamos la Clase1 porque est� en otro paquete
import poo.modificadores_acceso.Clase1;

/*Modificadores de acceso visibilidad:

 * Public: 
 	* Clase:    Si
 	* Paquete:  Si
 	* Subclase: Si
 	* Todos:    Si
 
 * Protected: 
 	* Clase:    Si
 	* Paquete:  Si
 	* Subclase: Si (Incluso una subclase que herede en otro paquete)
 	* Todos:    No
 
  * Private: 
 	* Clase:    Si
 	* Paquete:  No
 	* Subclase: No
 	* Todos:    No
 
 * Por defecto (no colocar nada): 
 	* Clase:    Si
 	* Paquete:  Si
 	* Subclase: No (Solo es visible si se hereda en el mismo paquete)
 	* Todos:    No*/

public class Clase3 extends Clase1 {

}
