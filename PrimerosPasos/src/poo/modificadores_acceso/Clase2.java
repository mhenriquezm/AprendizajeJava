package poo.modificadores_acceso;

import poo.modificadores_acceso.prueba.Clase3;

/*Modificadores de acceso visibilidad:

 * Public: 
 	* Clase:    Si
 	* Paquete:  Si
 	* Subclase: Si
 	* Todos:    Si
 
 * Protected: 
 	* Clase:    Si
 	* Paquete:  Si
 	* Subclase: Si (Incluso una subclase que herede en otro paquete)
 	* Todos:    No
 
  * Private: 
 	* Clase:    Si
 	* Paquete:  No
 	* Subclase: No
 	* Todos:    No
 
 * Por defecto (no colocar nada): 
 	* Clase:    Si
 	* Paquete:  Si
 	* Subclase: No (Solo es visible si se hereda en el mismo paquete)
 	* Todos:    No*/

public class Clase2 {

	public static void main(String[] args) {
		
		Clase1 objeto1 = new Clase1();//Constructor por defecto
		Clase3 objeto2 = new Clase3();
		
		//Podemos acceder al campo por estar en el mismo paquete
		System.out.println(objeto1.variable1);
		
		//Podemos acceder desde la subclase por ser protected
		System.out.println(objeto2.variable2);
	}

}
