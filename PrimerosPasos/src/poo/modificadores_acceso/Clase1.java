package poo.modificadores_acceso;

/*Modificadores de acceso visibilidad:
 
 * Public: 
 	* Clase:    Si
 	* Paquete:  Si
 	* Subclase: Si
 	* Todos:    Si
 
 * Protected: 
 	* Clase:    Si
 	* Paquete:  Si
 	* Subclase: Si (Incluso una subclase que herede en otro paquete)
 	* Todos:    No
 
  * Private: 
 	* Clase:    Si
 	* Paquete:  No
 	* Subclase: No
 	* Todos:    No
 
 * Por defecto (no colocar nada): 
 	* Clase:    Si
 	* Paquete:  Si
 	* Subclase: No (Solo es visible si se hereda en el mismo paquete)
 	* Todos:    No*/

public class Clase1 {
	
	String getVariable2() {
		return "El valor de la variable2 es: " + variable2;
	}
	
	int variable1 = 5;
	protected int variable2 = 7;
}
