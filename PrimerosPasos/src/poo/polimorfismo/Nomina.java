package poo.polimorfismo;

import java.text.NumberFormat;
import java.util.Locale;

public class Nomina {
	
	public static void main(String[] args) {
		
		//Creamos una instancia de Jefe
		Jefe analistaDesarrollo = new Jefe("Manuel Henriquez", 600000, 
				14, 5, 2019);
		
		//Establecemos un incentivo
		analistaDesarrollo.establecerIncentivo(500000);
		
		Empleado[] nomina = {
			new Empleado("Sergio Cova"),
			new Empleado("Ender Paredes", 350000,	1, 1, 2018),
			new Empleado("Fernanda Miranda"),
			new Empleado("Hector Bastidas"),
			/*Al ser nomina un array de Empleado puede almacenar un
			  objeto de tipo Jefe porque por el principio de sustitución
			  un Jefe es un Empleado, es el concepto de polimorfismo
			*/
			analistaDesarrollo,
			new Jefe("Raquel Herrera", 850000, 10, 10, 2019)
		};
		
		int longNomina = nomina.length;
		
		for(int i=0;i<longNomina;i++) {
			
			nomina[i].aumentarSueldo(5);
		}
		
		for(Empleado i:nomina) {
			
			System.out.println("Datos del empleado:"
				+ "\n- Id: " + i.devolverId()
				+ "\n- Nombre: " + i.devolverNombre()
				+ "\n- Sueldo: " + i.devolverSueldo(NumberFormat
						.getCurrencyInstance(
						new Locale("es", "VE")))
				+ "\n- Alta de contrado: " + i.devolverFechaDeAlta() 
				+ "\n");
		}
		
	}
	
	
}