package poo.setter_getter;

public class Plataforma {
	
	public Plataforma() {
		
		ruedas = 4;
		largo = 2000F;
		ancho = 300F;
		pesoPlataforma = 500F;
	}
	
	//M�todos Setter o definidores, son void y no retornan nada
	
	//Recibimos un par�metro String con el color del veh�culo
	public void establecerColor(String color) {
		
		//El operador this hace referencia a la clase
		this.color = color;
	}
	
	public void establecerAsientos(String asientosCuero) {
		
		//Validando el par�metro comprobamos el tipo de asientos
		if(asientosCuero.equalsIgnoreCase("si")) {
			
			this.asientosCuero = true;
		}
		else {
			
			this.asientosCuero = false;
		}
		
	}
	
	//M�todos Getter o m�todos captadores
	
	public String devolverDatosGenerales() {
		//El m�todo Getter debe obligatoriamente retornar un valor
		return "Caracter�sticas generales del veh�culo: "
			+ "\n- Ruedas: "	+ ruedas + "."
			+ "\n- Largo: "	+ (largo/1000) + " m."
			+ "\n- Ancho: "	+ ancho + " cm."
			+ "\n- Peso: "	+ pesoPlataforma + " kg.";
	}
	
	//Este m�todo devuelve un String
	public String devolverColor() {
		
		return "- Color: " + color;
	}
	
	public String devolverAsientos() {
		
		if(asientosCuero) {
			//El m�todo termina al ejecutar el return
			return "- Tipo de asientos: Cuero.";
		}
		
		return "- Tipo de asientos: Serie.";
	}
	
	public String devolverDatosParticulares() {
		
		pesoTotal = pesoPlataforma + 120;
		climatizador = false;
		
		return "Caracter�sticas particulares del veh�culo: "
			+ "\n- Peso total: "	+ pesoTotal + " kg."
			+ "\n- Peso total: "	+ pesoTotal + " kg."
			+ "\n- Climatizador: "	+ climatizador;
	}
	
	//Atributos comunes
	private int ruedas;
	private float largo, ancho, pesoPlataforma;
	
	//Atributos variables
	private String color;
	private float pesoTotal;
	private boolean asientosCuero, climatizador;
	
}
