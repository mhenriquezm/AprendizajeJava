package poo.setter_getter;

public class Coche {
	
	public static void main(String[] args) {
		
		Plataforma audi = new Plataforma();
		
		audi.establecerColor("Negro");
		audi.establecerAsientos("Si");
		
		System.out.println(audi.devolverDatosGenerales());
		System.out.println(
			"\nCaracterÝsticas particulares:"
			+ "\n" + audi.devolverColor()
			+ "\n" + audi.devolverAsientos()
		);
		
	}
	
}
