package poo.uso_static;

/* Crear una n�mina de empleados a los que se le asignar� una secci�n
 * y se registrar� con su nombre. Luego de registrarse deber�n iniciar
 * en el departamento de administraci�n, por m�ritos y tiempo en la
 * empresa subir� de cargo. Asegurarse de validar el campo nombre para
 * que no pueda ser cambiado luego se ser asignado al trabajador.
 * Luego se le debe asignar un Id �nico, corelativo e incremental 
 * por cada trabajador registrado.*/

public class Empresa {

	public static void main(String[] args) {
		
		Empleado trabajador1 = new Empleado("Manuel Henriquez");
		Empleado trabajador2 = new Empleado("Raquel Herrera");
		Empleado trabajador3 = new Empleado("Paola Henriquez");
		Empleado trabajador4 = new Empleado("Alvaro de la Rosa");
		
		//Cambiamos la secci�n
		trabajador1.establecerSeccion("Tecnolog�a y desarrollo");
		
		System.out.println(
			trabajador1.devolverDatos() 
			+ "\n\n"
			+ trabajador2.devolverDatos()
			+ "\n\n"
			+ trabajador3.devolverDatos()
			+ "\n\n"
			+ trabajador4.devolverDatos());
		
	}

}

class Empleado {
	
	//M�todos constructores
	public Empleado(String nombre) {
		
		Id++;
		this.nombre = nombre;
		this.seccion = "Administraci�n";
		this.IdSiguiente = Id;
		
	}
	
	//M�todos Setter
	public void establecerSeccion(String seccion) {
		
		this.seccion = seccion;
	}
	
	//M�todos Getter
	public String devolverDatos() {
		
		return "Datos del empleado:"
			+ "\n- Id: " + IdSiguiente
			+ "\n- Nombre: " + nombre
			+ "\n- Secci�n: " + seccion;
	}
	
	//Atributos
	private final String nombre;
	private String seccion;
	private int IdSiguiente;
	private static int Id = 0;
	
}