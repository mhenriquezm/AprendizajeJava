package poo.uso_static.metodos;

//Crear un m�todo que devuelva el Id static de la clase

public class Empresa {

	public static void main(String[] args) {
		
		Empleado trabajador1 = new Empleado("Manuel Henriquez");
		Empleado trabajador2 = new Empleado("Raquel Herrera");
		Empleado trabajador3 = new Empleado("Paola Henriquez");
		Empleado trabajador4 = new Empleado("Alvaro de la Rosa");
		
		trabajador1.establecerSeccion("Tecnolog�a y desarrollo");
		
		System.out.println(
			trabajador1.devolverDatos() 
			+ "\n\n"
			+ trabajador2.devolverDatos()
			+ "\n\n"
			+ trabajador3.devolverDatos()
			+ "\n\n"
			+ trabajador4.devolverDatos());
		
		System.out.println("\n" + Empleado.devolverIdSiguiente());
		
	}

}

class Empleado {
	
	//M�todos constructores
	public Empleado(String nombre) {
		
		Id++;
		this.nombre = nombre;
		this.seccion = "Administraci�n";
		this.IdSiguiente = Id;
		
	}
	
	//M�todos Setter
	public void establecerSeccion(String seccion) {
		
		this.seccion = seccion;
	}
	
	//M�todos Getter
	public String devolverDatos() {
		
		return "Datos del empleado:"
			+ "\n- Id: " + IdSiguiente
			+ "\n- Nombre: " + nombre
			+ "\n- Secci�n: " + seccion;
	}
	
	public static String devolverIdSiguiente() {
		
		/*Los m�todos static no actuan en campos de ejemplar o clase
		 * ni pueden acceder a ellos a no ser que ese campo sea static*/
		return "El siguiente Id es: " + (Id + 1);
	}
	
	//Atributos
	private final String nombre;
	private String seccion;
	private int IdSiguiente;
	private static int Id = 0;
	
}