package poo.modularizacion;

/*La modularizaci�n consiste en dividir un gran programa en peque�os
 * m�dulos o clases, se puede modularizar utilizando un unico fichero
 * como en este caso tomando en cuenta que la clase principal es la
 * que tiene el m�todo main y de donde parte la ejecuci�n del programa
 * y ademas tiene el modificador de acceso public o separar cada clase
 * en ficheros diferentes (es la modularizaci�n al 100%)*/

public class Coche {
	
	//La clase principal es la que tiene el m�todo main
	public static void main(String[] args) {
		
		//Instanciar una clase, ejemplar de clase o crear un objeto
		Plataforma audi = new Plataforma();
		
		//Violaci�n a la integridad de la informaci�n
		audi.ruedas = 3;
		
		System.out.println(audi.ruedas);
		
	}
	
}

class Plataforma {
	
	//M�todo constructor, es el encargado de iniciar el objeto
	public Plataforma() {
		
		//El constructor es publico y tiene el mismo nombre de la clase
		ruedas = 4;
		largo = 2000F;
		ancho = 300F;
		peso = 500F;
	}
	
	//Atributos de clase
	int ruedas;
	float largo, ancho, peso;
	
}
