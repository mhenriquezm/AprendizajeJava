package poo.empleados;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;

//La clase publica debe tener el m�todo main y es la principal
public class Nomina {
	
	public static void main(String[] args) {
		
		//Almacenamos a los empleados en un array
		Empleado[] nomina = {
			new Empleado("Manuel Henriquez", 85000, 14, 5, 2019),
			new Empleado("Raquel Herrera", 95000, 1, 12, 2019),
			new Empleado("Paola Henriquez", 105000,	1, 1, 2018)
		};
		
		int longNomina = nomina.length;
		
		//Aumentamos los sueldos
		for(int i=0;i<longNomina;i++) {
			
			nomina[i].aumentarSueldo(5);
		}
		
		//Imprimimos los resultados
		for(Empleado i:nomina) {
			
			System.out.println("Datos del empleado:"
				+ "\n- Nombre: " + i.devolverNombre()
				+ "\n- Sueldo: " + new DecimalFormat("#,##0.00")
				.format(i.devolverSueldo()) + " $"//Formateamos a moneda
				+ "\n- Alta de contrado: " + i.devolverFechaDeAlta() 
				+ "\n");
		}
		
	}
	
	
}

//El resto de clases llevan el modificador por defecto
class Empleado {
	
	//M�todos Constructores
	public Empleado(String nombre, double sueldo, int dia, int mes,
		int anio) {
		
		this.nombre = nombre;
		this.sueldo = sueldo;
		
		//Construimos la fecha
		GregorianCalendar calendario = new GregorianCalendar(anio, 
			(mes - 1), dia);//Los meses van de 0-11
		
		//Almacenamos la fecha con el m�todo getTime()
		altaContrato = calendario.getTime();
		
		
	}
	
	//M�todos Setter
	public void aumentarSueldo(double porcentaje) {
		
		//Procedimiento para aumentar el sueldo el porcentaje dado
		sueldo *= (1 + (porcentaje / 100));
		
	}
	
	//M�todos Getter
	public String devolverNombre() {
		return nombre;
	}
	
	public double devolverSueldo() {
		return sueldo;
	}
	
	public String devolverFechaDeAlta() {
		
		//Damos formato a la fecha con la clase SimpleDateFormat
		return new SimpleDateFormat("dd/MM/yyyy").format(altaContrato);
	}
	
	//Atributos
	private String nombre;
	private double sueldo;
	private Date altaContrato;
}