package graficos;

import java.awt.Dimension;
import java.awt.Image;
import java.awt.Toolkit;
import java.io.File;

import javax.swing.JFrame;

public class MarcoCentrado {

	public static void main(String[] args) {
		
		Ventana marco = new Ventana();
		
		marco.setVisible(true);
		marco.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

}

class Ventana extends JFrame {
	
	//M�todos constructores
	public Ventana() {
		
		//Al ser una clase abstracta no se puede instanciar con new
		Toolkit pantalla = Toolkit.getDefaultToolkit();
		
		//Al tener un objeto Toolkit capturamos la resolucion de pantalla
		Dimension resolucion = pantalla.getScreenSize();
		
		//Almacenamos el ancho y el alto de la pantalla en pixeles
		int ancho = resolucion.width;
		int alto = resolucion.height;
		
		//Damos las dimensiones relativas a la pantalla, la mitad
		setSize((ancho/2), (alto/2));
		
		//Ubicamos en el centro dividiendo entre 4 el ancho y el alto
		setLocation((ancho/4), (alto/4));
		
		//Damos un t�tulo al marco
		setTitle("Marco centrado");
		
		//Guardamos la im�gen con su ruta relativa
		Image icono = pantalla.getImage("src" 
		+ File.separator + "graficos" + File.separator + "icono.gif");
		
		//Establecemos el icono del marco
		setIconImage(icono);
	}
	
	//Campos de clase
	private static final long serialVersionUID = 1L;
	
}