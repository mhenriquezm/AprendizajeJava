package graficos;

import javax.swing.JFrame;

public class PrimerMarco {

	public static void main(String[] args) {
		
		Marco marco1 = new Marco();
		
		//Hacemos visible el marco
		marco1.setVisible(true);
		
		//Le decimos al marco que termine la ejecuci�n al cerrar
		marco1.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
	}

}

class Marco extends JFrame {
	
	//M�todos constructores
	public Marco() {
		//Los marcos nacen con un tama�o inutil y hay que establecerlo
		setSize(500,300);
	}
	
	//Campos de clase
	private static final long serialVersionUID = 1L;
}