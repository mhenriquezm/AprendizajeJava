package graficos.pintar_figuras;

import java.awt.GraphicsEnvironment;

import javax.swing.JOptionPane;

public class ConsultarFuentes {

	public static void main(String[] args) {
		
		GraphicsEnvironment entornoGrafico = GraphicsEnvironment
				.getLocalGraphicsEnvironment();
		
		String[] misFuentes = entornoGrafico.getAvailableFontFamilyNames();
		String fuente = JOptionPane.showInputDialog("Fuente a buscar");
		boolean existe = false;
		
		for(String i: misFuentes) {
			
			if(fuente.equals(i)) {
				existe = true;
				break;
			}
			
		}
		
		if(existe) {
			JOptionPane.showMessageDialog(null, "La fuente: " + fuente 
					+ " est� instalada");
		}
		else {
			JOptionPane.showMessageDialog(null, "La fuente: " + fuente 
					+ " no est� instalada");
		}
		
	}

}
