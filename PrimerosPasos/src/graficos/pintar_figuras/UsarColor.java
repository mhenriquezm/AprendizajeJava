package graficos.pintar_figuras;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.SystemColor;
import java.awt.Toolkit;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Rectangle2D;

import javax.swing.JFrame;
import javax.swing.JPanel;

public class UsarColor {

	public static void main(String[] args) {
		
		MarcoColor marco = new MarcoColor();
		
		marco.setVisible(true);
		marco.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
	}

}

class MarcoColor extends JFrame {
	
	//M�todos constructores
	public MarcoColor() {
		
		super("Usando colores");
		
		Dimension pantalla = Toolkit.getDefaultToolkit().getScreenSize();
		
		int ancho = pantalla.width;
		int alto = pantalla.height;
		
		setBounds(((ancho - 400) / 2), (((alto - 400) / 2)), 400, 400);
		
		add(new LaminaColor());
		
	}
	
	//Campos de clase
	private static final long serialVersionUID = 1L;
	
}

class LaminaColor extends JPanel {
	
	//M�todos Setter
	public void paintComponent(Graphics g) {
		
		super.paintComponent(g);
		
		Graphics2D g2 = (Graphics2D) g;
		
		int ancho = getWidth();
		int alto = getHeight();
		
		Rectangle2D rectangulo = new Rectangle2D.Double(
				((ancho - 200) / 2), ((alto - 150) / 2), 200, 150);
		
		Ellipse2D elipse = new Ellipse2D.Double();
		
		elipse.setFrame(rectangulo);
		
		g2.setPaint(Color.RED);
		g2.draw(rectangulo);
		
		//Establecer el color de los trazos
		Color negro = new Color(0,0,0);
		
		g2.setPaint(negro);
		
		//Dibujar el interior de la figura
		g2.fill(rectangulo);
		
		//Con brighter y darker podemos dar brillo y opacidad
		g2.setPaint(Color.GRAY.brighter().darker());
		g2.fill(elipse);
		
		//Establecemos el color de fondo de la l�mina con el color del SO
		setBackground(SystemColor.window);
	}
	
	//Campos de clase
	private static final long serialVersionUID = 1L;
	
}
