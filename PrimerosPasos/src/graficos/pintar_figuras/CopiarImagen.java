package graficos.pintar_figuras;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Toolkit;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class CopiarImagen {

	public static void main(String[] args) {
		
		MarcoCopia marco = new MarcoCopia();
		
		marco.setVisible(true);
		marco.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
	}

}

class MarcoCopia extends JFrame {
	
	//M�todos constructores
	public MarcoCopia() {
		
		super("Copiar una im�gen");
		
		Dimension pantalla = Toolkit.getDefaultToolkit().getScreenSize();
		
		int ancho = pantalla.width;
		int alto = pantalla.height;
		
		setBounds(((ancho - 550) / 2), ((alto - 400) / 2), 550, 400);
		
		add(new LaminaCopia());
		
	}
	
	//Campos de clase
	private static final long serialVersionUID = 1L;
	
}

class LaminaCopia extends JPanel {
	
	//M�todos constructores
	public LaminaCopia() {
		
		try {
			imagen = ImageIO.read(new File(
				"src"
				+ File.separator
				+ "graficos"
				+ File.separator
				+ "pintar_figuras"
				+ File.separator
				+ "icono_pelota.gif"));
			
		}
		catch(IOException e) {
			System.out.println("Error al buscar la im�gen");
		}
		
	}
	
	//M�todos Setter
	public void paintComponent(Graphics g) {
		
		super.paintComponent(g);
		
		if(imagen == null) {
			g.setFont(new Font("Verdana", Font.BOLD, 25));
			g.drawString("La im�gen no pudo ser cargada", 50, 50);
		}
		else {
			g.drawImage(imagen, 0, 0, this);
			
			int anchoLamina = getWidth();
			int altoLamina = getHeight();
			int anchoImagen = imagen.getWidth(this);
			int altoImagen = imagen.getHeight(this);
			int i = 0, j = 0;
			
			while(i < anchoLamina) {
				while(j < altoLamina) {
					if((i + j) > 0) {
						g.copyArea(0, 0, anchoImagen, altoImagen, i, j);
					}
					
					j += altoImagen;
				}
				
				i += anchoImagen;
				j = 0;
			}
			
		}
		
	}
	
	//Campos de clase
	private Image imagen;
	private static final long serialVersionUID = 1L;
	
}