package graficos.pintar_figuras;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Toolkit;

import javax.swing.JFrame;
import javax.swing.JPanel;

public class EscribirTexto {

	public static void main(String[] args) {
		
		MarcoTexto marco = new MarcoTexto();
		
		marco.setVisible(true);
		marco.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
	}

}

class MarcoTexto extends JFrame {
	
	//M�todos constructores
	public MarcoTexto() {
		
		super("Escribiendo en el marco");
		
		Dimension resolucion = Toolkit.getDefaultToolkit().getScreenSize();
		
		int ancho = resolucion.width;
		int alto = resolucion.height;
		
		//Usamos la f�rmula: (padre - hijo) / 2 para ubicarlo en el centro
		setBounds(((ancho - 600) / 2),((alto - 450) / 2),600,450);
		
		//Agregamos la l�mina al marco
		add(new LaminaTexto());
	}
	
	//Campos de clase
	private static final long serialVersionUID = 1L;
}

class LaminaTexto extends JPanel {
	
	//M�todos Setter 
	public void paintComponent(Graphics g) {
		
		//Llamamos al m�todo de la clase padre para que haga su trabajo
		super.paintComponent(g);
		
		//Comenzamos a pintar nuestros componentes
		g.drawString("Estamos aprendiendo Swing", 100, 100);
	}
	
	//Campos de clase
	private static final long serialVersionUID = 1L;
	
}