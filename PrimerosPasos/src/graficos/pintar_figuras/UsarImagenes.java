package graficos.pintar_figuras;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Toolkit;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class UsarImagenes {

	public static void main(String[] args) {
		
		MarcoImagenes marco = new MarcoImagenes();
		
		marco.setVisible(true);
		marco.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
	}

}

class MarcoImagenes extends JFrame {
	
	//M�todos constructores
	public MarcoImagenes() {
		
		super("Trabajo con im�genes");
		
		Dimension pantalla = Toolkit.getDefaultToolkit().getScreenSize();
		
		int ancho = pantalla.width;
		int alto = pantalla.height;
		
		setBounds(((ancho - 400) / 2), ((alto - 300) / 2), 400, 300);
		
		add(new LaminaImagenes());
		
	}
	
	//Campos de clase
	private static final long serialVersionUID = 1L;
	
}

class LaminaImagenes extends JPanel {
	
	//M�todos Setter
	public void paintComponent(Graphics g) {
		
		super.paintComponent(g);
		
		//Intentar leer la im�gen
		try {
			
			imagen = ImageIO.read(new File(
					"src"
					+ File.separator 
					+ "graficos"
					+ File.separator
					+ "pintar_figuras"
					+ File.separator
					+ "ligth.jpg"));
			
			int alto = getHeight();
			int ancho = getWidth();
			
			g.drawImage(imagen, ((ancho - imagen.getWidth(this)) / 2), 
					((alto - imagen.getHeight(this)) / 2), this);
		}
		//En caso de error lanzar un mensaje
		catch (IOException e) {
			
			g.setFont(new Font("Verdana", Font.BOLD, 18));
			g.drawString("La im�gen no pudo ser cargada", 50, 50);
		}
	}
	
	//Campos de clase
	private Image imagen;
	private static final long serialVersionUID = 1L;
	
}