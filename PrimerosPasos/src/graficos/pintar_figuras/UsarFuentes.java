package graficos.pintar_figuras;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Toolkit;

import javax.swing.JFrame;
import javax.swing.JPanel;

public class UsarFuentes {

	public static void main(String[] args) {
		
		MarcoFuentes marco = new MarcoFuentes();
		
		marco.setVisible(true);
		marco.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
	}

}

class MarcoFuentes extends JFrame {
	
	//M�todos constructores
	public MarcoFuentes() {
		
		super("Uso de fuentes");
		
		Dimension pantalla = Toolkit.getDefaultToolkit().getScreenSize();
		
		int ancho = pantalla.width;
		int alto = pantalla.height;
		
		setBounds(((ancho - 400) / 2), ((alto - 400) / 2), 400, 400);
		
		add(new LaminaFuentes());
	}
	
	//Campos de clase
	private static final long serialVersionUID = 1L;
	
}

class LaminaFuentes extends JPanel {
	
	//M�todos setter
	public void paintComponent(Graphics g) {
		
		super.paintComponent(g);
		
		//Establece el color frontal o por defecto de los trazados
		setForeground(Color.BLUE);
		
		Graphics2D g2 = (Graphics2D) g;
		
		//Instanciamos una fuente Arial, negrita y de tama�o 26
		Font fuente = new Font("Times new Roman", Font.BOLD, 26);
		
		//Establecemos la fuente
		g2.setFont(fuente);
		g2.drawString("Te amo Raquel", 100, 100);
		
		g2.setFont(new Font("Arial", (Font.BOLD + Font.ITALIC), 24));
		g2.setPaint(new Color(255,150,200));
		g2.drawString("Tres millones", 100, 150);
	}
	
	//Campos de clase
	private static final long serialVersionUID = 1L;
	
}