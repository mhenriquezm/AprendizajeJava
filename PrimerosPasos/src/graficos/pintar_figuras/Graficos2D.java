package graficos.pintar_figuras;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Toolkit;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Line2D;
import java.awt.geom.Rectangle2D;

import javax.swing.JFrame;
import javax.swing.JPanel;

public class Graficos2D {

	public static void main(String[] args) {
		
		MarcoDibujos2D marco = new MarcoDibujos2D();
		
		marco.setVisible(true);
		marco.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
	}

}

class MarcoDibujos2D extends JFrame {
	
	//M�todos constructores
	public MarcoDibujos2D() {
		
		super("Gr�ficos en 2D");
		
		Dimension pantalla = Toolkit.getDefaultToolkit().getScreenSize();
		
		int ancho = pantalla.width;
		int alto = pantalla.height;
		
		setBounds(((ancho - 400) / 2), ((alto - 400) / 2), 400, 400);
		
		setMinimumSize(new Dimension(275, 300));
		
		add(new LaminaDibujos2D());
	}
	
	//Campos de clase
	private static final long serialVersionUID = 1L;
	
}

class LaminaDibujos2D extends JPanel {
	
	//M�todos Setter
	public void paintComponent(Graphics g) {
		
		super.paintComponent(g);
		
		//Creamos un objeto Graphics2D refundiendo a g
		Graphics2D g2D = (Graphics2D) g;
		
		int ancho = getWidth();
		int alto = getHeight();
		
		//Instanciamos un rectangulo 2D con su clase interna
		Rectangle2D rectangulo = new Rectangle2D.Double(
				((ancho - 200) / 2),
				((alto - 150) / 2),
				200,150);
		
		//Instanciamos un elipse 2D con su clase interna
		Ellipse2D elipse = new Ellipse2D.Double();
		
		//Establecemos los l�mites donde se dibujar� la elipse
		elipse.setFrame(rectangulo);
		
		//Dibujamos las figuras
		g2D.draw(rectangulo);
		g2D.draw(elipse);
		
		//Par�metros del c�rculo
		double centroX = rectangulo.getCenterX();
		double centroY = rectangulo.getCenterY();
		double radio = 125;
		
		Ellipse2D circulo = new Ellipse2D.Double();
		
		circulo.setFrameFromCenter(centroX, centroY, 
				(centroX + radio), (centroY + radio));
		
		//Instanciamos dos l�neas directamente en el m�todo draw
		g2D.draw(new Line2D.Double(
				((ancho - 200) / 2),
				((alto - 150) / 2),
				(((ancho - 200) / 2) + 200),
				(((alto - 150) / 2) + 150)));
		
		g2D.draw(new Line2D.Double(
				((ancho - 200) / 2),
				(((alto - 150) / 2) + 150),
				(((ancho - 200) / 2) + 200),
				((alto - 150) / 2)));
		
		g2D.draw(circulo);
		
	}
	
	//Campos de clase
	private static final long serialVersionUID = 1L;
	
}