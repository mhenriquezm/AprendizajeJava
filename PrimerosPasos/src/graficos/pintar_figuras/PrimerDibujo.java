package graficos.pintar_figuras;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Toolkit;

import javax.swing.JFrame;
import javax.swing.JPanel;

public class PrimerDibujo {

	public static void main(String[] args) {
		
		MarcoDibujo marco = new MarcoDibujo();
		
		marco.setVisible(true);
		marco.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
	}

}

class MarcoDibujo extends JFrame {
	
	//M�todos constructores
	public MarcoDibujo() {
		
		super("Primer dibujo");
		
		Dimension pantalla = Toolkit.getDefaultToolkit().getScreenSize();
		
		int ancho = pantalla.width;
		int alto = pantalla.height;
		
		setBounds(((ancho - 400) / 2), ((alto - 400) / 2), 400, 400);
		
		add(new LaminaDibujo());
	}
	
	//Campos de clase
	private static final long serialVersionUID = 1L;
}

class LaminaDibujo extends JPanel {
	
	//M�todos Setter
	public void paintComponent(Graphics g) {
		
		super.paintComponent(g);
		
		//Capturamos el ancho y el alto actual del componente
		int ancho = getWidth();
		int alto = getHeight();
		
		//Dibujamos un rect�ngulo con sus coordenadas y dimensiones
		g.drawRect(((ancho - 200) / 2), ((alto - 200) / 2), 200, 200);
		
		//Dibujamos una linea
		g.drawLine(
				((ancho - 200) / 2), 
				((alto - (alto / 4)) + 8), 
				(ancho - (ancho / 4) + 3), 
				((alto / 4) - 8));
		
		//Dibujamos un arco
		g.drawArc(145, 130, 100, 100, 0, 360);
	}
	
	//Campos de clase
	private static final long serialVersionUID = 1L;
}