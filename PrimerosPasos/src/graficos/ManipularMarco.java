package graficos;

import javax.swing.*;

public class ManipularMarco {

	public static void main(String[] args) {
		
		Frame marco = new Frame();
		
		marco.setVisible(true);
		marco.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
	}

}

class Frame extends JFrame {

	//M�todos constructores
	public Frame() {
		
		//Llamamos al constructor padre para establecer un t�tulo
		super("Primer marco");
		
		setSize(500,300);
		
		//Establecemos la ubicaci�n del marco
		setLocation(500,300);
		
		//Establece si permite o no redimensionar la ventana
		setResizable(false);
		
		//Permite manipular el estado de la ventana
		//setExtendedState(JFrame.MAXIMIZED_BOTH);
	}
	
	//Campos de clase
	private static final long serialVersionUID = 1L;
	
}