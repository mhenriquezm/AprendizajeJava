
public class UsoString2 {

	public static void main(String[] args) {
		
		/*Creamos un objeto String, si la frase es muy larga podemos 
		 * hacer un salto de l�nea y concatenar*/
		String frase = "Hoy es un estupendo d�a para aprender "
				+ "a programar en Java";
		
		/*El m�todo substring recibe 2 par�metros, el primero es el 
		 * �ndice desde donde se inicia la subcadena y el segundo es 
		 * donde termina */
		String frase2 = frase.substring(0, 29) + "irnos a la "
				+ "playa y olvidarnos de todo o " 
				+ frase.substring(29);
		/*Si le enviamos un �nico par�metro a substring toma ese indice 
		 * de partida hasta el final de la cadena original y retorna el 
		 * resultado de esa nueva cadena*/
		
		System.out.println(frase2);
		
	}

}
